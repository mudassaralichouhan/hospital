<?php

session_start();
include '../../vendor/autoload.php';


$title = 'Busness Report';
include('../include/wraper start.php');

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">

        <div class="col-lg-12 md-3">
            <section class="widget shadow-01 md-4" id="widget-01">
                <div class="widget-block">
                    <header>
                        <h3 class="widget-title">Acount Setting</h3>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            <br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </header>

                    <div class="row">

                        <div class="col-4">
                            <div class="list-group" id="list-tab" role="tablist">
                                <a class="list-group-item list-group-item-action active" id="list-home-list"
                                   data-toggle="list" href="#list-home" role="tab" aria-controls="home">Pagination</a>
                                <a class="list-group-item list-group-item-action" id="list-messages-list"
                                   data-toggle="list" href="#list-messages" role="tab"
                                   aria-controls="messages">Danger Zone</a>
                            </div>
                        </div>

                        <div class="col-8">
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="list-home" role="tabpanel"
                                     aria-labelledby="list-home-list">
                                    <form>
                                        <div class="form-group">
                                            <label for="pagination-page-content">Pagination page content size</label>
                                            <select multiple class="form-control"
                                                    id="pagination-page-content"
                                                    onchange="changeSetting();">
                                                <?php
                                                for ($i = 5; $i <= 150; $i += 5) {
                                                    echo '<option value="'.$i.'">' . $i . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="pagination-buttons">Pagination buttons are</label>
                                            <select class="form-control"
                                                    id="pagination-buttons"
                                                    onchange="changeSetting();">
                                                <?php
                                                for ($i = 3; $i <= 20; $i++) {
                                                    echo '<option>' . $i . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="list-messages" role="tabpanel"
                                     aria-labelledby="list-messages-list">

                                    <div class="col-md-12">
                                        <section class="widget border-1 border-dark" id="widget-18">
                                            <div class="widget-block">
                                                <header class="">
                                                    <h3 class="widget-title">Deactive your account</h3>
                                                    <p class="text-muted">
                                                        Deactive your account permanently if you want <b>re-activete</b> contact with administator
                                                    </p>
                                                </header>
                                                <div class="row">
                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm-password" title="Delete your account">
                                                        <i class="fa fa-trash-o"></i> Delete (de-active)
                                                    </button>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
        </div>

    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>
<script src="<?=$url?>/ui/public/js/user.js"></script>
<script>

    let user_id = <?=$_SESSION[SESSION]['id']?>;

    (function() {
        let user = JSONreq('user', 'fetch_setting', {
            id: user_id
        }, 'post');

        document.getElementById('pagination-page-content').value = user['setting']['rec_per_page'];
        document.getElementById('pagination-buttons').value = user['setting']['button'];

    })();

</script>
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="confirm-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">De-active your acccount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="status" class="container">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="consultant-contact" class="col-form-label">Confrim Password</label>
                        <input type="password" autofocus
                               id="password" class="form-control form-control-md mb-1"
                               placeholder="******">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="deactive()">Confirm</button>
            </div>
        </div>
    </div>
</div>
