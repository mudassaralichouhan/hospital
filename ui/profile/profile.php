<?php

session_start();
include '../../vendor/autoload.php';

$title = 'Profile';
include('../include/wraper start.php');

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">

        <div class="col-lg-12 md-3">
            <section class="widget shadow-01 md-4" id="widget-01">
                <div class="widget-block">
                    <header>
                        <h3 class="widget-title">Profile</h3>
                        <p class="text-muted">
                            User management system
                        </p>
                    </header>

                    <div class="row">

                        <div class="col-md-3"><!--left col-->
                            <div class="text-center">
                                <img src="<?= $url ?>/ui/public/images/avatar_2x.png" class="avatar img-circle rounded img-thumbnail " id="profile-photo" alt="avatar">
                                <br/><br/>
                                <h6>Upload a different photo...</h6>
                                <input type="file" class="text-center center-block file-upload" id="image" onchange="uploadProfilePhoto();">
                            </div>
                            <br/><br/>

                            <ul class="list-group">
                                <li class="list-group-item text-muted">
                                    Activity
                                    <i class="fa fa-dashboard fa-1x"></i>
                                </li>
                                <li class="list-group-item text-right">
                                    <span class="pull-left"><strong>Rule</strong></span>
                                    <span class="pull-right"><?= $_SESSION[SESSION]['rule'] ?></span>
                                </li>
                                <li class="list-group-item text-right">
                                    <span class="pull-left"><strong>Last login</strong></span>
                                    <span class="pull-right" id="user-last-login"></span>
                                </li>
                                <li class="list-group-item text-right">
                                    <span class="pull-left"><strong>Contact</strong></span>
                                    <span class="pull-right" id="user-contact"></span>
                                </li>
                                <li class="list-group-item text-right">
                                    <span class="pull-left"><strong>E-Male</strong></span>
                                    <span class="pull-right" id="user-email"></span>
                                </li>
                            </ul>

                        </div><!--/col-3-->

                        <div class="col-sm-9">

                            <ul class="nav nav-tabs overflow-auto">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile-tab">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#security-tab">Account security</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#log-tab">Security log</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#emails-tab">Emails</a>
                                </li>
                            </ul>

                            <div class="tab-content">

                                <div class="tab-pane" id="profile-tab"><br/>

                                    <div id="profile-err">
                                    </div>

                                    <form action="#" method="post" id="profile-from">

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="profile-name"><h4>Full name</h4></label>
                                                <input type="text"
                                                       class="form-control"
                                                       onChange="strLenth('profile-name','profile-name-err', 30, 3)"
                                                       id="profile-name"
                                                       placeholder="name">
                                                <label id="profile-name-err" class="col-form-label"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="inputGender" class="col-form-label">Gender</label><br/>
                                                <div class="form-row">
                                                    <div class="form-group col-md-2">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <input type="radio"
                                                               onChange="chkRadio('female','profile-gender-err')"
                                                               name="gender"
                                                               class="profile-gender"
                                                               id="female" value="Female"
                                                        > Female
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <input type="radio"
                                                               onChange="chkRadio('male','profile-gender-err')"
                                                               name="gender"
                                                               class="profile-gender"
                                                               id="male" value="Male"
                                                        > Male
                                                    </div>
                                                </div>
                                                <label id="profile-gender-err" class="col-form-label"></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="profile-age"><h4>age</h4></label>
                                                <input type="number"
                                                       onchange="isNumeric('profile-age','profile-age-err')"
                                                       class="form-control"
                                                       id="profile-age"
                                                       placeholder="first name">
                                                <label id="profile-age-err" class="col-form-label"></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="profile-contact"><h4>Mobile</h4></label>
                                                <input type="text"
                                                       onchange="pkTel('profile-mobile','profile-mobile-err')"
                                                       class="form-control"
                                                       id="profile-contact"
                                                       placeholder="enter mobile number">
                                                <label id="profile-contact-err" class="col-form-label"></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="profile-location"><h4>Location</h4></label>
                                                <textarea class="form-control" id="profile-location"
                                                          placeholder="somewhere"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <br>
                                                <button class="btn btn-primary"
                                                        type="submit"
                                                        onclick="updateProfile();">
                                                    <i class="glyphicon glyphicon-ok-sign"></i> Save
                                                </button>
                                                <button class="btn btn-md" type="reset">
                                                    <i class="glyphicon glyphicon-repeat"></i> Reset
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <hr>
                                </div>

                                <div class="tab-pane" id="security-tab"><br/>
                                    <form class="form" action="#" method="post" id="password-change-from">

                                        <div id="password-err">
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="security-curr-password"><h4>Current</h4></label>
                                                <input type="password" class="form-control"
                                                       name="security-curr-password" id="security-curr-password"
                                                       placeholder="Current password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="security-password"><h4>New</h4></label>
                                                <input type="password" class="form-control" name="security-password"
                                                       id="security-password" placeholder="New password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="security-password2"><h4>New re-type</h4></label>
                                                <input type="password" class="form-control" name="security-password2"
                                                       id="security-password2" placeholder="Re-Type password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <button onclick="changePassword(document.getElementById('security-curr-password'), document.getElementById('security-password'), document.getElementById('security-password2'));" class="btn btn-primary"
                                                        type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save
                                                </button>
                                                <button class="btn" type="reset"><i
                                                            class="glyphicon glyphicon-repeat"></i> Reset
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                                <div class="tab-pane active" id="log-tab"><br/>

                                    <!-- table -->
                                    <div class="col-lg-12 mb-12">
                                        <table class="table table-striped table-hover">
                                            <thead class="text-muted" id="header">
                                            </thead>
                                            <tbody id="data">
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /table -->

                                </div>

                                <div class="tab-pane" id="emails-tab"><br/>

                                    <form class="form" action="#" method="post">
                                        <div id="emails-new-email-err">
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="emails-email"><h4>E-Mail</h4></label>
                                                <input type="email" class="form-control" name="emails-email"
                                                       id="emails-email" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="emails-new-email"><h4>Change E-Mail</h4></label>
                                                <input type="email" class="form-control" name="emails-new-email"
                                                       id="emails-new-email" placeholder="new email">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <button onclick="changeEmail();" class="btn btn-primary" type="submit">
                                                    <i class="glyphicon glyphicon-ok-sign"></i> Save
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                            </div><!--/tab-content-->
                        </div><!--/col-sm-9-->

                    </div><!--/row-->

                </div>
            </section>
        </div>

    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>
<script src="../public/js/user.js"></script>
<script>

    userLog();
    setProfile();
    setProfilePhoto();

</script>
