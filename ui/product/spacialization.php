<?php

session_start();
include '../../vendor/autoload.php';

if(!is_admin()) {
    header("location: ".$_SERVER['HTTP_HOST']);
}

$title = 'Spacialization';
include('../include/wraper start.php');

?>

    <div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <section class="widget shadow-01 mb-4">
                    <div class="widget-block">
                        <header>
                            <h3>Consultant Spacialization Method</h3>
                            <p class="text-muted">
                                More Spacialization can also be created with this method.
                            </p>
                        </header>
                        <div class="widget-content py-3" id="panel">

                            <!-- form -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name" class="col-form-label">Dental Procedure</label>
                                    <input type="text"
                                           class="form-control form-control-lg mb-1"
                                           placeholder="Consultant Spacial"
                                           id="name"
                                           onChange="strLenth('name', 'name-err',30,3)">
                                    <label id="name-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <button onClick="insertSpacializ()"
                                        class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                            <!-- /form -->

                        </div>
                    </div>
                </section>

                <section class="widget shadow-01 mb-4">
                    <div class="widget-block">
                        <header>
                            <h3>Spacialization are listed here</h3>
                        </header>

                    </div>

                    <!-- table -->
                    <div class="col-lg-12 mb-12">
                        <table class="table table-striped table-hover">
                            <thead class="text-muted">
                            <th>#</th>
                            <th>Name</th>
                            <th>Register</th>
                            <th>Update</th>
                            <th>Action</th>
                            </thead>
                            <tbody id="data">
                            </tbody>
                        </table>
                    </div>
                    <!-- /table -->

                </section>
            </div>
        </div>
    </div>

    <script>


    </script>

    <!-- Modal -->
    <div class="modal fade"
         id="edit" tabindex="-1"
         role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit Consultant Spacialization</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div id="status" class="container">
                    </div>

                    <!-- form -->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="edit-name" class="col-form-label">Spacialization</label>
                            <input type="text"
                                   id="edit-name"
                                   class="form-control form-control-lg mb-1"
                                   placeholder="Spacialization e.g(Hematologists)"
                                   onChange="strLenth('edit-name','edit-name-err',100,3)" value="">
                            <label id="edit-name-err" class="col-form-label"></label>
                        </div>
                    </div>
                    <input type="hidden" id='edit-id'>
                    <!-- /form -->

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button"
                            onClick="updateSpacializ()"
                            class="btn btn-primary">
                        Save changes
                    </button>
                </div>

            </div>
        </div>

    </div>
    <!-- /Modal -->
    <script>

        // append data to table - table content -->
        $(function () {

            appendSpacializToTalbe(JSONreq('procedure_spacializ', 'limit', {
                'offset': 0,
                'rows': 20
            }), $('#data'), 0);

            let $offset = 20;
            let $rows = 3;

            $(window).scroll(function () {
                if ($(window).scrollTop() >= $('#data').height() - 250) {
                    appendSpacializToTalbe(JSONreq('procedure_spacializ', 'limit', {
                        'offset': $offset,
                        'rows': $rows
                    }), $('#data'), $offset);
                    $offset += $rows;
                }
            });

        });

        // end table content -->

        function setSpacializToModelForEdit(HTMLelements) {
            for (let i = 0; i < HTMLelements.length; i++) {

                HTMLelements[i].onclick = function () {
                    let response = JSONreq('procedure_spacializ', 'fetch', {
                        id: $(this).attr('id')
                    });

                    document.getElementById('edit-name').value = response['name'];
                    document.getElementById('edit-id').value = $(this).attr('id');
                }
            }
        }

        function updateSpacializ() {

            let name = document.getElementById('edit-name');
            let id = document.getElementById('edit-id');
            let nameErr = document.getElementById('edit-name-err');

            if (name.value.length >= 3) {

                let response = JSONreq('procedure_spacializ', 'update', {
                    'spacializ_name': name.value,
                    'id': id.value
                });

                if (response.status) {
                    $('#edit').modal('hide');

                    let $html = '<div class="alert alert-dark" role="alert">' +
                        'Your record is Update <b>Successfully</b>' +
                        '</div>';

                    notification($html);

                    name.value = '';
                    name.style.borderColor = '#ced4da';
                    nameErr.style.borderColor = '#ced4da';
                    nameErr.innerHTML = '';
                } else {
                    name.style.borderColor = 'red';
                    nameErr.style.borderColor = 'red';
                    nameErr.style.color = 'red';
                    nameErr.innerHTML = 'Spacialization not avalible for register';
                }
            } else {
                name.style.borderColor = 'red';
                nameErr.style.borderColor = 'red';
                nameErr.style.color = 'red';
                nameErr.innerHTML = 'Name is Require';
            }
        }

        function insertSpacializ() {

            let name = document.getElementById('name');
            let nameErr = document.getElementById('name-err');

            if (name.value.length >= 3) {

                let response = JSONreq('procedure_spacializ', 'insert', {
                    'spacializ_name': name.value
                });

                $('.alert').remove();
                if (response.status) {

                    let $html = '<div class="alert alert-success" role="alert">' +
                        'Your record is Store <b>Successfully</b> with all condition' +
                        '</div>';
                    notification($html);
                    name.value = '';
                    name.style.borderColor = '#ced4da';
                    nameErr.innerHTML = '';

                } else if (response.status === false) {

                    let $html = '<div class="alert alert-danger" role="alert">' +
                        'This Record is <b>already</b> inserted in passed on base of contact#.' +
                        'So Change the cred and try another way!' +
                        '</div>';

                    notification($html);

                    name.style.borderColor = 'red';
                    nameErr.style.color = 'red';
                    nameErr.innerHTML = 'already exist';
                } else {

                    let $html = '<div class="alert alert-danger" role="alert">' +
                        'Some Thing was wrong!' +
                        '</div>';

                    notification($html);
                }


            } else {
                name.style.borderColor = 'red';
                let err = document.getElementById('name-err');
                err.style.borderColor = 'red';
                err.style.color = 'red';
                err.innerHTML = 'Require name';
            }
        }

        function appendSpacializToTalbe(response, container, offset) {

            if (response.status !== false) {
                for (let i = 0, y = offset + 1; i < response.length; i++, y++) {
                    let tableRow = '<tr>' +
                        '<th scope="row">' + response[i].id + '</th>' +
                        '<td>' + response[i].name + '</td>' +
                        '<td>' + readableDate(response[i].reg_on) + '</td>' +
                        '<td>' + readableDate(response[i].update_on) + '</td>' +
                        '<td>' + actionIcon(response[i].id) + '</td>' +
                        '</tr>';

                    container.append(tableRow);
                }
            }

            deleteById('procedure_spacializ', document.getElementsByClassName('delete'));
            setSpacializToModelForEdit(document.getElementsByClassName('edit'));
        }

    </script>
<?php
include($path . '/ui/include/wraper end.php');
