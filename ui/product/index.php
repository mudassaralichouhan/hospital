<?php

session_start();
include '../../vendor/autoload.php';

if(!is_admin()) {
    header("location: ".$_SERVER['HTTP_HOST']);
}

$title = 'Procedure';
include('../include/wraper start.php');

$get_product = isset($_GET['p']) ? $_GET['p'] : exit;

?>

    <div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <section class="widget shadow-01 mb-4">
                    <div class="widget-block">
                        <header>
                            <h3><?=$get_product?> Procedure Method</h3>
                            <p class="text-muted">
                                More dental procedure can also be created with this method.
                            </p>
                        </header>
                        <div class="widget-content py-3" id="panel">

                            <!-- form -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name" class="col-form-label">Dental Procedure</label>
                                    <input type="text"
                                           class="form-control form-control-lg mb-1"
                                           placeholder="Procedure e.g(Filling)"
                                           id="name"
                                           onChange="strLenth('name', 'name-err',30,3)">
                                    <label id="name-err" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="price" class="col-form-label">Price</label>
                                    <input type="number"
                                           id="price"
                                           onChange="isEmpty('price','fee-err');"
                                           class="form-control form-control-lg mb-1" placeholder="Price e.g(300 Rs)"
                                           value=0>
                                    <label id="fee-err" class="col-form-label"></label>

                                </div>
                                <div class="form-group col-md-12">
                                    <button onClick="insertProcedure('<?= $get_product ?>')"
                                            class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                            <!-- /form -->

                        </div>
                    </div>
                </section>

                <section class="widget shadow-01 mb-4">
                    <div class="widget-block">
                        <header>
                            <h3>Dental are listed here</h3>
                        </header>

                    </div>

                    <!-- table -->
                    <div class="col-lg-12 mb-12">
                        <table class="table table-striped table-hover">
                            <thead class="text-muted">
                            <th>#</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Register</th>
                            <th>Update</th>
                            <th>Action</th>
                            </thead>
                            <tbody id="data">
                            </tbody>
                        </table>
                    </div>
                    <!-- /table -->

                </section>
            </div>
        </div>
    </div>

    <script>

        // append data to table - table content -->
        $(function () {
            appendProductToTalbe(JSONreq('<?=$get_product?>', 'limit', {
                'offset': 0,
                'rows': 20
            }), $('#data'), 0);

            let $offset = 20;
            let $rows = 3;

            let $editElement = {
                'edit-name': 'name',
                'edit-price': 'price'
            };

            $(window).scroll(function () {
                if ($(window).scrollTop() >= $('#data').height() - 250) {
                    appendProductToTalbe(JSONreq('<?=$get_product?>', 'limit', {
                        'offset': $offset,
                        'rows': $rows
                    }), $('#data'), $offset);
                    $offset += $rows;

                    deleteById('<?=$get_product?>', document.getElementsByClassName('delete'));
                    setProductToModelForEdit('<?=$get_product?>', $editElement, document.getElementsByClassName('edit'));
                }
            });

            deleteById('<?=$get_product?>', document.getElementsByClassName('delete'));
            setProductToModelForEdit('<?=$get_product?>', $editElement, document.getElementsByClassName('edit'));

        });
        // end table content -->
    </script>

    <!-- Modal -->
    <div class="modal fade"
         id="edit" tabindex="-1"
         role="dialog"
         aria-labelledby="ModalCenterTitle"
         aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="ModalCenterTitle">Edit Product Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div id="status" class="container">
                    </div>

                    <!-- form -->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="edit-name" class="col-form-label">Laboratory Test</label>
                            <input type="text"
                                   id="edit-name"
                                   class="form-control form-control-lg mb-1"
                                   placeholder="Procedure e.g(Allergic Reactions)"
                                   onChange="strLent('edit-name','edit-name-err',30,3)">
                            <label id="edit-name-err" class="col-form-label"></label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="edit-price" class="col-form-label">Price</label>
                            <input type="number" id="edit-price"
                                   onChange="isEmpty('edit-price','edit-fee-err');"
                                   class="form-control form-control-lg mb-1" placeholder="Price e.g(in Rs)" value="">
                            <label id="edit-fee-err" class="col-form-label"></label>
                        </div>
                    </div>
                    <input type="hidden" id='edit-id'>
                    <!-- /form -->

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button"
                            onClick="updateProcedure('<?= $get_product ?>')"
                            class="btn btn-primary">
                        Save changes
                    </button>
                </div>

            </div>
        </div>

    </div>
    <!-- /Modal -->

    <script src="<?= $url ?>/ui/public/js/product_function.js"></script>

<?php
//echo '<script src="'.$url.'/ui/public/js/product_function.js"></script>';
include($path . '/ui/include/wraper end.php');
