<?php

session_start();
include '../../vendor/autoload.php';

$title = 'View';
include('../include/wraper start.php');

$request = isset($_GET['r']) ? $_GET['r'] : exit;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$method = isset($_GET['m']) ? $_GET['m'] : 'GET';

?>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3><?= $request ?> view</h3>
                        <p class="text-muted">
                            More dental patient can also be created with this method.
                        </p>
                    </header>
                    <div class="widget-content py-3" id="get-patient-form">
                        <!-- pagination -->
                        <div class="container">
                            <nav aria-label="...">
                                <ul class="pagination">
                                </ul>
                            </nav>
                        </div>
                        <!-- /pagination -->

                        <!-- table -->
                        <div class="col-lg-12 mb-12">
                            <table class="table table-striped table-hover">
                                <thead class="text-muted" id="header">
                                </thead>
                                <tbody id="data">
                                </tbody>
                            </table>
                        </div>
                        <!-- /table -->

                        <!-- pagination -->
                        <div class="container">
                            <nav aria-label="...">
                                <ul class="pagination">
                                </ul>
                            </nav>
                        </div>
                        <!-- /pagination -->
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php
include($path . '/ui/include/wraper end.php');
?>
<script>
    $method = '<?= $method ?>';
    $request_to = '<?= $request ?>';
    $page = '<?= $page ?>';
</script>
<script src="../../ui/public/js/view.js"></script>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myExtraLargeModalLabel" class="modal fade bd-example-modal-lg" id="edit"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <?php
            switch ($request) {
                case 'patient':
                    include 'model_patient.php';
                    break;
                case 'consultant':
                    include 'model_conultant.php';
                    break;
                case 'user':
                    include 'model_user.php';
                    break;
                case 'slip_dental':
                case 'slip_surgical':
                case 'slip_lab_test':
                case 'slip_xray':
                    include 'model_slip.php';
                    break;
            }
            ?>

        </div>
    </div>
</div>
<!-- /Modal -->