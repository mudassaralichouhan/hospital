<?php

$title = 'Slip information';
include('../include/wraper start.php');

$id = isset($_GET['id']) ? $_GET['id'] : exit;

?>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3>Slip view</h3>
                        <p class="text-muted">
                            More Slip patient's information view can also be created with this method.
                        </p>
                    </header>
                    <div class="widget-content py-3" id="get-patient-form">

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php
include($path . '/ui/include/wraper end.php');
?>
