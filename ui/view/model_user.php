<div class="modal-header">
    <h5 class="modal-title" id="ModalLabel">User Edit</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div id="status" class="container">
    </div>
    <div class="row">

        <div class="form-group col-md-6"><span class="badge badge-secondary">Patient Id</span>
            <h3 class="h3" id="id"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">Register Date</span>
            <h3 class="h3" id="user-register-date"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">Last Modifed</span>
            <h3 class="h3" id="user-update"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">Contact</span>
            <h3 class="h3" id="user-contact"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">E-mail</span>
            <h3 class="h3" id="user-email"></h3>
        </div>
    </div>
    <!-- form -->

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="user-name" class="col-form-label">Name</label>
            <input type="text" id='user-name' id="user-name"
                   onChange="strLenth('user-name', 'name-err',30,3)"
                   class="form-control form-control-lg mb-1" placeholder="Consultant Name">
            <label id="name-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="user-age" class="col-form-label">Age
            </label>
            <select id='user-age'
                    onChange="chkSelect('user-age', 'age-err', 'Selected Lab Percentage ')"
                    class="form-control form-control-lg mb-1" required="true">
                <option value="" Disabled>Select Age</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="age-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="user-rule" class="col-form-label">Rule
            </label>
            <select id='user-rule'
                    onChange="chkSelect('user-rule', 'rule-err', 'Selected Lab Percentage ')"
                    class="form-control form-control-lg mb-1" required="true">
                <option value="" Disabled>Select Rule</option>
                <option value="User">Normal User</option>
                <option value="Admin">Super Admin</option>
            </select>
            <label id="rule-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="gender" class="col-form-label">Gender</label><br/><br/>
            <div class="form-row">
                <div class="form-group col-md-2">
                </div>

                <div class="form-group col-md-3">
                    <input type="radio" name='gender'
                           onChange="chkRadio('female','gender-err')"
                           class="user-gender" id="female" value="Female"> Female
                </div>

                <div class="form-group col-md-4">
                    <input type="radio" name='gender'
                           onChange="chkRadio('male','gender-err')"
                           class="user-gender" id="male" value="Male"> Male
                </div>
            </div>
            <label id="gender-err" class="col-form-label"></label>
        </div>
    </div>

    <div class="form-group">
        <label for="user-address" class="col-form-label">Address <code>&lt;(optional)&gt;</code></label>
        <textarea id='user-address' class="form-control form-control-lg mb-1" rows="3"></textarea>
    </div>

    <!-- /form -->
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <!-- update patient on click event -->
    <button type="button"
            class="btn btn-primary"
            onclick="updateUser();"
            id="updateBtn"
    >
        Save changes
    </button>
</div>

<script src="<?= $url ?>/ui/public/js/user.js""></script>