<div class="modal-header">
    <h5 class="modal-title" id="ModalLabel">Patient Edit</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div id="status" class="container">
    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="form-group col-md-6"><span class="badge badge-secondary">Patient Id</span>
                <h3 class="h3" id="id"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Register Date</span>
                <h3 class="h3" id="patient-register-date"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Last Modifed</span>
                <h3 class="h3" id="patient-update"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Last Modifed</span>
                <h3 class="h3" id="patient-user"></h3>
            </div>
        </div>

        <div class="row">
            <!-- form -->

            <div class="form-row col-md-12">
                <div class="form-group col-md-12">
                    <label for="patient-consultant-id" class="col-form-label">Consultant</label>
                    <select id='patient-consultant-id'
                            onChange="patientPaidAmt(); chkSelect('patient-consultant-id','consultant-err', 'Selected consultant: ');"
                            class="form-control form-control-lg mb-1">
                    </select>
                    <label id="consultant-err" class="col-form-label"></label>
                </div>
            </div>


            <div class="form-row col-md-6">
                <div class="form-group col-md-12">
                    <label for="patient-name" class="col-form-label">Name</label>
                    <input type="text" id="patient-name" autofocus
                           onChange="strLenth('patient-name','name-err', 30, 3)"
                           class="form-control form-control-lg mb-1" placeholder="Patient Name">
                    <label id="name-err" class="col-form-label"></label>
                </div>
            </div>

            <div class="form-row col-md-6">
                <div class="form-group col-md-12">
                    <label for="patient-contact" class="col-form-label">Contact (Mobile#)</label>
                    <input type="number" onChange="pkTel('patient-contact','contact-err');"
                           id="patient-contact" class="form-control form-control-lg mb-1"
                           placeholder="e.g: (0300 11 22 333)" maxlength="11" value="0300">
                    <label id="contact-err" class="col-form-label"></label>
                </div>
            </div>

            <div class="form-row col-md-6">
                <div class="form-group col-md-12">
                    <label for="patient-age" class="col-form-label">Age</label>
                    <select id="patient-age"
                            onChange="chkSelect('patient-age','age-err','Age is: ');"
                            class="form-control form-control-lg mb-1">
                        <?php for ($age = 1; $age < 150; $age++) {
                            echo "<option value='$age'>$age</option>";
                        } ?>
                    </select>
                    <label id="age-err" class="col-form-label"></label>
                </div>
            </div>

            <div class="form-row col-md-6">
                <div class="form-group col-md-12">
                    <label class="col-form-label">Gender</label><br/>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                        </div>

                        <div class="form-group col-md-3">
                            <input type="radio"
                                   onChange="chkRadio('male','gender-err')"
                                   name="gender" class="patient-gender" id="male"
                                   value="Female"> Female
                        </div>

                        <div class="form-group col-md-4">
                            <input type="radio"
                                   onChange="chkRadio('femaler','gender-err')"
                                   name="gender" class="patient-gender" id="female"
                                   value="Male">
                            Male
                        </div>
                    </div>
                    <label id="gender-err" class="col-form-label"></label>
                </div>
            </div>


            <div class="form-row col-md-6">
                <div class="form-group col-md-12">
                    <label for="patient-discount" class="col-form-label">Discount
                        <code>&lt;(optional)&gt;</code></label>
                    <select id="patient-discount"
                            onChange="patientPaidAmt(); chkSelect('patient-discount','discount-err','Discount: ');"
                            class="form-control form-control-lg mb-1">
                        <option value="" Disabled>Select Discount</option>
                        <?php for ($discount = 0; $discount <= 100; $discount++) {
                            echo "<option value='$discount'>$discount</option>";
                        } ?>
                    </select>
                    <label id="discount-err" class="col-form-label"></label>
                </div>
            </div>


            <div class="form-row col-md-6">
                <div class="form-group col-md-12">
                    <label for="patient-paid" class="col-form-label">Paid</label>
                    <p class="h1" id="patient-paid">0 Rs</p>
                </div>
            </div>

            <div class="form-row col-md-12">
                <div class="form-group col-md-12">
                    <label for="patient-address" class="col-form-label">Address<code>&lt;(optional)&gt;</code></label>
                    <textarea id="patient-address" class="form-control form-control-lg mb-1" rows="3"></textarea>
                </div>
            </div>
            <!-- /form -->
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <!-- update patient on click event -->
    <button type="button"
            class="btn btn-primary"
            onclick="updatePatient();"
            id="updateBtn"
    >Save changes
    </button>
</div>
<script src="<?= $url ?>/ui/public/js/patient.js"></script>
<script>setNameFeeOfConsultant();</script>