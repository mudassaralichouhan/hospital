
<div class="modal-header">
    <h5 class="modal-title" id="ModalLabel">Slip Edit</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div id="status" class="container">
    </div>
    <div class="col-lg-12">
        <div class="row">

            <div class="form-group col-md-6"><span class="badge badge-secondary">Patient Id</span>
                <h3 class="h3" id="id"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Register Date</span>
                <h3 class="h3" id="register-date"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Last Modifed</span>
                <h3 class="h3" id="update"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Employe name</span>
                <h3 class="h3" id="user"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Patient name</span>
                <h3 class="h3" id="patient-name"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Consultant name</span>
                <h3 class="h3" id="consultant-name"></h3>
            </div>
            <div class="form-group col-md-6"><span class="badge badge-secondary">Get Amount from Patient is:</span>
                <h3 class="h3" id="already-get-amount"></h3>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="form-group col-md-12">
            <label for="procedure-id" class="col-form-label">Select Procedure</label>
            <select onChange="chkSelect('procedure-id','procedure-err',  'Selected Laboratory Procedure: '); setSlipPaidAmt()"
                    class="form-control form-control-lg mb-1" id="procedure-id">
                <option value="">Select Procedure</option>
            </select>
            <label id="procedure-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="get-amount" class="col-form-label">Get Amount</label>
            <input type="text"
                   id='get-amount'
                   onChange="isEmpty('get-amount','get-amount-err');"
                   class="form-control form-control-lg mb-1"
                   placeholder="Get Amount">
            <label id="get-amount-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="discount" class="col-form-label">Discount</label>
            <select id="discount"
                    onChange="setSlipPaidAmt(); chkSelect('discount','discount-err','Discount: ');"
                    class="form-control form-control-lg mb-1" height="100">
                <option value="" Disabled>Select Discount</option>
                <?php for ($disc = 0; $disc <= 100; $disc++) {
                    echo "<option value='$disc'>$disc</option>";
                } ?>
            </select>
            <label id="discount-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-12">
            <p class="h5">Paid amount after discount: </p>
            <p id="price" class="h3">0 Rs</p>
        </div>

    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <!-- update patient on click event -->
    <button type="button"
            class="btn btn-primary"
            onclick="updateSlip($request_to);"
            id="updateBtn">Save changes
    </button>
</div>
<script src="<?= $url ?>/ui/public/js/slip_function.js"></script>