<div class="modal-header">
    <h5 class="modal-title" id="ModalLabel">Consultant Edit</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div id="status" class="container">
    </div>
    <div class="row">
        <div class="form-group col-md-6"><span class="badge badge-secondary">Consultant Id</span>
            <h3 class="h3" id="id"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">Register Date</span>
            <h3 class="h3" id="consultant-register-date"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">Last Modifed</span>
            <h3 class="h3" id="consultant-update"></h3>
        </div>
        <div class="form-group col-md-6"><span class="badge badge-secondary">User</span>
            <h3 class="h3" id="consultant-user"></h3>
        </div>
    </div>
    <!-- form -->
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="consultant-specialization" class="col-form-label">Specialization</label>
            <select id='consultant-specialization'
                    onChange="chkSelect('consultant-specialization', 'specialization-err',  'Selected Specialization: ')"
                    class="form-control form-control-lg mb-1">
                <option value="">Select Specialization</option>
            </select>
            <label id="specialization-err" class="col-form-label"></label>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="consultant-name" class="col-form-label">Name</label>
            <input type="text" id='consultant-name'
                   onChange="strLenth('consultant-name', 'name-err',30,3)"
                   class="form-control form-control-lg mb-1" placeholder="Consultant Name">
            <label id="name-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="consultant-age" class="col-form-label">Age
            </label>
            <select id='consultant-age'
                    onChange="chkSelect('consultant-age', 'age-err', 'Selected Lab Percentage ')"
                    class="form-control form-control-lg mb-1">
                <option value="" Disabled>Select Age</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="age-err" class="col-form-label"></label>
        </div>

        <div class="form-group col-md-6">
            <label for="consultant-contact" class="col-form-label">Contact (Mobile#)</label>
            <input type="number" onChange="pkTel('consultant-contact','contact-err');"
                   id="consultant-contact" class="form-control form-control-lg mb-1"
                   placeholder="e.g: (0300 11 22 333)">
            <label id="contact-err" class="col-form-label"></label>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="consultant-fee" class="col-form-label">Fee
                <code>&lt;(optional)&gt;</code></label>
            <input onChange="isEmpty('consultant-fee','fee-err');"
                   id='consultant-fee'
                   class="form-control form-control-lg mb-1" value="0"
                   placeholder="Consultant Fee e.g: 300.Rs">
            <label id="fee-err" class="col-form-label"></label>
        </div>
        <div class="form-group col-md-6">
            <label for="gender" class="col-form-label">Gender</label><br/><br/>
            <div class="form-row">

                <div class="form-group col-md-2">
                </div>

                <div class="form-group col-md-3">
                    <input type="radio" name='gender'
                           onChange="chkRadio(this,'gender-err')"
                           class="form-check-input consultant-gender" value="Female"> Female
                </div>

                <div class="form-group col-md-4">
                    <input type="radio" name='gender'
                           onChange="chkRadio(this,'gender-err')"
                           class="form-check-input consultant-gender" value="Male"> Male
                </div>
            </div>
            <label id="gender-err" class="col-form-label"></label>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="consultant-perc" class="col-form-label">Consultantance % <code>&lt;(optional)&gt;</code></label>
            <select id='consultant-perc'
                    onChange="chkSelect('consultant-perc', 'consultant-perc-err', 'Selected Consultant Percentage ')"
                    class="form-control form-control-lg mb-1" require="true">
                <option value="" Disabled>Select Consultantance</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="consultant-perc-err" class="col-form-label"></label>
        </div>
        <div class="form-group col-md-3">
            <label for="consultant-lab-perc" class="col-form-label">Lab %
                <code>&lt;(optional)&gt;</code></label>
            <select id='consultant-lab-perc'
                    onChange="chkSelect('consultant-lab-perc', 'lab-perc-err', 'Selected Lab Percentage ')"
                    class="form-control form-control-lg mb-1" required="true">
                <option value="" Disabled>Select Lab</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="lab-perc-err" class="col-form-label"></label>
        </div>
        <div class="form-group col-md-3">
            <label for="consultant-dental-perc" class="col-form-label">Dental %
                <code>&lt;(optional)&gt;</code></label>
            <select id='consultant-dental-perc'
                    onChange="chkSelect('consultant-dental-perc', 'dental-perc-err', 'Selected Dental Percentage ')"
                    class="form-control form-control-lg mb-1" required="true">
                <option value="" Disabled>Select Dental</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="dental-perc-err" class="col-form-label"></label>
        </div>
        <div class="form-group col-md-3">
            <label for="consultant-xray-perc" class="col-form-label">X Ray %
                <code>&lt;(optional)&gt;</code></label>
            <select id='consultant-xray-perc'
                    onChange="chkSelect('consultant-xray-perc', 'xray-perc-err', 'Selected X-Ray Percentage ')"
                    class="form-control form-control-lg mb-1" required="true">
                <option value="" Disabled>Select X Ray</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="xray-perc-err" class="col-form-label"></label>
        </div>
        <div class="form-group col-md-3">
            <label for="consultant-surgical-perc" class="col-form-label">Surgical %
                <code>&lt;(optional)&gt;</code></label>
            <select id='consultant-surgical-perc' id="surgical"
                    onChange="chkSelect('consultant-surgical-perc', 'surgicalt-perc-err'), 'Selected Surgical Percentage ')"
                    class="form-control form-control-lg mb-1" require="true">
                <option value="" Disabled>Select Surgical</option>
                <?php
                for ($i = 0; $i <= 100; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
            <label id="surgicalt-perc-err" class="col-form-label"></label>
        </div>
    </div>

    <div class="form-group">
        <label for="consultant-address" class="col-form-label">Address <code>&lt;(optional)&gt;</code></label>
        <textarea id='consultant-address' class="form-control form-control-lg mb-1" rows="3"></textarea>
    </div>
    <!-- /form -->
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <!-- update patient on click event -->
    <button type="button"
            class="btn btn-primary"
            onclick="updateConsultant();"
            id="updateBtn"
    >Save changes
    </button>
</div>

<script src="<?= $url ?>/ui/public/js/consultant.js""></script>
<script>setSpacializtion()</script>