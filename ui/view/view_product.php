<?php

session_start();
include '../../vendor/autoload.php';


$title = 'Slip information';
include('../include/wraper start.php');

$procedure_id = isset($_GET['procedure_id']) ? $_GET['procedure_id'] : exit;
$name = isset($_GET['name']) ? $_GET['name'] : exit;
$page = isset($_GET['page']) ? $_GET['page'] : 1;

?>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3>Product view</h3>
                        <p class="text-muted">
                            More Slip patient's information view can also be created with this method.
                        </p>
                    </header>

                    <div class="widget-content py-3">
                        <div class="row">
                            <div class="form-group col-md-7"><span class="badge badge-secondary">Product Id</span>
                                <h3 class="h3" id="id"></h3>
                            </div>
                            <div class="form-group col-md-2"><span class="badge badge-secondary">Register Date</span>
                                <h3 class="h3" id="product-register-date"></h3>
                            </div>
                            <div class="form-group col-md-3"><span class="badge badge-secondary">Last Modifed</span>
                                <h3 class="h3" id="product-update"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">User name</span>
                                <h3 class="h3" id="product-user"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">Price</span>
                                <h3 class="h3" id="product-price"></h3>
                            </div>
                        </div>
                    </div>

                    <div class="widget-content py-3" id="get-patient-form">
                        <div class="row">
                            <!-- table -->
                            <div class="col-lg-12 mb-12">
                                <table class="table table-striped table-hover">
                                    <thead class="text-muted" id="header">
                                    </thead>
                                    <tbody id="data">
                                    </tbody>
                                </table>
                            </div>
                            <!-- /table -->

                            <!-- pagination -->
                            <div class="container">
                                <nav aria-label="...">
                                    <ul class="pagination">
                                    </ul>
                                </nav>
                            </div>
                            <!-- /pagination -->

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php
include($path . '/ui/include/wraper end.php');
?>

<script src="<?= $url ?>/ui/public/js/product_function.js"></script>
<script>

    let $response = JSONreq('<?=$name?>','fetch',{id:<?=$procedure_id?>});
    displayProduct($response);

    $response = JSONreq('<?=$name?>','fetch_with_patient_limit', {
        id: <?=$procedure_id?>,
        page: <?=$page?>
    });

    let $table_headers = $response['headers'];
    let $table_data = $response['data'];
    let $pagi = $response['pagination'];

    // append data to table - table content -->
    let $row = '<tr>';
    for (let $i = 0; $i < $table_headers.length; $i++) {
        $row += '<th>' + $table_headers[$i] + '</th>';
    }
    $row += '<th>Action</th>';
    $row += '</tr>';
    $('#header').append($row);

    for (let $i = 0; $i < Object.keys($table_data).length; $i++) {

        let $row = '<tr>';
        $.each($table_data[$i], function (index, item) {

            if(index === 'reg_on') {
                $row += '<td>' + readableDate(item) + '</td>';
            } else if (index === 'patient_id') {
                $row += '<td><a href="view_patient.php?id=' + $table_data[$i]['patient_id'][1] + '">' +
                        $table_data[$i]['patient_id'][0] +
                    '</a></td>';
            } else if (index === 'user_id') {
                $row += '<td><a href="view_user.php?id=' + $table_data[$i]['user_id'][1] + '">'
                        + $table_data[$i]['user_id'][0] +
                    '</a></td>';
            } else {
                $row += '<td>' + item + '</td>';
            }


        });
        $row += '<td>' + actionIcon($table_data[$i]['patient_id'][1]) + '</td>';
        $row += '</tr>';

        $('#data').append($row);
    }

    deleteById('patient', document.getElementsByClassName('delete'));
    let $editElement = document.getElementsByClassName('edit');

    for (let i = 0; i < $editElement.length; i++) {
        $editElement[i].onclick = function () {

            let $response = JSONreq('patient', 'fetch', {
                'id': $(this).attr('id')
            });

            setPatientModelData($response);
        }
    }
    // end table content -->


    /*
     * Pagination
     */
    // pagination -->
    let $html = '<li class="page-item ' + ($pagi.previous_page === '#' ? $prev = 'disabled' : '') + '">' +
        '<a class="page-link" href="?procedure_id=<?=$procedure_id?>&name=<?=$name?>&page=' + $pagi.previous_page + '" tabindex="-1" aria-disabled="true">' +
        'Previous' +
        '</a>' +
        '</li>';

    $buttons = $pagi.buttons;
    $.each($buttons, function (index, value) {
        if (index === 'active') {
            $html += '<li class="page-item active">' +
                '<a class="page-link" href="?procedure_id=<?=$procedure_id?>&name=<?=$name?>&page=' + value + '">' + value + '</a>' +
                '</li>';
        } else {
            $html += '<li class="page-item">' +
                '<a class="page-link" href="?procedure_id=<?=$procedure_id?>&name=<?=$name?>&page=' + value + '">' + value + '</a>' +
                '</li>';
        }
    });

    $html += '<li class="page-item ' + ($pagi.next_page === '#' ? $next = 'disabled' : '') + '">' +
        '<a class="page-link" href="?procedure_id=<?=$procedure_id?>&name=<?=$name?>&page=' + $pagi.next_page + '" tabindex="-1" aria-disabled="true">' +
        'Next' +
        '</a>' +
        '</li>';

    $(".pagination").append($html);
    // end pagination -->


</script>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myExtraLargeModalLabel" class="modal fade bd-example-modal-lg" id="edit"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <script> let $request_to = 'slip_dental'; </script>
            <?php
            include 'model_patient.php';
            ?>
        </div>
    </div>
</div>
<!-- /Modal -->