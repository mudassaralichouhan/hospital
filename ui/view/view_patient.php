<?php

session_start();
include '../../vendor/autoload.php';


$title = 'Patient information';
include('../include/wraper start.php');

$id = isset($_GET['id']) ? $_GET['id'] : exit;

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3>Patient view</h3>
                        <p class="text-muted">
                            More patient information view can also be created with this method.
                        </p>
                    </header>
                    <div class="widget-content py-3">
                        <div class="row">
                            <div class="form-group col-md-7"><span class="badge badge-secondary">Patient Id</span>
                                <h3 class="h3" id="id"></h3>
                            </div>
                            <div class="form-group col-md-2"><span class="badge badge-secondary">Register Date</span>
                                <h3 class="h3" id="patient-register-date"></h3>
                            </div>
                            <div class="form-group col-md-3"><span class="badge badge-secondary">Last Modifed</span>
                                <h3 class="h3" id="patient-update"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">User</span>
                                <h3 class="h3" id="patient-user"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">Name</span>
                                <h3 class="h3" id="patient-name"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">Age</span>
                                <h3 class="h3" id="patient-age"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">Gender</span>
                                <h3 class="h3" id="patient-gender"></h3>
                            </div>
                            <div class="form-group col-md-12"><span class="badge badge-secondary">Contact</span>
                                <h3 class="h3" id="patient-contact"></h3>
                            </div>

                            <div class="form-group col-12"><span class="badge badge-secondary">Address</span>
                                <h3 class="h3" id="patient-address"></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3>Procedure view</h3>
                        <p class="text-muted">
                            More Consultant <span class="consultant-name"></span> information view can also be created with this method.
                        </p>
                    </header>
                    <div class="widget-content py-3">
                        <div class="row">
                            <!-- table -->
                            <div class="col-lg-12 mb-12">
                                <table class="table table-striped table-hover">
                                    <thead class="text-muted" id="header">
                                    </thead>
                                    <tbody id="data">
                                    </tbody>
                                </table>
                            </div>
                            <!-- /table -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myExtraLargeModalLabel" class="modal fade bd-example-modal-lg" id="edit"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <script> let $request_to = 'slip_dental'; </script>
            <?php
            include 'model_slip.php';
            ?>
        </div>
    </div>
</div>
<!-- /Modal -->
<script src="<?= $url ?>/ui/public/js/patient.js"></script>
<script>

    $url = $baseURL+'/ui/view/view_product.php';

    let data = JSONreq('patient','fetch',{id:<?=$id?>});
    displayPatient(data);

    let $response = JSONreq('patient', 'patient_slip', {
        id : <?=$id?>
    });

    let $table_headers = $response['headers'];
    let $table_data = $response['data'];
    // end get data from server -->

    // append data to table - table content -->
    let $row = '<tr>';
    $row += '<th> # </th>';
    for (let $i = 0; $i < $table_headers.length; $i++) {
        $row += '<th>' + $table_headers[$i] + '</th>';
    }
    $row += '<th>Action</th>';
    $row += '</tr>';
    $('#header').append($row);

    for (let $i = 0; $i < Object.keys($table_data).length; $i++) {
        $row = '<tr>';

        jQuery.each($table_data[$i], function (index, item) {

            if(index === 'reg_on') {
                $row += '<td>' + readableDate(item) + '</td>';
            } else if(index === 'type_of') {
                if('Dental Ship' === $table_data[$i].name) {
                    $request_to = 'procedure_dental';
                    $row += '<td>'
                        + '<a href="'+$url+'?procedure_id='+$table_data[$i].type_of[1]+'&name='+$request_to+'">'
                        + $table_data[$i].type_of[0]+'</a>'
                        + '</td>';
                } else if('Lab Slip' === $table_data[$i].name) {
                    $request_to = 'procedure_lab_test';
                    $row += '<td>'
                        + '<a href="'+$url+'?procedure_id='+$table_data[$i].type_of[1]+'&name='+$request_to+'">'
                        + $table_data[$i].type_of[0]+'</a>'
                        + '</td>';
                } else if('X Ray Slip' === $table_data[$i].name) {
                    $request_to = 'procedure_xray';
                    $row += '<td>'
                        + '<a href="'+$url+'?procedure_id='+$table_data[$i].type_of[1]+'&name='+$request_to+'">'
                        + $table_data[$i].type_of[0]+'</a>'
                        + '</td>';
                } else if('Surgical Slip' === $table_data[$i].name) {
                    $request_to = 'procedure_surgical';
                    $row += '<td>'
                        + '<a href="'+$url+'?procedure_id='+$table_data[$i].type_of[1]+'&name='+$request_to+'">'
                        + $table_data[$i].type_of[0]+'</a>'
                        + '</td>';
                }
            } else {
                $row += '<td>' + item + '</td>';
            }
        });
        $row += '<td>' + actionIcon($table_data[$i].id) + '</td>';
        $row += '</tr>';

        $('#data').append($row);
    }

    deleteById('slip_dental', document.getElementsByClassName('delete'));
    let $editElement = document.getElementsByClassName('edit');

    for (let i = 0; i < $editElement.length; i++) {
        $editElement[i].onclick = function () {

            let req = this.parentElement.parentElement.children[1].textContent;

            if('Dental Ship' === req) {
                $request_to = 'slip_dental';
                let $response = JSONreq($request_to, 'fetch', {
                    id: $(this).attr('id')
                });
                setProductToSelectElement($procedureFee, 'procedure_dental');
                setSlipModelData($response);

            } else if('Lab Slip' === req) {
                $request_to = 'slip_lab_test';
                let $response = JSONreq($request_to, 'fetch', {
                    id: $(this).attr('id')
                });
                setProductToSelectElement($procedureFee, 'procedure_lab_test');
                setSlipModelData($response);

            } else if('X Ray Slip' === req) {
                $request_to = 'slip_xray';
                let $response = JSONreq($request_to, 'fetch', {
                    id: $(this).attr('id')
                });
                setProductToSelectElement($procedureFee, 'procedure_xray');
                setSlipModelData($response);

            } else if('Surgical Slip' === req) {
                $request_to = 'slip_surgical';
                let $response = JSONreq($request_to, 'fetch', {
                    id: $(this).attr('id')
                });
                setProductToSelectElement($procedureFee, 'procedure_surgical');
                setSlipModelData($response);

            }

        }
    }
    // end table content -->

</script>
