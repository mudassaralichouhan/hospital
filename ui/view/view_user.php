<?php

session_start();
include '../../vendor/autoload.php';

if(is_admin()) {
    header("location: ".$_SERVER['HTTP_HOST']);
}

$title = 'User information';
include('../include/wraper start.php');

$id = isset($_GET['id']) ? $_GET['id'] : exit;
$page = isset($_GET['page']) ? $_GET['page'] : 1;

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3>User view</h3>
                        <p class="text-muted">
                            More User information view can also be created with this method.
                        </p>
                    </header>
                    <div class="widget-content py-3">
                        <div class="row">
                            <div class="form-group col-md-7"><span class="badge badge-secondary">Consultant Id</span>
                                <h3 class="h3" id="id"></h3>
                            </div>
                            <div class="form-group col-md-2"><span class="badge badge-secondary">Register Date</span>
                                <h3 class="h3" id="user-register-date"></h3>
                            </div>
                            <div class="form-group col-md-3"><span class="badge badge-secondary">Last Modifed</span>
                                <h3 class="h3" id="user-update"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">Name</span>
                                <h3 class="h3 user-name"></h3>
                            </div>
                            <div class="form-group col-md-4"><span class="badge badge-secondary">Age</span>
                                <h3 class="h3" id="user-age"></h3>
                            </div>
                            <div class="form-group col-md-6"><span class="badge badge-secondary">Gender</span>
                                <h3 class="h3" id="user-gender"></h3>
                            </div>
                            <div class="form-group col-md-12"><span class="badge badge-secondary">Contact</span>
                                <h3 class="h3" id="user-contact"></h3>
                            </div>
                            <div class="form-group col-12"><span class="badge badge-secondary">Address</span>
                                <h3 class="h3" id="user-address"></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">
                    <header>
                        <h3>Patient of user view</h3>
                        <p class="text-muted">
                            More user information <span class="user-name"></span> view can also be created with this method.
                        </p>
                    </header>
                    <div class="widget-content py-3" id="get-patient-form">
                        <div class="row">
                            <!-- table -->
                            <div class="col-lg-12 mb-12">
                                <table class="table table-striped table-hover">
                                    <thead class="text-muted" id="header">
                                    </thead>
                                    <tbody id="data">
                                    </tbody>
                                </table>
                            </div>
                            <!-- /table -->

                            <!-- pagination -->
                            <div class="container">
                                <nav aria-label="...">
                                    <ul class="pagination">
                                    </ul>
                                </nav>
                            </div>
                            <!-- /pagination -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>

<script src="<?= $url ?>/ui/public/js/user.js"></script>
<script src="<?= $url ?>/ui/public/js/consultant.js"></script>

<script>
    let response = JSONreq('patient', 'fetch', {
        id: <?=$id?>
    });
    displayUser(response);

    $response = JSONreq('patient', 'patient_limit_user', {
        'page': <?=$page?>,
        id : <?=$id?>
    });

    let $table_headers = $response['headers'];
    let $table_data = $response['data'];
    let $pagi = $response['pagination'];
    // end get data from server -->

    // append data to table - table content -->
    let $row = '<tr>';
    $row += '<th> # </th>';
    for (let $i = 0; $i < $table_headers.length; $i++) {
        $row += '<th>' + $table_headers[$i] + '</th>';
    }
    $row += '<th>Action</th>';
    $row += '</tr>';
    $('#header').append($row);

    for ($i = 0; $i < Object.keys($table_data).length; $i++) {
        $row = '<tr>';

        $table_data[$i]['name'] = '<a href="view_patient.php?id=' + $table_data[$i]['id'] +
            '">' + $table_data[$i]['name'] + '</a>';
        $table_data[$i]['consultant'] = '<a href="view_consultant.php?id=' + $table_data[$i]['consultant'][0] +
            '">' + $table_data[$i]['consultant'][1] + '</a>';

        jQuery.each($table_data[$i], function (index, item) {

            if(index === 'reg_on') {
                $row += '<td>' + readableDate(item) + '</td>';
            } else if (index === 'update_on') {
                $row += '<td>' + readableDate(item) + '</td>';
            } else {
                $row += '<td>' + item + '</td>';
            }
        });
        $row += '<td>' + actionIcon($table_data[$i].id) + '</td>';
        $row += '</tr>';

        $('#data').append($row);
    }

    deleteById('patient', document.getElementsByClassName('delete'));
    let $editElement = document.getElementsByClassName('edit');

    for (let i = 0; i < $editElement.length; i++) {
        $editElement[i].onclick = function () {

            let $response = JSONreq('patient', 'fetch', {
                id: $(this).attr('id')
            });
            setPatientModelData($response);
        }
    }
    // end table content -->

    // pagination -->
    let $html = '<li class="page-item ' + ($pagi.previous_page === '#' ? $prev = 'disabled' : '') + '">' +
        '<a class="page-link" href="?id=<?=$id?>&page=' + $pagi.previous_page + '" tabindex="-1" aria-disabled="true">' +
        'Previous' +
        '</a>' +
        '</li>';

    $buttons = $pagi.buttons;
    $.each($buttons, function (index, value) {
        if (index === 'active') {
            $html += '<li class="page-item active">' +
                '<a class="page-link" href="?id=<?=$id?>&page=' + value + '">' + value + '</a>' +
                '</li>';
        } else {
            $html += '<li class="page-item">' +
                '<a class="page-link" href="?id=<?=$id?>&page=' + value + '">' + value + '</a>' +
                '</li>';
        }
    });

    $html += '<li class="page-item ' + ($pagi.next_page === '#' ? $next = 'disabled' : '') + '">' +
        '<a class="page-link" href="?id=<?=$id?>&page=' + $pagi.next_page + '" tabindex="-1" aria-disabled="true">' +
        'Next' +
        '</a>' +
        '</li>';

    $(".pagination").append($html);
    // end pagination -->
</script>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myExtraLargeModalLabel" class="modal fade bd-example-modal-lg" id="edit"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php
            include 'model_patient.php';
            ?>
        </div>
    </div>
</div>
<!-- /Modal -->
