/*
 *
 */

$(function () {
    let $suggest = $('#suggestions');
    let $instanceQuery = $('#instance-query');

    $instanceQuery.keyup(function (evt) {
        $suggest.empty();
        if (evt.which !== 27) {
            let $i = 0;
            let $html = '';
            let $response = JSONreq('search', 'suggest', {'key': this.value});
            for ($i = 0; $i < $response.length; $i++) {
                $html += '<a class="list-group-item list-group-item-action" href="<?=$url?>/ui/view/view_patient.php?id=' +
                    $response[$i].id + '">' + $response[$i].name + '</a>';
            }
            $html += '<a class="list-group-item list-group-item-action" href="'+$baseURL+'/ui/search/search.php?keys='+
                this.value+'"> More information... </a>';
            $suggest.append($html);
        }
    });

    $(window).click(function () {
        $suggest.empty();
    });

});
const actionIcon = (id) => {
    return '<i id="' + id + '" class="edit fa fa-pencil-square" aria-hidden="true" data-toggle="modal" data-target="#edit" data-placement="left" title="Edit"></i> ' +
        '<i id="' + id + '" class="delete fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Delete"></i>';
};

const deleteIcon = (id) => {
    return '<i id="' + id + '" class="delete fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Delete"></i>';
};

const deleteById = (sendReqFor, HTMLelements, method = 'get') => {
    for (let i = 0; i < HTMLelements.length; i++) {
        HTMLelements[i].onclick = function () {

            let parentElement = this.parentElement.parentElement; // this line is static

            if (confirm('Do you want Delete!!! is totally upto you.')) {
                let response = JSONreq(sendReqFor, 'delete', {
                    id : $(this).attr('id')
                }, method);
                parentElement.style.display = "none";
            }

        } // end click
    } // end for-loop
};

dueclick = 0;
function dueSlipList() {

    if(dueclick) {
        return false;
    }
    dueclick = 1;

    let response = JSONreq('slip','due');

    document.getElementById('noti-slip').innerHTML = response['notif'];

    if(response.status) {
        let html = '';
        let slip_data = response['data'];
        for(let i=0; i<slip_data.length; i++) {
            html += '<li class="media d-flex pl-4 pr-4 pt-3 pb-3">' +
                '                        <div class="media-body">' +
                '                            <h4>'+slip_data[i]['name']+'</h4>' +
                '                            <p>Due amount: <b>'+slip_data[i]['remain_amt']+'</b></p>' +
                '                            <div>' +
                '                                <a href="'+$baseURL+'/ui/view/view_patient.php?id='+slip_data[i]['patient_id']+'" class="btn btn-icon-sm btn-primary rounded-circle">' +
                '                                    <i class="fa fa-eye"></i>' +
                '                                </a>' +
                '                                <span class="float-right text-muted mb-2"><small>'+readableDate(slip_data[i]['reg_on'])+'</small></span>' +
                '                            </div>' +
                '                        </div>' +
                '                    </li><hr/>';
        }

        html +=
            '                <div class="p-4 d-block">' +
            '                    <footer>' +
            '                        <a href="'+$baseURL+'/ui/slip/due.php">See '+response['total_slips']+' More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>' +
            '                    </footer>' +
            '                </div><br/>';

        $('#slip-noti').html(html);
    }
}

todayPatientsclick = 0;
function todayPatients() {
    if(todayPatientsclick) {
        return false;
    }
    todayPatientsclick = 1;

    let response = JSONreq('patient','today');

        let html = '';
        for(let i=0; i<response.length; i++) {
            html += '<li class="media d-flex pl-4 pr-4 pt-3 pb-4">\n' +
                '      <div class="media-body">\n' +
                '          <a href="#">\n' +
                '              <div class="d-flex w-100 justify-content-between">\n' +
                '                  <h4 class="mb-1 mr-4">'+response[i].name+'</h4> - <span>'+response[i].gender+'</span>\n' +
                '              </div>\n' + response[i].location+
                '          </a>\n' +
                '      </div>\n' +
                '  </li>';
        }

        $('#today-patient-noti').html(html);
        console.log(response);
}

const setProfilePhoto = () => {

    if(localStorage['profile_photo'] !== undefined) {
        $('#profile-photo-icon').attr('src', localStorage['profile_photo']);
        $('#profile-photo').attr('src', localStorage['profile_photo']);
        return false;
    }

    $(document).ready(function () {
        $.ajax({
            url: $baseURL + '/ajax_request.php',
            method: 'post',
            dataType: 'json',
            data: {
                switch_to: 'user', // define on define.php
                action: 'profile_photo_fetch', // define on define.php
            },
            success: function (response) {
                if(response.status) {
                    console.log(image.msg);
                    localStorage['profile_photo'] = response.msg;
                    $('#profile-photo-icon').attr('src', response.msg);
                    $('#profile-photo').attr('src',response.msg);
                }
            },
            error: function (err) {
                console.log('response error maybe(404)');
                console.log('server is down get');

                let html = '<div class="alert alert-info" role="alert">' +
                    "Server not response right <strong>note: </strong> maybe(404)" +
                    '</div>';
                notification(html, 60000);
            }
        });
    });
};
// setProfilePhoto();