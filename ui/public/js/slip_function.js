/*
 *
 */

$procedureFee = [];

const insertSlip = (nameSlip) => {

    if(document.getElementById('procedure-id').value==='')
    {
        document.getElementById('procedure-err').style.color = 'red';
        document.getElementById('procedure-err').innerHTML = 'requried';
        document.getElementById('procedure-id').style.borderColor = 'red';
        return;
    }

    let response = JSONreq(nameSlip,'insert',{
        'product_id':document.getElementById('procedure-id').value,
        'patient_id':document.getElementById('patient-id').innerText.match(/(\d+)/)[0],
        'discount':document.getElementById('discount').value,
        'get_amount':document.getElementById('get-amount').value
    });

    if(response.status) {
    }

    if (response.status) {

        let $html = '<div class="alert alert-dark" role="alert">' +
            'Your record is Update <b>Successfully</b> with all condition' +
            '</div>';

        notification($html);

        document.getElementById('insert-slip-btn').disabled = true;
    } else {

        let $html = '<div class="alert alert-danger" role="alert">' +
            'Some Thing was wrong!' +
            '</div>';

        notification($html);
    }
};

const getPatientSlipInfo = (input, err) => { // usage for slip

    if (isNumeric(input, err)===false) {
        return false;
    }

    input = document.getElementById(input);
    err = document.getElementById(err);

    let patient = JSONreq('patient', 'fetch', {
        'id': input.value
    });

    if (patient.id) {
        document.getElementById('patient-name').append(patient.name);
        document.getElementById('patient-id').append(patient.id);
        document.getElementById('consultant-name').append(patient.consultant);
        document.getElementById('patient-contact').append(patient.contact);
        document.getElementById('patient-gender').append(patient.gender);
        document.getElementById('patient-age').append(patient.age);

        $('#get-patient-form').hide();
        $('#get-patient-result').show();

    }else if (patient.status === false) {
        input.style.borderColor = '#ced4da';
        err.style.color = 'red';
        err.innerHTML = 'About id patient found error';
    }
    return true;

};

const setProductToSelectElement = (fee, procedureName) => {

    if(localStorage[procedureName] === undefined) {
        localStorage[procedureName] = req(procedureName, 'name_fee');
    }

    let nameFee = JSON.parse(localStorage[procedureName]);

    let procedure = $('#procedure-id');
    console.log(procedure);
    procedure.empty();
    procedure.append('<option value=""> --- Select Procedure --- <option>');
    for (i = 0; i < nameFee.length; i++) {
        procedure.append('<option value="' + nameFee[i].id + '">' + nameFee[i].name + '<option>');
        fee.push([nameFee[i].id, nameFee[i].price]);
    }
};

const setSlipPaidAmt = () => {

    let lable = document.getElementById('price');
    let id = document.getElementById('procedure-id');
    let discount = document.getElementById('discount');

    for (i = 0; i < $procedureFee.length; i++) {
        f = $procedureFee[i];
        if (f[0] === id.value) {
            f = (f[1] / 100) * (100 - discount.value);
            lable.innerHTML = f + ' Rs.';
            break;
        }
    }
};

const setSlipModelData = ($data) => {

    /*
     * re-set Data
     */
    document.getElementById('id').innerHTML = '';
    document.getElementById('register-date').innerHTML = '';
    document.getElementById('update').innerHTML = '';
    document.getElementById('user').innerHTML = '';
    document.getElementById('patient-name').innerHTML = '';
    document.getElementById('consultant-name').innerHTML = '';
    document.getElementById('already-get-amount').innerHTML = '';

    document.getElementById('procedure-id').value = '';
    document.getElementById('discount').value = 0;
    document.getElementById('get-amount').value = 0; // remaining amount
    document.getElementById('updateBtn').disabled=false;
    $('.alert').remove();

    /*
     * set Data
     */

    document.getElementById('id').innerHTML = $data.id;
    document.getElementById('patient-name').innerHTML = $data.patient_id;
    document.getElementById('register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('update').innerHTML = readableDate($data.update_on);
    document.getElementById('user').innerHTML = $data.user_id;
    document.getElementById('already-get-amount').innerHTML = ($data.price / 100) * (100 - $data.discount)-$data.remain_amt+'.Rs';
    document.getElementById('price').innerHTML = ($data.price / 100) * (100 - $data.discount)+'.Rs';

    document.getElementById('procedure-id').value = $data.type_of;
    document.getElementById('discount').value = $data.discount;
    document.getElementById('get-amount').value = $data.remain_amt; // remaining amount
    document.getElementById('price').value = $data.remain_amt;

};

const updateSlip = (nameSlip) => {
    let response = JSONreq(nameSlip, 'update', {

        'id' : document.getElementById('id').innerText,
        'product_id': document.getElementById('procedure-id').value,
        'discount': document.getElementById('discount').value,
        'get_amount': document.getElementById('get-amount').value

    });

    $('.alert').remove();

    if (response.status) {
        $html = '<div class="alert alert-dark" role="alert">' +
            'Your record is Update <b>Successfully</b> with all condition' +
            '</div>';
        $('#status').append($html);

        document.getElementById('updateBtn').disabled = true;
    } else {

        $html = '<div class="alert alert-danger" role="alert">' +
            'Some Thing was wrong!' +
            '</div>';
        $('#status').append($html);
    }
};
