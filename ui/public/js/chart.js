/*
 *
 */

if(localStorage['chart_last_12_months'] === undefined) {
    localStorage['chart_last_12_months'] = [
        req('chart_info', 'last_12_months')
    ];
}
let chart_last_12_month_names = JSON.parse(localStorage['chart_last_12_months']).last_month_names;
for(let i=0; i<chart_last_12_month_names.length; i++) {
    chart_last_12_month_names[i] = chart_last_12_month_names[i].slice(0,3);
    console.log();
}

if(localStorage['chart_last_year_patient_data'] === undefined) {
    localStorage['chart_last_year_patient_data'] = [
        req('chart_info', 'last_year_patient_data')
    ];
}

new Chart('chart-line', {
    type: 'line',
    data: {
        labels: chart_last_12_month_names,
        datasets: [{
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: '#EC807A',
            data: JSON.parse(localStorage['chart_last_year_patient_data']).data
        }]
    },
    options: {
        maintainAspectRatio: false,
        elements: {
            line: {
                tension: 0.4,
                "borderWidth": 2
            }
        },
        legend: {display: false},
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "#999999"
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: "#999999"
                }
            }]
        }
    }
});

new Chart('chart-bar', {
    type: 'bar',
    data: {
        labels: chart_last_12_month_names,
        datasets: [{
            backgroundColor: '#27A9E0',
            data: JSON.parse(localStorage['chart_last_year_patient_data']).data
        }]
    },
    options: {
        maintainAspectRatio: false,
        legend: {display: false}
    }
});

// let $last_year_slips_count = request('chart_info','last_year_slips_count');

if(localStorage['chart_last_year_slips_count'] === undefined) {
    localStorage['chart_last_year_slips_count'] = [
        req('chart_info','last_year_slips_count')
    ];
}

new Chart('chart-bar-horizontal', {
    type: 'horizontalBar',
    data: {
        labels: chart_last_12_month_names,

        datasets: [{
                backgroundColor: '#FF675F',
                data: JSON.parse(localStorage['chart_last_year_slips_count'])['dental']
        },
        {
            backgroundColor: '#F9CE5E',
            data: JSON.parse(localStorage['chart_last_year_slips_count'])['sugical']
        },

        {
            backgroundColor: '#71C7C1',
            data: JSON.parse(localStorage['chart_last_year_slips_count'])['lab']
        },

        {
            backgroundColor: '#FF679F',
            data: JSON.parse(localStorage['chart_last_year_slips_count'])['xray']
        }]
    },

options: {
    maintainAspectRatio: false,
    legend: {display: false}
    }
});
