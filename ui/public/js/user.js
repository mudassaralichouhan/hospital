/*
 *
 */

const setUserModelData = ($data) => {

    /*
     * remove text who all ready printed
     */
    document.getElementById('id').innerHTML = '';
    document.getElementById('user-register-date').innerHTML = '';
    document.getElementById('user-update').innerHTML = '';

    document.getElementById('user-name').value = '';
    document.getElementById('user-age').value = '';
    document.getElementById('user-address').value = '';

    document.getElementById('name-err').innerHTML = '';
    document.getElementById('age-err').innerHTML = '';
    document.getElementById('gender-err').innerHTML = '';
    document.getElementById('updateBtn').disabled=false;
    $('.alert').remove();

    /*
     * set Data
     */

    document.getElementById('id').innerHTML = $data.id;
    document.getElementById('user-register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('user-update').innerHTML = readableDate($data.update_on);
    document.getElementById('user-email').innerHTML = $data.email;
    document.getElementById('user-contact').innerHTML = $data.contact;

    document.getElementById('user-name').value = $data.name;
    document.getElementById('user-age').value = $data.age;
    document.getElementById('user-address').value = $data.location;

    document.getElementById('user-rule').value = $data.rule;

    if($data.gender === 'Male')
    {
        document.getElementsByClassName('user-gender')[1].checked = true;
    }
    else
    {
        document.getElementsByClassName('user-gender')[0].checked = true;
    }
    

};

const updateUser = () => {

    let response = JSONreq('user', 'update', {
        'user_name':document.getElementById('user-name').value,
        'user_gender':document.getElementsByClassName('user-gender')[1].checked === true?'Male' : 'Femail',
        'user_age':document.getElementById('user-age').value,
        'user_address':document.getElementById('user-address').value,
        'user_rule':document.getElementById('user-rule').value
    }, 'post');

    $('.alert').remove();

    if (response.status) {
        let html = '<div class="alert alert-dark" role="alert">' +
            'Your record is Update <b>Successfully</b> with all condition' +
            '</div>';
        notification(html);
        document.getElementById('updateBtn').disabled = true;

    } else {

        let html = '<div class="alert alert-danger" role="alert">' +
            'Some Thing was wrong!' +
            '</div>';
        notification(html);
    }
};

function changeEmail() {
    let user = JSONreq('user', 'change_email', {
        email: document.getElementById('emails-new-email').value
}, 'post');

    $('.alert').remove();

    if (user.status) {
        notification('<div class="alert alert-success">' + user['msg'] + '</div>');
        setTimeout(() => {
            $('.alert').remove();
            document.getElementById('emails-new-email').value = '';
        }, 3000);
    } else {
        notification('<div class="alert alert-danger">' + user['msg'] + '</div>');
    }
}

function updateProfile() {
    let what = 1;

    let nameErr = document.getElementById('profile-name-err');
    let name = document.getElementById('profile-name');

    let contactErr = document.getElementById('profile-contact-err');
    let contact = document.getElementById('profile-contact');

    let ageErr = document.getElementById('profile-age-err');
    let age = document.getElementById('profile-age');

    let genderErr = document.getElementById('profile-gender-err');
    let gender = document.getElementsByClassName('profile-gender');
    let g = '';

    if (name.value.length <= 3) {
        name.style.borderColor = 'red';
        nameErr.style.color = 'red';
        nameErr.innerHTML = 'Require Name Between 3 to 30';
        what = 0;
    }

    if (contact.value.length !== 11) {
        contact.style.borderColor = 'red';
        contactErr.style.color = 'red';
        contactErr.innerHTML = 'Require Contact valid-format';
        what = 0;
    }

    if (age.value === "") {
        age.style.borderColor = 'red';
        ageErr.style.color = 'red';
        ageErr.innerHTML = 'Select Age';
        what = 0;
    }

    if (gender[1].checked === false && gender[0].checked === false) {
        genderErr.style.color = 'red';
        genderErr.innerHTML = 'Select Gender';
        what = 0;
    }

    if (what === 1) {
        let address = document.getElementById('profile-location');

        if (gender[0].checked === true) {
            g = gender[0].value;
        } else if (gender[1].checked === true) {
            g = gender[1].value;
        }

        let response = JSONreq('user', 'update', {
            user_name: name.value,
            user_gender: g,
            user_contact: contact.value,
            user_address: address.value,
            user_age: age.value
    }, 'post');

        $('.alert').remove();

        if (response.status) {
            let html = '<div class="alert alert-success" role="alert">' +
                'Your record is Store <b>Successfully</b> with all condition' +
                '</div>';
            notification(html);
        } else {

            html = '<div class="alert alert-danger" role="alert">' +
                'Some Thing was wrong!' +
                '</div>';
            notification(html);
        }
    }
}

function userLog() {
    let user = JSONreq('user', 'user_log', '', 'post');

    let $table_headers = user['headers'];
    let $table_data = user['log'];

    let $row = '<tr>';
    for ($i = 0; $i < $table_headers.length; $i++) {

        $row += '<th>' + $table_headers[$i] + '</th>';
    }
    $row += '<th>Action</th>';
    $row += '</tr>';
    $('#header').append($row);

    for (let $i = 0; $i < Object.keys($table_data).length; $i++) {
        let $row = '<tr>';
        jQuery.each($table_data[$i], function (index, item) {
            if (index==='reg_on') {
                $row += '<td>' + readableDate(item); + '</td>';
            } else if (index==='id') {

            } else {
                $row += '<td>' + item + '</td>';
            }
        });
        $row += '<td>' + deleteIcon($table_data[$i]['id']) + '</td>';
        $row += '</tr>';

        $('#data').append($row);
    }

    let HTMLelements = document.getElementsByClassName('delete');
    for (let i = 0; i < HTMLelements.length; i++) {
        HTMLelements[i].onclick = function () {

            let parentElement = this.parentElement.parentElement; // this line is static

            if (confirm('Do you want Delete!!! is totally upto you.')) {
                let response = JSONreq('user', 'user_log_delete', {
                    'id': $(this).attr('id')
                }, 'post');
                parentElement.style.display = "none";
            }

        } // end click
    } // end for-loop
}

function displayUser(data) {

    document.getElementById('id').innerHTML = data.id;
    document.getElementById('user-register-date').innerHTML = readableDate(data.reg_on);
    document.getElementById('user-update').innerHTML = readableDate(data.update_on);

    document.getElementsByClassName('user-name')[0].innerHTML = data.name;
    document.getElementsByClassName('user-name')[1].innerHTML = data.name;
    document.getElementById('user-age').innerHTML = data.age;
    document.getElementById('user-contact').innerHTML = data.contact;
    document.getElementById('user-address').innerHTML = data.location;
    document.getElementById('user-gender').innerHTML = data.gender;
}

function changeSetting() {
    let buttons = document.getElementById('pagination-buttons');
    let pageContent = document.getElementById('pagination-page-content');

    let response = JSONreq('user', 'user_setting', {
            id: user_id,
        button: buttons.value,
        rec_per_page: pageContent.value,
        }, 'post');
}

function deactive() {

    let response = JSONreq('user','delete',{
        password : document.getElementById('password').value
    }, 'POST');
    if(response.status) {
        localStorage.clear();
        window.location = $baseURL;
    } else {
        $('#status').append('<div class="alert alert-danger" role="alert">' +
                response.msg
            + '</div>');
    }
}

const setProfile = () => {
    let user = JSONreq('user', 'fetch', '', 'post');
    document.getElementById('user-last-login').innerHTML = readableDate(user['last_login']);
    document.getElementById('user-contact').innerHTML = user['contact'];
    document.getElementById('user-email').innerHTML = user['email'];
    document.getElementById('emails-email').value = user['email'];

    document.getElementById('profile-name').value = user['name'];
    if (user.gender === 'Male')
        document.getElementsByClassName('profile-gender')[1].checked = true;
    else
        document.getElementsByClassName('profile-gender')[0].checked = true;
    document.getElementById('profile-age').value = user['age'];
    document.getElementById('profile-contact').value = user['contact'];
    document.getElementById('profile-location').value = user['location'];
};

const uploadProfilePhoto = () => {
    imageUpload('user', 'profile_photo_insert', {
        'image': [document.getElementById('image').files[0]],
    });
    setTimeout(setProfilePhoto(),500);
};
