/*
 *
 */

localStorage.clear();

const login = () => {

    let response = JSONreq('user','login',{
        'email': document.getElementById('login-email').value,
        'password': document.getElementById('login-password').value
    }, 'post');

    $('.alert').remove();

    if (response.status) {

        let $html = '<div class="alert alert-dark" role="alert">' +
            'login <b>Successfully</b> with all condition' +
            '</div>';
        $('#login-notifications').append($html);

        $('button').disabled = true;
        setTimeout(window.location='index.php', 1000);
        localStorage.clear();

    } else {
        // console.log(response);
        $('#login-notifications').append('<div class="alert alert-danger" role="alert">' + response.msg + '</div>');
    }
    return false;
};

const register = () => {
    let valid = true;

    // password
    let password = document.getElementById('register-password');
    let password2 = document.getElementById('register-password2');
    let passwordErr = document.getElementById('password-err');
    if(password.value === password2.value && password.value.length >= 6)
    {
        passwordErr.innerHTML = '';
    } else {
        passwordErr.style.color = 'red';
        passwordErr.innerHTML = 'These your password are not same!!!';
        valid = false;
    }

    // name
    let fullname = document.getElementById('register-name');
    let nameErr = document.getElementById('name-err');
    if(fullname.value.length<3)
    {
        nameErr.style.color = 'red';
        nameErr.innerHTML = 'your name length is less then 3 character\'s';
        valid = false;
    } else if(fullname.value.length>=3)
    {
        nameErr.innerHTML = '';
    }

    // gender
    let male = document.getElementById('register-male');
    let female = document.getElementById('register-female');
    let genderErr = document.getElementById('gender-err');
    let gender = '';
    if(male.checked)
    {
        gender = male;
        genderErr.innerHTML = '';
    } else if(female.checked){
        gender = female;
        genderErr.innerHTML = '';
    }
    else{
        genderErr.style.color = 'red';
        genderErr.innerHTML = 'Select a your Gender';
        valid = false;
    }

    let email = document.getElementById('register-email');
    let emailErr = document.getElementById('email-err');
    if(/^[a-zA-z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.value)){
        emailErr.innerHTML = '';
    }else{
        emailErr.style.color = 'red';
        emailErr.innerHTML = 'In-Valid Email';
    }
    if(email.value.indexOf('@') === -1)
    {
        if(/^((\(((\+|00)92)\)|(\+|00)92)(( |\-)?)(3[0-9]{2})\6|0(3[0-9]{2})( |\-)?)[0-9]{3}( |\-)?[0-9]{4}$/.test(email.value)){
            emailErr.innerHTML = '';
        }else{
            emailErr.style.color = 'red';
            emailErr.innerHTML = 'In-Valid Mobile Number';
            valid = false;
        }
    }

    if(valid===true) {
        let response = JSONreq('user','insert',{
            'name': fullname.value,
            'gender': gender.value,
            'email': email.value,
            'password': password.value,
        }, 'post');

        $('.alert').remove();

        if(response.status)
        {
            $('#register-notifications').append('<div class="alert alert-success" role="alert">' +
                'Register <b>Successfully</b> with all condition' +
            '</div>');
            $('button').disabled = true;
        }
        else
        {
            $('#register-notifications').append('<div class="alert alert-danger" role="alert">' +
                response.msg +
            '</div>');
            $('button').disabled = true;
        }
    }

    return false;
};