/*
 *
 */

// setTimeout(()=>{localStorage.clear()},10000);

if(localStorage['chart_last_12_months'] === undefined) {
    localStorage['chart_last_12_months'] = [
        req('chart_info', 'last_12_months')
    ];
}
let chart_last_12_month_names = JSON.parse(localStorage['chart_last_12_months']).last_month_names;
for(let i=0; i<chart_last_12_month_names.length; i++) {
    chart_last_12_month_names[i] = chart_last_12_month_names[i].slice(0,3);
}

if(localStorage['chart_last_year_patient_data'] === undefined) {
    localStorage['chart_last_year_patient_data'] = req('chart_info', 'last_year_patient_data')
}

last_month_names = chart_last_12_month_names;
data = JSON.parse(localStorage['chart_last_year_patient_data']).data;

new Chart('chart-bar', {
    type: 'bar',
    data: {
        labels: last_month_names,
        datasets: [{
            backgroundColor: '#27A9E0',
            data: data
        }]
    },
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "#999999"
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: "#999999"
                }
            }]
        }
    }
});

    if(localStorage['dashborad_info'] === undefined) {
        localStorage['dashborad_info'] = req('dashborad_info', '');
    }
    let response = JSON.parse(localStorage['dashborad_info']);

    document.getElementById('patient-total').prepend(response.patient_info.total);
    document.getElementById('patient-symbol').innerHTML = ' ' + response.patient_info.symbol;

    document.getElementById('consultant-total').prepend(response.consultant_info.total);
    document.getElementById('consultant-symbol').innerHTML = ' ' + response.consultant_info.symbol;

    document.getElementById('user-total').prepend(response.user_info.total);
    document.getElementById('user-symbol').innerHTML = ' ' + response.user_info.symbol;

    document.getElementById('dental-total').prepend(response.dental.total);
    document.getElementById('dental-symbol').innerHTML = ' ' + response.dental.symbol;
    document.getElementById('dental-outof-perc').innerHTML = response.dental.perc + '%';

    document.getElementById('surgical-total').prepend(response.surgical.total);
    document.getElementById('surgical-symbol').innerHTML = ' ' + response.surgical.symbol;
    document.getElementById('surgical-outof-perc').innerHTML = response.surgical.perc + '%';

    document.getElementById('labtest-total').prepend(response.labtest.total);
    document.getElementById('labtest-symbol').innerHTML = ' ' + response.labtest.symbol;
    document.getElementById('labtest-outof-perc').innerHTML = response.labtest.perc + '%';

    document.getElementById('xray-total').prepend(response.xray.total);
    document.getElementById('xray-symbol').innerHTML = ' ' + response.xray.symbol;
    document.getElementById('xray-outof-perc').innerHTML = response.xray.perc + '%';