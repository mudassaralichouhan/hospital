/*
 *
 */

const setProductToModelForEdit = (sendReqFor, editElement, HTMLelements) => {
    for (let i = 0; i < HTMLelements.length; i++) {

        HTMLelements[i].onclick = function () {
            let response = JSONreq(sendReqFor, 'fetch', {
                'id' : $(this).attr('id')
            });

            for(let i=0, keys = Object.keys(editElement); i<keys.length; i++) {
                document.getElementById(keys[i]).value = response[editElement[keys[i]]];
            }
            document.getElementById('edit-id').value = $(this).attr('id');
        }

    }
};

const updateProcedure = (req) => {

    let name = document.getElementById('edit-name');
    let price = document.getElementById('edit-price');
    let id = document.getElementById('edit-id');
    let nameErr = document.getElementById('edit-name-err');
    let priceErr = document.getElementById('edit-fee-err');

    if (name.value.length >= 3) {

        let response = JSONreq(req, 'update', {
            'product_name': name.value,
            'product_price': price.value,
            'id': id.value
        });

        if (response.status) {
            $('#edit').modal('hide');
            $afterHeader = $('.widget-block').find('header');

            $html = '<div class="alert alert-dark" role="alert">' +
                'Your record is Update <b>Successfully</b>' +
                '</div>';

            $afterHeader.append($html);

            name.value = '';
            price.value = '';
            name.style.borderColor = '#ced4da';
            price.style.borderColor = '#ced4da';
            nameErr.style.borderColor = '#ced4da';
            nameErr.innerHTML = '';
            priceErr.innerHTML = '';
        } else {
            name.style.borderColor = 'red';
            nameErr.style.borderColor = 'red';
            nameErr.style.color = 'red';
            nameErr.innerHTML = 'Procedure not avalible for register';
        }
    } else {
        name.style.borderColor = 'red';
        nameErr.style.borderColor = 'red';
        nameErr.style.color = 'red';
        nameErr.innerHTML = 'Name is Require';
    }
};

const insertProcedure = (req) => {

    let name = document.getElementById('name');
    let price = document.getElementById('price');
    let nameErr = document.getElementById('name-err');
    let feeErr = document.getElementById('fee-err');

    if (name.value.length >= 3) {

        let response = JSONreq(req,'insert', {
            'product_name': name.value,
            'product_price': price.value
        });

        $('.alert').remove();
        if (response.status) {
            $afterHeader = $('.widget-block').find('header');

            $html = '<div class="alert alert-dark" role="alert">' +
                'Your record is Store <b>Successfully</b> with all condition' +
                '</div>';

            $afterHeader.append($html);

            name.value = '';
            price.value = '';
            name.style.borderColor = '#ced4da';
            price.style.borderColor = '#ced4da';
            nameErr.innerHTML = '';
            priceErr.innerHTML = '';

        } else if (response.status === false) {
            $afterHeader = $('.widget-block').find('header');

            $html = '<div class="alert alert-danger" role="alert">' +
                'This Record is <b>already</b> inserted in passed on base of contact#.' +
                'So Change the cred and try another way!' +
                '</div>';

            $afterHeader.append($html);

            name.style.borderColor = 'red';
            nameErr.style.color = 'red';
            nameErr.innerHTML = 'already exist';
        } else {
            $afterHeader = $('.widget-block').find('header');

            $html = '<div class="alert alert-danger" role="alert">' +
                'Some Thing was wrong!' +
                '</div>';

            $afterHeader.append($html);
        }


    } else {
        name.style.borderColor = 'red';
        let err = document.getElementById('name-err');
        err.style.borderColor = 'red';
        err.style.color = 'red';
        err.innerHTML = 'Name is required';
    }
};

const appendProductToTalbe = (response, container, offset) => {

    if (response.status !== false) {
        for (let i = 0, y = offset + 1; i < response.length; i++, y++) {
            let $tableRow = '<tr>' +
                '<th scope="row">' + response[i].id + '</th>' +
                '<td>' + response[i].name + '</td>' +
                '<td>' + response[i].price + '</td>' +
                '<td>' + readableDate(response[i].reg_on) + '</td>' +
                '<td>' + readableDate(response[i].update_on) + '</td>' +
                '<td>' + actionIcon(response[i].id) + '</td>' +
            '</tr>';

            container.append($tableRow);
        }
    }
};

const displayProduct = ($data) => {

    document.getElementById('id').innerHTML = $data.name;
    document.getElementById('product-register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('product-update').innerHTML = readableDate($data.update_on);
    document.getElementById('product-user').innerHTML = $data.user_id[0];
};
