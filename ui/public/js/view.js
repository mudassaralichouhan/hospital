// get data from server -->

(function () {

    let $response = JSONreq($request_to, 'limit', {
            'page': $page
    }, $method);

    let $table_headers = $response['headers'];
    let $table_data = $response['data'];
    let $pagi = $response['pagination'];
    // end get data from server -->

    // append data to table - table content -->
    let $row = '<tr>';
    $row += '<th> # </th>';
    for (let $i = 0; $i < $table_headers.length; $i++) {
        $row += '<th>' + $table_headers[$i] + '</th>';
    }
    $row += '<th>Action</th>';
    $row += '</tr>';
    $('#header').append($row);

    for (let $i = 0; $i < Object.keys($table_data).length; $i++) {
        $row = '<tr>';

        switch ($request_to) {
            case 'consultant':
                $table_data[$i]['name'] = '<a href="view_consultant.php?id=' + $table_data[$i]['id'] +
                    '">' + $table_data[$i]['name'] + '</a>';
                break;
            case "patient":
                $table_data[$i]['name'] = '<a href="view_patient.php?id=' + $table_data[$i]['id'] +
                    '">' + $table_data[$i]['name'] + '</a>';

                $table_data[$i]['consultant'] = '<a href="view_consultant.php?id=' + $table_data[$i]['consultant'][0] +
                    '">' + $table_data[$i]['consultant'][1] + '</a>';
                break;
            case 'user':
                $table_data[$i]['name'] = '<a href="view_user.php?id=' + $table_data[$i]['id'] +
                    '">' + $table_data[$i]['name'] + '</a>';
                break;
            case 'slip_dental':
                $table_data[$i]['patient_id'] = '<a href="view_patient.php?id=' + $table_data[$i]['patient_id'][1] +
                    '">' + $table_data[$i]['patient_id'][0] + '</a>';
                $table_data[$i]['type_of'] = '<a href="view_product.php?procedure_id='+$table_data[$i]['type_of'][1]+'&name=procedure_dental">' +
                    $table_data[$i]['type_of'][0] + '</a>';
                break;
            case 'slip_surgical':
                $table_data[$i]['patient_id'] = '<a href="view_patient.php?id=' + $table_data[$i]['patient_id'][1] +
                    '">' + $table_data[$i]['patient_id'][0] + '</a>';
                $table_data[$i]['type_of'] = '<a href="view_product.php?procedure_id='+$table_data[$i]['type_of'][1]+'&name=procedure_surgical">' +
                    $table_data[$i]['type_of'][0] + '</a>';
                break;
            case 'slip_lab_test':
                $table_data[$i]['patient_id'] = '<a href="view_patient.php?id=' + $table_data[$i]['patient_id'][1] +
                    '">' + $table_data[$i]['patient_id'][0] + '</a>';
                $table_data[$i]['type_of'] = '<a href="view_product.php?procedure_id='+$table_data[$i]['type_of'][1]+'&name=procedure_lab_test">' +
                    $table_data[$i]['type_of'][0] + '</a>';
                break;
            case 'slip_xray':

                $table_data[$i]['patient_id'] = '<a href="view_patient.php?id=' + $table_data[$i]['patient_id'][1] +
                    '">' + $table_data[$i]['patient_id'][0] + '</a>';
                $table_data[$i]['type_of'] = '<a href="view_product.php?procedure_id='+$table_data[$i]['type_of'][1]+'&name=procedure_xray">' +
                    $table_data[$i]['type_of'][0] + '</a>';

                break;
        }

        $.each($table_data[$i], function (index, item) {

            if(index === 'reg_on') {
                $row += '<td>' + readableDate(item) + '</td>';
            } else if (index === 'update_on') {
                $row += '<td>' + readableDate(item) + '</td>';
            } else {
                $row += '<td>' + item + '</td>';
            }
        });
        $row += '<td>' + actionIcon($table_data[$i].id) + '</td>';
        $row += '</tr>';

        $('#data').append($row);
    }

    deleteById($request_to, document.getElementsByClassName('delete'), $method);
    let $editElement = document.getElementsByClassName('edit');

    for (let i = 0; i < $editElement.length; i++) {
        $editElement[i].onclick = function () {

            $response = JSONreq($request_to, 'fetch', {
                    'id': $(this).attr('id')
                }, $method);

            /*
             * responser set model
             */
            switch ($request_to) {
                case 'consultant':
                    setConsultantModelData($response);
                    break;
                case "patient":
                    setPatientModelData($response);
                    break;
                case 'user':
                    setUserModelData($response);
                    break;
                case 'slip_dental':
                case 'slip_surgical':
                case 'slip_lab_test':
                case 'slip_xray':
                    setSlipModelData($response);
                    if ('slip_lab_test' === $request_to) {
                        setProductToSelectElement($procedureFee, 'procedure_lab_test');
                    } else if ('slip_xray' === $request_to) {
                        setProductToSelectElement($procedureFee, 'procedure_xray');
                    } else if ('slip_surgical' === $request_to) {
                        setProductToSelectElement($procedureFee, 'procedure_surgical');
                    } else if ('slip_dental' === $request_to) {
                        setProductToSelectElement($procedureFee, 'procedure_dental');
                    }
                    break;

                default:
                    break;

            }
        }
    }
    // end table content -->


    /*
     * Pagination
     */
    // pagination -->
    let $html = '<li class="page-item ' + ($pagi.previous_page === '#' ? $prev = 'disabled' : '') + '">' +
        '<a class="page-link" href="?m='+$method+'&r='+$request_to+'&page=' + $pagi.previous_page + '" tabindex="-1" aria-disabled="true">' +
        'Previous' +
        '</a>' +
        '</li>';

    $buttons = $pagi.buttons;
    $.each($buttons, function (index, value) {
        if (index === 'active') {
            $html += '<li class="page-item active">' +
                '<a class="page-link" href="?m='+$method+'&r='+$request_to+'&page=' + value + '">' + value + '</a>' +
                '</li>';
        } else {
            $html += '<li class="page-item">' +
                '<a class="page-link" href="?m='+$method+'&r='+$request_to+'&page=' + value + '">' + value + '</a>' +
                '</li>';
        }
    });

    $html += '<li class="page-item ' + ($pagi.next_page === '#' ? $next = 'disabled' : '') + '">' +
        '<a class="page-link" href="?m='+$method+'&r='+$request_to+'&page=' + $pagi.next_page + '" tabindex="-1" aria-disabled="true">' +
        'Next' +
        '</a>' +
        '</li>';

    $(".pagination").append($html);
    // end pagination -->
})();