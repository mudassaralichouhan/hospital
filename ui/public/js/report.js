/*
 *
 */
function consultantReport() {

    let from = document.getElementById('from');
    let to = document.getElementById('to');
    let id = document.getElementById('patient-consultant-id');

    let response = JSONreq('report', 'get_consultant_report', {
        'from': from.value,
        'to': to.value,
        'id': id.value
    });

    $('.alert').remove();

    if (response.status === false) {
        let html = '<div class="alert alert-danger" role="alert">' + response.msg + '</div>';
        $('#status').html(html);
        reportBox.hide();
        return false;
    }

    reportBox.show();

    document.getElementById('total-patient').innerHTML = response.patient.total;
    document.getElementById('profite-patient').innerHTML = response.patient.profite;
    document.getElementById('profite-outof-patient').innerHTML = response.patient.outof;
    document.getElementById('discount-patient').innerHTML = response.patient.discount_avg;
    document.getElementById('lost-amt-patient').innerHTML = response.patient.lose;
    document.getElementById('floated-total-patient').innerHTML = response.patient.total / 1000;

    document.getElementById('total-dental').innerHTML = response.dental.total;
    document.getElementById('profite-dental').innerHTML = response.dental.profite;
    document.getElementById('profite-outof-dental').innerHTML = response.dental.outof;
    document.getElementById('discount-dental').innerHTML = response.dental.discount_avg;
    document.getElementById('lost-amt-dental').innerHTML = response.dental.lose;
    document.getElementById('floated-total-dental').innerHTML = response.dental.total / 1000;

    document.getElementById('total-surgical').innerHTML = response.surgical.total;
    document.getElementById('profite-surgical').innerHTML = response.surgical.profite;
    document.getElementById('profite-outof-surgical').innerHTML = response.surgical.outof;
    document.getElementById('discount-surgical').innerHTML = response.surgical.discount_avg;
    document.getElementById('lost-amt-surgical').innerHTML = response.surgical.lose;
    document.getElementById('floated-total-surgical').innerHTML = response.surgical.total / 1000;

    document.getElementById('total-xray').innerHTML = response.xray.total;
    document.getElementById('profite-xray').innerHTML = response.xray.profite;
    document.getElementById('profite-outof-xray').innerHTML = response.xray.outof;
    document.getElementById('discount-xray').innerHTML = response.xray.discount_avg;
    document.getElementById('lost-amt-xray').innerHTML = response.xray.lose;
    document.getElementById('floated-total-xray').innerHTML = response.xray.total / 1000;

    document.getElementById('total-lab').innerHTML = response.lab.total;
    document.getElementById('profite-lab').innerHTML = response.lab.profite;
    document.getElementById('profite-outof-lab').innerHTML = response.lab.outof;
    document.getElementById('discount-lab').innerHTML = response.lab.discount_avg;
    document.getElementById('lost-amt-lab').innerHTML = response.lab.lose;
    document.getElementById('floated-total-lab').innerHTML = response.lab.total / 1000;

    document.getElementById('id').innerHTML = response.consultant_info.id;
    document.getElementById('consultant-register-date').innerHTML = response.consultant_info.reg_on;
    document.getElementById('consultant-update').innerHTML = response.consultant_info.update_on;
    document.getElementById('consultant-user').innerHTML = response.consultant_info.user_id;
    document.getElementById('consultant-perc').innerHTML = response.consultant_info.consultant_perc;
    document.getElementById('consultant-dental-perc').innerHTML = response.consultant_info.dental_perc;
    document.getElementById('consultant-surgical-perc').innerHTML = response.consultant_info.surgical_perc;
    document.getElementById('consultant-lab-perc').innerHTML = response.consultant_info.lab_perc;
    document.getElementById('consultant-xray-perc').innerHTML = response.consultant_info.xray_perc;

}

function userReport() {

    let from = document.getElementById('from');
    let to = document.getElementById('to');

    let response = JSONreq('report', 'get_user_report', {
        'from': from.value,
        'to': to.value,
    });

    $('.alert').remove();

    if (response.status === false) {
        let html = '<div class="alert alert-danger" role="alert">' + response.msg + '</div>';
        $('#status').html(html);
        return false;
    }

    document.getElementById('total-patient').innerHTML = response.patient.total;
    document.getElementById('profite-patient').innerHTML = response.patient.profite;
    document.getElementById('profite-outof-patient').innerHTML = response.patient.outof;
    document.getElementById('discount-patient').innerHTML = response.patient.discount_avg;
    document.getElementById('lost-amt-patient').innerHTML = response.patient.lose;
    document.getElementById('floated-total-patient').innerHTML = response.patient.total / 1000;

    document.getElementById('total-dental').innerHTML = response.dental.total;
    document.getElementById('profite-dental').innerHTML = response.dental.profite;
    document.getElementById('profite-outof-dental').innerHTML = response.dental.outof;
    document.getElementById('discount-dental').innerHTML = response.dental.discount_avg;
    document.getElementById('lost-amt-dental').innerHTML = response.dental.lose;
    document.getElementById('floated-total-dental').innerHTML = response.dental.total / 1000;

    document.getElementById('total-surgical').innerHTML = response.surgical.total;
    document.getElementById('profite-surgical').innerHTML = response.surgical.profite;
    document.getElementById('profite-outof-surgical').innerHTML = response.surgical.outof;
    document.getElementById('discount-surgical').innerHTML = response.surgical.discount_avg;
    document.getElementById('lost-amt-surgical').innerHTML = response.surgical.lose;
    document.getElementById('floated-total-surgical').innerHTML = response.surgical.total / 1000;

    document.getElementById('total-xray').innerHTML = response.xray.total;
    document.getElementById('profite-xray').innerHTML = response.xray.profite;
    document.getElementById('profite-outof-xray').innerHTML = response.xray.outof;
    document.getElementById('discount-xray').innerHTML = response.xray.discount_avg;
    document.getElementById('lost-amt-xray').innerHTML = response.xray.lose;
    document.getElementById('floated-total-xray').innerHTML = response.xray.total / 1000;

    document.getElementById('total-lab').innerHTML = response.lab.total;
    document.getElementById('profite-lab').innerHTML = response.lab.profite;
    document.getElementById('profite-outof-lab').innerHTML = response.lab.outof;
    document.getElementById('discount-lab').innerHTML = response.lab.discount_avg;
    document.getElementById('lost-amt-lab').innerHTML = response.lab.lose;
    document.getElementById('floated-total-lab').innerHTML = response.lab.total / 1000;

    document.getElementById('id').innerHTML = response.user_info.id;
    document.getElementById('user-register-date').innerHTML = response.user_info.reg_on;
    document.getElementById('user-update').innerHTML = response.user_info.update_on;
    document.getElementById('user-user').innerHTML = response.user_info.user_id;
    document.getElementById('user-perc').innerHTML = response.user_info.user_perc;
    document.getElementById('user-dental-perc').innerHTML = response.user_info.dental_perc;
    document.getElementById('user-surgical-perc').innerHTML = response.user_info.surgical_perc;
    document.getElementById('user-lab-perc').innerHTML = response.user_info.lab_perc;
    document.getElementById('user-xray-perc').innerHTML = response.user_info.xray_perc;

}