/*
 *
 */

let feeArr = [];

function setNameFeeOfConsultant() {
    if(localStorage['consultant_name_fee'] === undefined) {
        localStorage['consultant_name_fee'] = req('consultant', 'name_fee');
    }
    let response = localStorage['consultant_name_fee'];
    response = JSON.parse(response);

    for (i = 0; i < response.length; i++) {
        $('#patient-consultant-id').append('<option value="' + response[i].id + '">' + response[i].name + '<option>');
        feeArr.push([response[i].id, response[i].fee]);
    }
}

const patientInsert = (btn) => {
    let what = 1;

    let consutantErr = document.getElementById('consultant-err');
    let consutantId = document.getElementById('patient-consultant-id');

    let nameErr = document.getElementById('name-err');
    let name = document.getElementById('patient-name');

    let contactErr = document.getElementById('contact-err');
    let contact = document.getElementById('patient-contact');

    let ageErr = document.getElementById('age-err');
    let age = document.getElementById('patient-age');

    let genderErr = document.getElementById('gender-err');
    let gender = document.getElementsByClassName('patient-gender');
    let g = '';

    if (consutantId.value === "") {
        consutantId.style.borderColor = 'red';
        consutantErr.style.color = 'red';
        consutantErr.innerHTML = 'Select Consultant';
        what = 0;
    }

    if (name.value.length <= 3) {
        name.style.borderColor = 'red';
        nameErr.style.color = 'red';
        nameErr.innerHTML = 'Require Name Between 3 to 30';
        what = 0;
    }

    if (contact.value.length !== 11) {
        contact.style.borderColor = 'red';
        contactErr.style.color = 'red';
        contactErr.innerHTML = 'Require Contact valid-format';
        what = 0;
    }

    if (age.value === "") {
        age.style.borderColor = 'red';
        ageErr.style.color = 'red';
        ageErr.innerHTML = 'Select Consultant';
        what = 0;
    }

    if (gender[1].checked === false && gender[0].checked === false) {
        genderErr.style.color = 'red';
        genderErr.innerHTML = 'Select Gender';
        what = 0;
    }

    if (what === 1) {
        let discount = document.getElementById('patient-discount');
        let address = document.getElementById('patient-address');

        if (gender[0].checked === true) {
            g = gender[0].value;
        } else if (gender[1].checked === true) {
            g = gender[1].value;
        }

        let response = JSONreq('patient', 'insert', {
            'patient_name': name.value,
            'patient_gender': g,
            'patient_contact': contact.value,
            'patient_address': address.value,
            'patient_age': age.value,
            'patient_consultant_id': consutantId.value,
            'patient_discount': discount.value,
        });

        if (response.status) {
            notification('<div class="alert alert-dark" role="alert">' +
                'Your record is Store <b>Successfully</b> with all condition' +
                '</div>');

            btn.disabled = true;
            setTimeout(window.location.reload(), 7*1000);
        } else {
            notification('<div class="alert alert-danger" role="alert">' +
                'Some Thing was wrong!' +
                '</div>');
        }
    }
};

const patientPaidAmt = () => {
    let lable = document.getElementById('patient-paid');
    let id = document.getElementById('patient-consultant-id');
    let discount = document.getElementById('patient-discount');

    for (i = 0; i < feeArr.length; i++) {
        f = feeArr[i];
        if (f[0] === id.value) {
            f = (f[1] / 100) * (100 - discount.value);
            lable.innerHTML = f + ' Rs.';
            break;
        }
    }
};

const setPatientModelData = ($data) => {

    /*
     * remove text who all ready printed
     */
    document.getElementById('id').innerHTML = '';
    document.getElementById('patient-register-date').innerHTML = '';
    document.getElementById('patient-update').innerHTML = '';
    document.getElementById('patient-user').innerHTML = '';

    document.getElementById('patient-name').value = '';
    document.getElementById('patient-age').value = '';
    document.getElementById('patient-name').value = '';
    document.getElementById('patient-contact').value = '';
    document.getElementById('patient-discount').value = '';
    document.getElementById('patient-paid').innerHTML = '';
    document.getElementById('patient-consultant-id').value = '';
    document.getElementById('patient-address').value = '';

    document.getElementById('consultant-err').innerHTML = '';
    document.getElementById('name-err').innerHTML = '';
    document.getElementById('contact-err').innerHTML = '';
    document.getElementById('age-err').innerHTML = '';
    document.getElementById('gender-err').innerHTML = '';
    document.getElementById('discount-err').innerHTML = '';
    document.getElementById('updateBtn').disabled=false;
    $('.alert').remove();

    /*
     * set Data
     */

    document.getElementById('id').innerHTML = $data.id;
    document.getElementById('patient-register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('patient-update').innerHTML = readableDate($data.update_on);
    document.getElementById('patient-user').innerHTML = $data.user_id;

    document.getElementById('patient-name').value = $data.name;
    document.getElementById('patient-age').value = $data.age;
    document.getElementById('patient-name').value = $data.name;
    document.getElementById('patient-contact').value = $data.contact;
    document.getElementById('patient-discount').value = $data.discount;
    document.getElementById('patient-paid').innerHTML = $data.paid;
    document.getElementById('patient-consultant-id').value = $data.consultant;
    document.getElementById('patient-address').value = $data.location;
    if($data.gender === 'Male') {
        document.getElementsByClassName('patient-gender')[1].checked = true;
    } else {
        document.getElementsByClassName('patient-gender')[0].checked = true;
    }
};
const displayPatient = ($data) => {

    document.getElementById('id').innerHTML = $data.id;
    document.getElementById('patient-register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('patient-update').innerHTML = readableDate($data.update_on);
    document.getElementById('patient-user').innerHTML = $data.user_id;

    document.getElementById('patient-name').innerHTML = $data.name;
    document.getElementById('patient-age').innerHTML = $data.age;
    document.getElementById('patient-contact').innerHTML = $data.contact;
    document.getElementById('patient-address').innerHTML = $data.location;
    document.getElementById('patient-gender').innerHTML = $data.gender;
};

const updatePatient = () => {
    let response = JSONreq('patient', 'update', {

        'patient_name': document.getElementById('patient-name').value,
        'id' : document.getElementById('id').innerText,
        'patient_consultant_id': document.getElementById('patient-consultant-id').value,
        'patient_contact': document.getElementById('patient-contact').value,
        'patient_age': document.getElementById('patient-age').value,
        'patient_gender': document.getElementsByClassName('patient-gender')[1].checked === true?'Male' : 'Female',
        'patient_discount': document.getElementById('patient-discount').value,
        'patient_address': document.getElementById('patient-address').value

    });

    $('.alert').remove();

    if (response.status) {
        $html = '<div class="alert alert-dark" role="alert">' +
            'Your record is Update <b>Successfully</b> with all condition' +
            '</div>';
        $('#status').append($html);

        document.getElementById('updateBtn').disabled = true;
    } else {

        $html = '<div class="alert alert-danger" role="alert">' +
            'Some Thing was wrong!' +
            '</div>';
        $('#status').append($html);
    }
};