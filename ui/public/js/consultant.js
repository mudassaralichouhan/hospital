/*
 *
 */

const setSpacializtion = ()=>{
    if(localStorage['procedure_spacializ_list'] === undefined) {
        localStorage['procedure_spacializ_list'] = req('procedure_spacializ', 'list');
    }
    let response = localStorage['procedure_spacializ_list'];
    response = JSON.parse(response);

    for (i = 0; i < response.length; i++) {
        $('#consultant-specialization').append('<option value="' + response[i].id + '">' + response[i].name + '<option>');
    }
};

const consultant = () => {
    let what = 1;

    let specializationErr = document.getElementById('specialization-err');
    let specialization = document.getElementById('consultant-specialization');

    let nameErr = document.getElementById('name-err');
    let name = document.getElementById('consultant-name');

    let ageErr = document.getElementById('consultant-age-err');
    let age = document.getElementById('consultant-age');

    let contactErr = document.getElementById('contact-err');
    let contact = document.getElementById('consultant-contact');

    let genderErr = document.getElementById('gender-err');
    let gender = document.getElementsByClassName('consultant-gender');
    let g = '';

    if (specialization.value === "") {
        specialization.style.borderColor = 'red';
        specializationErr.style.color = 'red';
        specializationErr.innerHTML = 'Select Consultant';
        what = 0;
    }

    if (name.value.length <= 3) {
        name.style.borderColor = 'red';
        nameErr.style.color = 'red';
        nameErr.innerHTML = 'Require Name Between 3 to 30';
        what = 0;
    }

    if (age.value === '') {
        age.style.borderColor = 'red';
        ageErr.style.color = 'red';
        ageErr.innerHTML = 'Require Age';
        what = 0;
    }

    if (contact.value.length !== 11) {
        contact.style.borderColor = 'red';
        contactErr.style.color = 'red';
        contactErr.innerHTML = 'Require Contact valid-format';
        what = 0;
    }

    if (gender[1].checked === false && gender[0].checked === false) {
        genderErr.style.color = 'red';
        genderErr.innerHTML = 'Select Gender';
        what = 0;
    }

    if (what === 1) {

        let fee = document.getElementById('consultant-fee');
        let consultant_per = document.getElementById('consultant-perc');
        let lab_per = document.getElementById('consultant-lab-perc');
        let dental_per = document.getElementById('consultant-dental-perc');
        let xray_per = document.getElementById('consultant-xray-perc');
        let surgical_per = document.getElementById('consultant-surgical-perc');
        let address = document.getElementById('consultant-address');
        if (gender[0].checked === true) {
            g = gender[0].value;
        } else if (gender[1].checked === true) {
            g = gender[1].value;
        }

            let response = JSONreq('consultant', 'insert', {

                'consult_specializ': specialization.value,
                'consult_name': name.value,
                'consult_age': age.value,
                'consult_contact': contact.value,
                'consult_gender': g,
                'consult_fee': fee.value,

                'consult_consultant_per': consultant_per.value,
                'consult_lab_per': lab_per.value,
                'consult_dental_per': dental_per.value,
                'consult_surgical_per': surgical_per.value,
                'consult_xray_per': xray_per.value,

                'consult_address': address.value
            });
        $(()=>{
            $('.alert').remove();

            if (response.status) {
                $div = $('.widget-content > *').remove();

                $html = '<div class="alert alert-dark" role="alert">' +
                    'Your record is Store <b>Successfully</b> with all condition' +
                    '</div>';

                $('.widget-block').append($html);
            } else if (response.status === false) {
                $afterHeader = $('.widget-block').find('header');

                $html = '<div class="alert alert-danger" role="alert">' +
                    'This Record is <b>already</b> inserted in passed on base of contact#.' +
                    'So Change the cred and try another way! ' +
                    response.msg +
                    '</div>';

                $afterHeader.append($html);
                contact.style.borderColor = 'red';
            } else {
                $afterHeader = $('.widget-block').find('header');

                $html = '<div class="alert alert-danger" role="alert">' +
                    'Some Thing was wrong!' +
                    '</div>';

                $afterHeader.append($html);
            }
        });
    }
};

const setConsultantModelData = ($data = '') => {

    /*
     * set Data
     */

    document.getElementById('id').innerHTML = $data.id;
    document.getElementById('consultant-register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('consultant-update').innerHTML = readableDate($data.update_on);
    document.getElementById('consultant-user').innerHTML = $data.user_id;

    document.getElementById('consultant-specialization').value = $data.specialization;
    document.getElementById('consultant-name').value = $data.name;
    document.getElementById('consultant-age').value = $data.age;
    document.getElementById('consultant-contact').value = $data.contact;
    document.getElementById('consultant-fee').value = $data.fee;
    document.getElementById('consultant-address').value = $data.location;

    document.getElementById('consultant-perc').value = $data.consultant_perc;
    document.getElementById('consultant-lab-perc').value = $data.lab_perc;
    document.getElementById('consultant-dental-perc').value = $data.dental_perc;
    document.getElementById('consultant-surgical-perc').value = $data.surgical_perc;
    document.getElementById('consultant-xray-perc').value = $data.xray_perc;

    if($data.gender === 'Male') {
        document.getElementsByClassName('consultant-gender')[1].checked = true;
    } else {
        document.getElementsByClassName('consultant-gender')[0].checked = true;
    }

    /*
     * remove text who all ready printed
     */
    document.getElementById('updateBtn').disabled = false;
    $('.alert').remove();
};

const displayConsultant = ($data = '') => {

    document.getElementById('id').innerHTML = $data.id;
    document.getElementById('consultant-register-date').innerHTML = readableDate($data.reg_on);
    document.getElementById('consultant-update').innerHTML = readableDate($data.update_on);
    document.getElementById('consultant-user').innerHTML = $data.user_id;

    document.getElementById('consultant-specialization').innerHTML = $data.specialization;
    document.getElementsByClassName('consultant-name')[0].innerHTML = $data.name;
    document.getElementsByClassName('consultant-name')[1].innerHTML = $data.name;
    document.getElementById('consultant-age').innerHTML = $data.age;
    document.getElementById('consultant-contact').innerHTML = $data.contact;
    document.getElementById('consultant-fee').innerHTML = $data.fee;
    document.getElementById('consultant-address').innerHTML = $data.location;

    document.getElementById('consultant-perc').innerHTML = $data.consultant_perc;
    document.getElementById('consultant-lab-perc').innerHTML = $data.lab_perc;
    document.getElementById('consultant-dental-perc').innerHTML = $data.dental_perc;
    document.getElementById('consultant-surgical-perc').innerHTML = $data.surgical_perc;
    document.getElementById('consultant-xray-perc').innerHTML = $data.xray_perc;
    document.getElementById('consultant-gender').innerHTML = $data.gender;

};

const updateConsultant = () => {
    let response = JSONreq('consultant', 'update', {
        'id': document.getElementById('id').innerText,
        'consult_specializ':document.getElementById('consultant-specialization').value,
        'consult_name':document.getElementById('consultant-name').value,
        'consult_gender':document.getElementsByClassName('consultant-gender')[1].checked === true?'Male' : 'Femail',
        'consult_age':document.getElementById('consultant-age').value,
        'consult_contact':document.getElementById('consultant-contact').value,
        'consult_address':document.getElementById('consultant-address').value,
        'consult_fee':document.getElementById('consultant-fee').value,

        'consult_consultant_per':document.getElementById('consultant-perc').value,
        'consult_lab_per':document.getElementById('consultant-lab-perc').value,
        'consult_xray_per':document.getElementById('consultant-xray-perc').value,
        'consult_surgical_per':document.getElementById('consultant-surgical-perc').value,
        'consult_dental_per':document.getElementById('consultant-dental-perc').value
    });

    $('.alert').remove();

    if (response.status) {
        $html = '<div class="alert alert-dark" role="alert">' +
            'Your record is Update <b>Successfully</b> with all condition' +
            '</div>';
        $('#status').append($html);

        document.getElementById('updateBtn').disabled = true;
    } else {

        $html = '<div class="alert alert-danger" role="alert">' +
            'Some Thing was wrong!' +
            '</div>';
        $('#status').append($html);
    }
};

