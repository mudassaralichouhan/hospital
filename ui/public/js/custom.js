/* 
 * -----------------------------------------------------
 *  custom method
 * -----------------------------------------------------
 */

$(() => {
    $('form').on('submit', function (e) {
        e.preventDefault();
    });
});

$('#get-patient-result').hide();

const notification = (noti, dely = 5000) => {
    let notification = $('#notifications');
    notification.empty();
    notification.append(noti);
    setTimeout(() => {
        notification.empty();
    }, dely);
};

const JSONreq = (switch_to, action, passArrToServer = [], method = 'GET') => {

    let res = [];

    console.log(method+' requesting... ');

    $.ajax({
        url: $baseURL + '/ajax_request.php',
        method: method,
        async: false,
        data: {
            switch_to: switch_to, // define on define.php
            action: action, // define on define.php
            data_arr: passArrToServer // define on define.php
        },
        success: function (response) {
            try {
                res = JSON.parse(response);

            } catch (e) {
                console.log('Exception is occuree');

                let html = '<div class="alert alert-danger" role="alert">' +
                    "Server response but not right <strong>note: </strong> maybe(syntex)" +
                    '</div>';
                notification(html, 60000);
            }
        },
        error: function (err) {
            console.log('response error maybe(404)');
            console.log('server is down get');

            let html = '<div class="alert alert-danger" role="alert">' +
                "Server not response right <strong>note: </strong> maybe(404)" +
                '</div>';
            notification(html, 60000);
        }
    });
    if(res.status === false)
        notification('<div class="alert alert-danger" role="alert">\n' + res.msg + '</div>');
    return res;
};

const req = (switch_to, action, passArrToServer = []) => {

    let res = [];

    $.ajax({
        url: $baseURL + '/ajax_request.php',
        method: 'GET',
        async: false,
        data: {
            switch_to: switch_to, // define on define.php
            action: action, // define on define.php
            data_arr: passArrToServer // define on define.php
        },
        success: function (response) {
                res = response
        }
    });
    return res;
};

const imageUpload = (switch_to, action, passArrToServer = {}) => {

    var form_data = new FormData();

    form_data.append('switch_to',switch_to);
    form_data.append('action',action);
    //form_data.append('data_arr', passArrToServer);

    for(let i=0, keys = Object.keys(passArrToServer); i<keys.length; i++) {
        form_data.append(keys[i], passArrToServer[keys[i]][0]);
    }
    $.ajax({
        url: $baseURL + '/ajax_request.php',
        method: 'post',
        async: false,
        cache : false,
        contentType: false,
        processData: false,
        data: form_data,
    });
};

const isNumeric = (input, err) => {
    //var regExp = /^0[0-9].*$/;

    input = document.getElementById(input);
    err = document.getElementById(err);

    if (input.value > 0) {
        input.style.borderColor = 'green';
        err.style.color = 'green';
        err.innerHTML = 'Valid: ' + input.value;

        return true; // is must use for getPatientSlipInfo() method
    }

    input.style.borderColor = 'red';
    err.style.color = 'red';
    err.innerHTML = 'ID Require: ' + input.value;

    return false;
};

const chkSelect = (input, err, msg) => {

    input = document.getElementById(input);
    err = document.getElementById(err);

    if (input.value !== "" && input.value !== '0') {
        input.style.borderColor = 'green';
        err.style.color = 'green';
        err.innerHTML = msg + input.options[input.selectedIndex].text;
    } else {
        input.style.borderColor = '#ced4da';
        err.style.color = '#ced4da';
        err.innerHTML = '';
    }
};

const chkRadio = (radio = this, err) => {

    radio = document.getElementById(radio);
    err = document.getElementById(err);

    err.innerHTML = radio.value;
    err.style.color = 'green';
};

const isEmpty = (input, err, msg = '') => {

    input = document.getElementById(input);
    err = document.getElementById(err);

    if (input.value === '0') {
        input.style.borderColor = '#ced4da';
        err.style.color = '#ced4da';
        err.innerHTML = '';
    } else if (input.value < '0') {
        input.style.borderColor = 'red';
        err.style.color = 'red';
        err.innerHTML = 'Nagtive Value not Allow';
    } else if (input.value !== "") {
        input.style.borderColor = 'green';
        err.style.color = 'green';
        err.innerHTML = msg + input.value;
    }
};

const pkTel = (input, err) => {

    err = document.getElementById(err);
    input = document.getElementById(input);

    if (/^((\(((\+|00)92)\)|(\+|00)92)(( |\-)?)(3[0-9]{2})\6|0(3[0-9]{2})( |\-)?)[0-9]{3}( |\-)?[0-9]{4}$/.test(input.value)) {
        input.style.borderColor = 'green';
        err.style.color = 'green';
        err.innerHTML = input.value;
    } else {
        input.style.borderColor = 'red';
        err.style.color = 'red';
        err.innerHTML = 'Invalid format';
    }
};

const strLenth = (input, err, max, min) => {
    input = document.getElementById(input);
    err = document.getElementById(err);

    if (input.value.length >= min) {
        input.style.borderColor = 'green';
        err.style.color = 'green';
        err.innerHTML = input.value;
    } else {
        input.style.borderColor = '#ced4da';
        err.style.color = '#ced4da';
        err.innerHTML = ' ';
    }
};

const readableDate = (date_time, mssage = 'nothing to Change') => {

    if (date_time === null) {
        return mssage;
    }
    date_time = date_time.split(' ');

    let date = date_time[0].split('-');
    let time = date_time[1].split(':');

    let today_date = new Date();
    let curr_hour = today_date.getHours();
    let curr_min = today_date.getMinutes();

    let curr_year = today_date.getFullYear();
    let curr_month = today_date.getMonth() + 1;
    let curr_day = today_date.getDate();

    /*
     * this sequance is important
     */

    // 2015 < 2020 year
    if (date[0] < curr_year) {
        let count = curr_year - date[0];
        return count + ' year ago';
    }

    // 07 < 09 month
    if (date[1] < curr_month) {
        let count = curr_month - date[1];
        return count + ' month ago';
    }

    // 19 < 23 day
    if (date[2] < curr_day) {
        let count = curr_day - date[2];
        return count + ' day ago';
    }

    // 19 < 10 hour
    if (time[0] < curr_hour) {
        let count = curr_hour - time[0];
        return count + ' hour ago';
    }
    //  < 09 mintue
    if (time[1] < curr_min) {
        let count = curr_min - time[1];
        return count + ' min ago';
    }

    return 'now';
};

// let intlDateObj  = Intl.DateTimeFormat('en-US', {timeZone:'America/New_York'});
// let usaTime = intlDateObj.format(new Date);
// console.log(usaTime);