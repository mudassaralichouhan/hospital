<?php

session_start();
include 'vendor/autoload.php';

include('include/wraper start.php');
?>

<!-- Main Content -->
<main>

    <div class="content-wrapper container-fluid px-5 mt-5 mb-4 trans-03-in-out">

        <div class="row">

            <div class="col-xl-6 col-md-6">
                <a href="<?= $url ?>/ui/view/view.php?r=patient">
                    <section class="widget headerline-danger small shadow-01" id="widget-01">
                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">Patients</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="patient-total"><span id="patient-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-success">Register <i class="fa fa-level-up"></i></span>
                    </section>
                </a>
            </div>

            <div class="col-xl-3 col-md-6">
                <a href="<?= $url ?>/ui/view/view.php?r=consultant">
                    <section class="widget headerline-danger small shadow-01" id="widget-03">
                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">Consultant</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="consultant-total"><span id="consultant-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-warning">Avalible <i class="fa fa-heart" aria-hidden="true"></i></span>
                    </section>
                </a>
            </div>

            <div class="col-xl-3 col-md-6">
                <a href="<?= $url ?>/ui/view/view.php?m=post&r=user">
                    <section class="widget headerline-danger small shadow-01" id="widget-04">
                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">User</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="user-total"><span id="user-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-primary">On Work <i class="fa fa-bolt" aria-hidden="true"></i></span>
                    </section>
                </a>
            </div>

            <div class="col-xl-3 col-md-6">
                <a href="<?= $url ?>/ui/view/view.php?r=slip_dental">
                    <section class="widget headerline-danger small shadow-01" id="widget-02">
                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">Dental Slip</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="dental-total"><span id="dental-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-success" id="dental-outof-perc"><i class="fa fa-level-up"></i></span>
                    </section>
                </a>
            </div>

            <div class="col-xl-3 col-md-6">
                <a href="<?= $url ?>/ui/view/view.php?r=slip_surgical">
                    <section class="widget headerline-danger small shadow-01" id="widget-02">
                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">Surgical Slip</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="surgical-total"><span id="surgical-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-success" id="surgical-outof-perc"><i class="fa fa-level-up"></i></span>
                    </section>
                </a>
            </div>

            <div class="col-xl-3 col-md-6"><a href="<?= $url ?>/ui/view/view.php?r=slip_lab_test">
                    <section class="widget headerline-danger small shadow-01" id="widget-02">

                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">Laboratory Slip</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="labtest-total"><span id="labtest-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-success" id="labtest-outof-perc">
                            <i class="fa fa-level-up"></i>
                        </span>
                    </section>
                </a>
            </div>

            <div class="col-xl-3 col-md-6"><a href="<?= $url ?>/ui/view/view.php?r=slip_xray">
                    <section class="widget headerline-danger small shadow-01" id="widget-02">
                        <div class="widget-block">
                            <header>
                                <h3 class="widget-title">X-Ray Slip</h3>
                            </header>
                            <div class="widget-content pt-1">
                                <div>
                                    <p class="h2" id="xray-total"><span id="xray-symbol"></span></p>
                                    <p class="text-muted mt-2">Total</p>
                                </div>
                            </div>
                            <footer></footer>
                        </div>
                        <span class="badge badge-success" id="xray-outof-perc"><i class="fa fa-level-up"></i></span>
                    </section>
                </a>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-12">
                <section class="widget shadow-01" id="widget-05">
                    <div class="widget-block">
                        <header>
                            <h3 class="widget-title">Sales</h3>
                            <p class="text-muted">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                <br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                        </header>
                        <div class="row">
                            <div class="col-lg-12 widget-content py-3 my-3">
                                <canvas id="chart-bar"></canvas>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

    </div>
</main>
<!-- /Main Content -->
<?php
include($path . '/ui/include/wraper end.php');
?>
<script src="<?=$url?>/ui/public/js/dashboard.js"></script>
