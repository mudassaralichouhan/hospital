<?php

session_start();
include '../vendor/autoload.php';

if(!is_admin()) {
    header("location: ".$_SERVER['HTTP_HOST']);
}

$title = 'Consultant';
include('include/wraper start.php');

?>


    <div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <section class="widget shadow-01 mb-4" id="widget-08">
                    <div class="widget-block">
                        <header>
                            <h3>Consultant Method</h3>
                            <p class="text-muted">
                                More patient can also be created with this method.
                            </p>
                        </header>
                        <div class="widget-content py-3">

                            <!-- form -->
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="consultant-specialization" class="col-form-label">Specialization</label>
                                    <select id='consultant-specialization'
                                            onChange="chkSelect('consultant-specialization', 'specialization-err',  'Selected Specialization: ')"
                                            class="form-control form-control-lg mb-1">
                                        <option value="">Select Specialization</option>
                                    </select>
                                    <label id="specialization-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="consultant-name" class="col-form-label">Name</label>
                                    <input type="text" id='consultant-name' id="consultant-name"
                                           onChange="strLenth('consultant-name', 'name-err',30,3)"
                                           class="form-control form-control-lg mb-1" placeholder="Consultant Name">
                                    <label id="name-err" class="col-form-label"></label>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="consultant-contact" class="col-form-label">Contact (Mobile#)</label>
                                    <input type="number" onChange="pkTel(this,'contact-err');"
                                           id="consultant-contact" class="form-control form-control-lg mb-1"
                                           placeholder="e.g: (0300 11 22 333)">
                                    <label id="contact-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="consultant-fee" class="col-form-label">Fee
                                        <code>&lt;(optional)&gt;</code></label>
                                    <input onChange="isEmpty('consultant-fee','fee-err');"
                                           id='consultant-fee'
                                           class="form-control form-control-lg mb-1" value="0"
                                           placeholder="Consultant Fee e.g: 300.Rs">
                                    <label id="fee-err" class="col-form-label"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="gender" class="col-form-label">Gender</label><br/><br/>
                                    <div class="form-row">

                                        <div class="form-group col-md-2">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <input type="radio" name='gender'
                                                   onChange="chkRadio('female','gender-err')"
                                                   class="consultant-gender"
                                                   id="female"
                                                   value="Female"> Female
                                        </div>

                                        <div class="form-group col-md-4">
                                            <input type="radio" name='gender'
                                                   onChange="chkRadio('male','gender-err')"
                                                   class="consultant-gender "
                                                   id="male"
                                                   value="Male"> Male
                                        </div>
                                    </div>
                                    <label id="gender-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="consultant-age" class="col-form-label">Consultant Age</label>
                                    <select id='consultant-age'
                                            onChange="chkSelect('consultant-age', 'consultant-age-err', 'Select Age ');"
                                            class="form-control form-control-lg mb-1" require="true">
                                        <option value="">Select Age</option>
                                        <?php for ($i = 20; $i < 100; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <label id="consultant-age-err" class="col-form-label"></label>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="consultant-perc" class="col-form-label">Consultantance % <code>&lt;(optional)&gt;</code></label>
                                    <select id='consultant-perc'
                                            onChange="chkSelect('consultant-perc', 'consultant-perc-err', 'Selected Consultant Percentage ')"
                                            class="form-control form-control-lg mb-1" require="true">
                                        <option value="" Disabled>Select Consultantance</option>
                                        <?php
                                        for ($i = 0; $i <= 100; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="consultant-perc-err" class="col-form-label"></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="consultant-lab-perc" class="col-form-label">Lab %
                                        <code>&lt;(optional)&gt;</code></label>
                                    <select id='consultant-lab-perc'
                                            onChange="chkSelect('consultant-lab-perc', 'lab-perc-err', 'Selected Lab Percentage ')"
                                            class="form-control form-control-lg mb-1">
                                        <option value="" Disabled>Select Lab</option>
                                        <?php
                                        for ($i = 0; $i <= 100; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="lab-perc-err" class="col-form-label"></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="consultant-dental-perc" class="col-form-label">Dental %
                                        <code>&lt;(optional)&gt;</code></label>
                                    <select id='consultant-dental-perc'
                                            onChange="chkSelect('consultant-dental-perc', 'dental-perc-err', 'Selected Dental Percentage ')"
                                            class="form-control form-control-lg mb-1">
                                        <option value="" Disabled>Select Dental</option>
                                        <?php
                                        for ($i = 0; $i <= 100; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="dental-perc-err" class="col-form-label"></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="consultant-xray-perc" class="col-form-label">X Ray %
                                        <code>&lt;(optional)&gt;</code></label>
                                    <select id='consultant-xray-perc'
                                            onChange="chkSelect('consultant-xray-perc', 'xray-perc-err', 'Selected X-Ray Percentage ')"
                                            class="form-control form-control-lg mb-1">
                                        <option value="" Disabled>Select X Ray</option>
                                        <?php
                                        for ($i = 0; $i <= 100; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="xray-perc-err" class="col-form-label"></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="consultant-surgical-perc" class="col-form-label">Surgical %
                                        <code>&lt;(optional)&gt;</code></label>
                                    <select id='consultant-surgical-perc' id="surgical"
                                            onChange="chkSelect('consultant-surgical-perc', 'surgical-perc-err', 'Selected Surgical Percentage ');"
                                            class="form-control form-control-lg mb-1" require="true">
                                        <option value="" Disabled>Select Surgical</option>
                                        <?php
                                        for ($i = 0; $i <= 100; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="surgical-perc-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="consultant-address" class="col-form-label">Address
                                    <code>&lt;(optional)&gt;</code></label>
                                <textarea id='consultant-address' class="form-control form-control-lg mb-1"
                                          rows="3"></textarea>
                            </div>

                            <button onclick="consultant()" class="btn btn-primary"> Submit</button>
                            <!-- /form -->

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="<?= $url ?>/ui/public/js/consultant.js"></script>
    <script>setSpacializtion()</script>
<?php
include($path . '/ui/include/wraper end.php');
?>