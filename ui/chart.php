<?php

session_start();
include '../vendor/autoload.php';


$title = 'Chart';
include('include/wraper start.php');

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">

        <div class="col-lg-12">
            <section class="widget shadow-01 md-5">
                <div class="widget-block">
                    <header>
                        <h3 class="widget-title">Line Chart</h3>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            <br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </header>
                    <div class="widget-content py-3">
                        <canvas id="chart-line" height="300"></canvas>
                    </div>
                </div>
            </section>
        </div>

        <div class="col-xl-6">
            <section class="widget shadow-01 mb-5">
                <div class="widget-block">
                    <header>
                        <h3>Bar chart</h3>
                        <p class="text-muted">
                            A bar chart provides a way of showing data values represented as vertical bars.
                        </p>
                    </header>
                    <div class="widget-content py-3">
                        <canvas id="chart-bar" height="400"></canvas>
                    </div>
                </div>
            </section>
        </div>

        <div class="col-xl-6">
            <section class="widget shadow-01 mb-5">
                <div class="widget-block">
                    <header>
                        <h3>Horizontal stacked chart</h3>
                        <p class="text-muted">
                            A horizontal bar chart is a variation on a vertical bar chart.
                            <span>Lab Test, X Ray, Surgical, Dental</span>
                        </p>
                    </header>
                    <div class="widget-content py-3">
                        <canvas id="chart-bar-horizontal" height="400"></canvas>
                    </div>
                </div>
            </section>
        </div>

    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>
<script src="<?=$url?>/ui/public/js/chart.js"></script>






