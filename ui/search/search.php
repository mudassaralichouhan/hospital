<?php

session_start();
include '../../vendor/autoload.php';

$title = 'Search';
include('../include/wraper start.php');

$keys = isset($_GET['keys']) ? $_GET['keys'] : exit;
$page = isset($_GET['page']) ? $_GET['page'] : 1;

?>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">

                    <header>
                        <h3>Search patient</h3>
                        <p class="text-muted">
                            More dental patient can also be created with this method.
                        </p>
                    </header>

                        <main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main" id="search-content">
                            <h5 id="">There are  - <code class="highlighter-rouge" id="offset"></code> records out of <code id="total" class="highlighter-rouge"></code></h5>
                            <br/><br/>
                        </main>

                    <!-- pagination -->
                    <div class="container">
                        <nav aria-label="...">
                            <ul class="pagination">
                            </ul>
                        </nav>
                    </div>
                    <!-- /pagination -->

                </div>
            </section>
        </div>
    </div>
</div>

<?php
include($path . '/ui/include/wraper end.php');
?>
<script>
    let user = JSONreq('search','search', {
        key : '<?=$keys?>',
        page: <?=$page?>
    });

    data = user['data'];
    pagi = user['pagination'];

    document.getElementById('offset').innerHTML = user.offset;
    document.getElementById('total').innerHTML = user.total;

    (function () {
        for (let i=0; i<data.length; i++) {
            $('#search-content').append('<h3>'+
                '<a href="<?=$url?>/ui/view/view_patient.php?id='+data[i]['id']+'">'+data[i]['name']+ '</a>'+
                    ' - <code class="highlighter-rouge">'+data[i]['contact']+'</code>'+
                '</h3>'+
                '<h6 class="text-info"> '+data[i]['gender']+'</h6>'+
                '<p class="text-secondary"> | Address'+data[i]['location']+'</p>');
        }
    })();

    // pagination -->
    $html = '<li class="page-item ' + (pagi.previous_page === '#' ? $prev = 'disabled' : '') + '">' +
        '<a class="page-link" href="?keys=<?= $keys ?>&page=' + pagi.previous_page + '" tabindex="-1" aria-disabled="true">' +
        'Previous' +
        '</a>' +
        '</li>';

    $.each(pagi.buttons, function (index, value) {
        if (index === 'active') {
            $html += '<li class="page-item active">' +
                '<a class="page-link" href="?keys=<?= $keys ?>&page=' + value + '">' + value + '</a>' +
                '</li>';
        } else {
            $html += '<li class="page-item">' +
                '<a class="page-link" href="?keys=<?= $keys ?>&page=' + value + '">' + value + '</a>' +
                '</li>';
        }
    });

    $html += '<li class="page-item ' + (pagi.next_page === '#' ? $next = 'disabled' : '') + '">' +
        '<a class="page-link" href="?keys=<?= $keys ?>&page=' + pagi.next_page + '" tabindex="-1" aria-disabled="true">' +
        'Next' +
        '</a>' +
        '</li>';

    $(".pagination").append($html);
    // end pagination -->
</script>
