<?php

session_start();
include '../../vendor/autoload.php';

$title = 'Slip Patient';
include('../include/wraper start.php');

?>

    <div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <section class="widget shadow-01 mb-4" id="widget-08">
                    <div class="widget-block">
                        <header>
                            <h3>Patient Method</h3>
                            <p class="text-muted">
                                More patient can also be created with this method.
                            </p>
                        </header>
                        <div class="widget-content py-3">

                            <!-- form -->
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="patient-consultant-id" class="col-form-label">Consultant</label>
                                    <select id='patient-consultant-id'
                                            onChange="patientPaidAmt(); chkSelect('patient-consultant-id','consultant-err', 'Selected consultant: ');"
                                            class="form-control form-control-lg mb-1">
                                        <option value="">Select Consultant</option>
                                    </select>
                                    <label id="consultant-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="patient-name" class="col-form-label">Name</label>
                                    <input type="text" id="patient-name" autofocus
                                           onChange="strLenth('patient-name','name-err', 30, 3)"
                                           class="form-control form-control-lg mb-1" placeholder="Patient Name">
                                    <label id="name-err" class="col-form-label"></label>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="patient-contact" class="col-form-label">Contact (Mobile#)</label>
                                    <input type="number" onChange="pkTel('patient-contact','contact-err');"
                                           id="patient-contact" class="form-control form-control-lg mb-1"
                                           placeholder="e.g: (0300 11 22 333)" maxlength="11" value="0300">
                                    <label id="contact-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="patient-age" class="col-form-label">Age</label>
                                    <select id='patient-age'
                                            onChange="chkSelect('patient-age','age-err','Age is: ');"
                                            class="form-control form-control-lg mb-1">
                                        <option value="">Select Age</option>
                                        <?php
                                        for ($age = 1; $age < 150; $age++) {
                                            echo "<option value='$age'>$age</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="age-err" class="col-form-label"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputGender" class="col-form-label">Gender</label><br/>
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <input type="radio"
                                                   onChange="chkRadio('female','gender-err')"
                                                   name="gender" class="patient-gender" id="female"
                                                   value="Female"> Female
                                        </div>

                                        <div class="form-group col-md-4">
                                            <input type="radio"
                                                   onChange="chkRadio('male','gender-err')"
                                                   name="gender" class="patient-gender" id="male"
                                                   value="Male">
                                            Male
                                        </div>
                                    </div>
                                    <label id="gender-err" class="col-form-label"></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="patient-discount" class="col-form-label">Discount
                                        <code>&lt;(optional)&gt;</code></label>
                                    <select id="patient-discount"
                                            onChange="patientPaidAmt(); chkSelect('patient-discount','disc-err','Discount: ');"
                                            class="form-control form-control-lg mb-1">
                                        <option value="" Disabled>Select Discount</option>
                                        <?php
                                        for ($discount = 0; $discount <= 100; $discount++) {
                                            echo "<option value='$discount'>$discount</option>";
                                        }
                                        ?>
                                    </select>
                                    <label id="disc-err" class="col-form-label"></label>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="patient-paid" class="col-form-label">Paid</label>
                                    <p class="h1" id="patient-paid">0 Rs</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="patient-address" class="col-form-label">Address
                                    <code>&lt;(optional)&gt;</code></label>
                                <textarea id="patient-address" class="form-control form-control-lg mb-1"
                                          rows="3"></textarea>
                            </div>

                            <button onclick="patientInsert(this)" class="btn btn-primary"> Print </button>
                            <!-- /form -->
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="<?= $url ?>/ui/public/js/patient.js"></script>
<?php
include($path . '/ui/include/wraper end.php');
?>
<script>setNameFeeOfConsultant();</script>
