<?php

session_start();
include '../../vendor/autoload.php';

$title = 'Slip';
include('../include/wraper start.php');

$switch_to = isset($_GET['s']) ? $_GET['s'] : exit;
$action_is = isset($_GET['p']) ? $_GET['p'] : exit;

?>

    <div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <section class="widget shadow-01 mb-4">

                    <div class="widget-block">

                        <header>
                            <h3><?= str_replace('_', ' ', $switch_to) ?> Patient Method</h3>
                            <p class="text-muted">
                                More dental patient can also be created with this method.
                            </p>
                        </header>

                        <div class="widget-content py-3" id="get-patient-form">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="input-patient-id" class="col-form-label">Patient Voucher no.</label>
                                    <input type="number" id="input-patient-id" class="form-control form-control-lg mb-1"
                                           placeholder="Voucher"
                                           onChange="isNumeric('input-patient-id','patient-id-err')">
                                    <p id='patient-id-err'></p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button onClick="getPatientSlipInfo('input-patient-id','patient-id-err')"
                                            class="btn btn-primary"> Find
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="widget-content py-3" id="get-patient-result">
                            <div class="col-lg-12">

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div class="h3" id="consultant-name"><span class="badge badge-secondary">Consultant</span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="h3" id="patient-name"><span
                                                    class="badge badge-secondary">Patient Name</span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="h3" id="patient-id"><span
                                                    class="badge badge-secondary">Patient ID</span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="h3" id="patient-contact"><span
                                                    class="badge badge-secondary">Contact</span></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="h3" id="patient-gender"><span
                                                    class="badge badge-secondary">Gender</span></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="h3" id="patient-age"><span class="badge badge-secondary">Age</span>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-12">
                                <div class="row">

                                    <div class="form-group col-md-12">
                                        <label for="procedure-id" class="col-form-label">Select Procedure applied</label>
                                        <select onChange="chkSelect('procedure-id','procedure-err',  'Selected Laboratory Procedure: '); setSlipPaidAmt()"
                                                class="form-control form-control-lg mb-1" id="procedure-id">
                                                <option value=""> --Procedure-- </option>
                                        </select>
                                        <label id="procedure-err" class="col-form-label"></label>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="get-amount" class="col-form-label">Get Amount</label>
                                        <input type="text" id='get-amount'
                                               onChange="isEmpty('get-amount','get-amount-err');"
                                               class="form-control form-control-lg mb-1"
                                               placeholder="Get Amount">
                                        <label id="get-amount-err" class="col-form-label"></label>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="discount" class="col-form-label">Discount</label>
                                        <select id="discount"
                                                onChange="setSlipPaidAmt(); chkSelect('discount','discount-err','Discount: ');"
                                                class="form-control form-control-lg mb-1" height="100">
                                            <option value="" Disabled>Select Discount</option>
                                            <?php
                                                for ($discount = 0; $discount < 100; $discount += 5) {
                                                    echo "<option value='$discount'>$discount</option>";
                                                }
                                            ?>
                                        </select>
                                        <label id="discount-err" class="col-form-label"></label>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <p class="h5">Paid amount</p>
                                        <p id="price" class="h3">0 Rs</p>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row col-md-4">
                                            <button class="btn btn-primary"
                                                    onclick="insertSlip('<?= $switch_to ?>');"
                                                    id="insert-slip-btn">
                                                Submit
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
    <script>

        $(function () {
            $procedureFee = [];
            setProductToSelectElement($procedureFee, '<?=$action_is?>');
        });

    </script>

    <script src="<?php echo $url; ?>/ui/public/js/slip_function.js"></script>
<?php
include($path . '/ui/include/wraper end.php');
?>