<?php

session_start();
include '../../vendor/autoload.php';

$title = 'Search';
include('../include/wraper start.php');

$page = isset($_GET['page']) ? $_GET['page'] : 1;

?>

<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <section class="widget shadow-01 mb-4">
                <div class="widget-block">

                    <header>
                        <h3>Due Slip's</h3>
                        <p class="text-muted">
                            More search dues patient amount can also be remamber you with this method.
                        </p>
                    </header>

                    <main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main" id="search-content">
                        <h5 id="">There are  - <code class="highlighter-rouge" id="current-slips"></code> records out of <code id="total-slip" class="highlighter-rouge"></code></h5>
                        <hr/>
                    </main>

                    <!-- pagination -->
                    <div class="container">
                        <nav aria-label="...">
                            <ul class="pagination">
                            </ul>
                        </nav>
                    </div>
                    <!-- /pagination -->

                </div>
            </section>
        </div>
    </div>
</div>

<?php
include($path . '/ui/include/wraper end.php');
?>
<script>
    let user = JSONreq('slip','due_with_limit', {
        page: <?=$page?>
    });

    let data = user['data'];
    let pagi = user['pagination'];
    document.getElementById('total-slip').innerHTML = user['total_slips'];
    document.getElementById('current-slips').innerHTML = user['current_slips'];

    (function () {
        for (let i=0; i<data.length; i++) {
            $('#search-content').append('<h3>'+
                '<a href="<?=$url?>/ui/view/view_patient.php?id='+data[i]['id']+'">'+data[i]['name']+ '</a></h3>'+
                '<p">Consultant: '+data[i]['consultant_id']+'</p>'+
                '<p">Amount: '+data[i]['remain_amt']+'.rs </p>'+
                '<p class="text-muted float-right">'+readableDate(data[i]['reg_on'])+' | </p>' +
                '<br/><hr/>'
            );
        }
    })();

    // pagination -->
    $html = '<li class="page-item ' + (pagi.previous_page === '#' ? $prev = 'disabled' : '') + '">' +
        '<a class="page-link" href="?page=' + pagi.previous_page + '" tabindex="-1" aria-disabled="true">' +
        'Previous' +
        '</a>' +
        '</li>';

    $.each(pagi.buttons, function (index, value) {
        if (index === 'active') {
            $html += '<li class="page-item active">' +
                '<a class="page-link" href="?page=' + value + '">' + value + '</a>' +
                '</li>';
        } else {
            $html += '<li class="page-item">' +
                '<a class="page-link" href="?page=' + value + '">' + value + '</a>' +
                '</li>';
        }
    });

    $html += '<li class="page-item ' + (pagi.next_page === '#' ? $next = 'disabled' : '') + '">' +
        '<a class="page-link" href="?page=' + pagi.next_page + '" tabindex="-1" aria-disabled="true">' +
        'Next' +
        '</a>' +
        '</li>';

    $(".pagination").append($html);
    // end pagination -->
</script>
