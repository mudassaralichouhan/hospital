<!-- Right Sidebar -->
<div id="sidebar-right" class="sidebar-right">

    <div class="sidebar-container">

        <ul class="nav nav-tabs nav-tabs-dark" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#notes" role="tab" aria-controls="notes"
                   aria-selected="true">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i> Due's
                </a>
            </li>
            <li class="nav-item" onclick="todayPatients()">
                <a class="nav-link" data-toggle="tab" href="#tasks" role="tab" aria-controls="tasks"
                   aria-selected="false">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i> Today
                </a>
            </li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane fade show active" id="notes" role="tabpanel" aria-labelledby="notes-tab">
                <div class="p-4">
                    <h5 class="tab-title"><span id="noti-slip"></span> Notefication Received</h5>
                    <p class="mb-0">
                        These are related notefication outof the some <b>Slip</b> are listed here
                    </p>
                </div>
                <ul class="list-unstyled list-striped" id="slip-noti">
                </ul>
            </div>

            <div class="tab-pane fade" id="tasks" role="tabpanel" aria-labelledby="tasks-tab">
                <div class="p-4">
                    <h5 class="tab-title">All most 12 Today Patient's</h5>
                    <p class="mb-0">
                        Today last patient are listed hare
                    </p>
                </div>
                <ul class="list-unstyled list-striped" id="today-patient-noti">
                </ul>
            </div>

        </div>

    </div>

</div>
<!-- /Right Sidebar -->