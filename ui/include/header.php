<!-- Top Toolbar -->
<div class="navbar navbar-light bg-white px-3 px-sm-5 py-3">
    <div class="d-inline-block mr-3">
        <a href="#" data-target="#sidebar-left" data-toggle="collapse-width"
           class="btn btn-dark btn-icon rounded-circle shadow-00">
            <i class="fa fa-navicon"></i>
        </a>
    </div>

    <form class="search-form form-inline my-2 my-lg-0">;
        <div class="input-group input-group-built-in">
            <input type="text" id="instance-query" class="form-control rounded-1" placeholder="Search for something..."
                   aria-label="Search for something...">
            <span class="input-group-btn">
                <a href="#" class="btn btn-icon">
                    <i class="fa fa-search"></i>
                </a>
            </span>
        </div>
    </form>

    <ul class="nav ml-auto">

        <li class="m-sm-1 m-md-2 position-relative">
            <a class="btn btn-icon" href="#">
                <img src="<?= $url ?>/ui/public/images/flags/pk.png" height="27" alt="flag">
            </a>
        </li>

        <li class="m-sm-1 m-md-2 position-relative">
            <a class="btn btn-light btn-icon rounded-circle shadow-00" href="#" onclick="localStorage.clear();location.reload();">
                <i class="fa fa-refresh" title="clear cashe"></i>
            </a>
        </li>

        <li class="m-sm-1 m-md-2">
            <a data-toggle="slide-right" data-target="#sidebar-right" onclick="dueSlipList()"
               class="btn btn-light btn-icon rounded-circle shadow-00" href="#">
                <i class="fa fa-bell" aria-hidden="true"></i>
            </a>
        </li>

        <li class="m-sm-1 m-md-2 position-relative">
            <a data-toggle="dropdown" href="#" aria-expanded="false">
                <div class="d-inline-block mr-2">
                    <img src="<?= $url ?>/ui/public/images/avatar_2x.png" class="rounded-circle"
                         height="32px" alt="img" id="profile-photo-icon">
                </div>
                <div class="d-none d-lg-inline-block">
                    <span class="d-block"><?= $_SESSION[SESSION]['name'] ?></span>
                </div>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="<?= $url ?>/ui/profile/profile.php">
                    <i class="fa fa-user-circle-o" aria-hidden="true"></i> Profile
                </a>
                <i class="dropdown-item fa fa-key" href="#"
                   aria-hidden="true"
                   data-toggle="modal" data-target="#change-password"
                   data-placement="right" title="Change Password"> Change Password
                </i>
                <a class="dropdown-item" href="<?= $url ?>/ui/profile/setting.php">
                    <i class="fa fa-cog" aria-hidden="true"></i> Setting
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?= $url ?>/login.php" onclick="localStorage.clear();JSONreq('logout','',{})">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> Logout
                </a>
            </div>

        </li>

    </ul>

</div>
<div class="px-3 px-sm-5">
    <div class="position-absolute col-md-7" style="z-index: 5">
        <div class="list-group" id="suggestions">
        </div>
    </div>
</div>
<!-- /Top Toolbar -->
<script>

    function changePassword(curr, pass, pass2) {

        if (pass.value === pass2.value && pass.value.length >= 6) {
            let user = JSONreq('user', 'change_password', {
                'id': 255,
                'password': pass.value,
                'password1': pass2.value,
                'curr_password': curr.value
            }, 'post');

            if (user.status) {
                notification('<div class="alert alert-success">' + user['msg'] + '</div>');

                pass2.value = '';
                pass.value = '';
                document.getElementById('security-curr-password').value = '';
            } else {
                notification('<div class="alert alert-danger">' + user['msg'] + '</div>');
            }
        } else {
            notification('<div class="alert alert-dark">You new password not match with each other</div>');
        }

    }
</script>
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="change-password">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="model-curr-password"><h4>Current</h4></label>
                        <input type="password" class="form-control"
                               name="model-curr-password" id="model-curr-password"
                               placeholder="Current password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="model-password"><h4>New</h4></label>
                        <input type="password" class="form-control" name="model-password"
                               id="model-password" placeholder="New password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="model-password2"><h4>New re-type</h4></label>
                        <input type="password" class="form-control" name="model-password2"
                               id="model-password2" placeholder="Re-Type password">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="changePassword(document.getElementById('model-curr-password'), document.getElementById('model-password'), document.getElementById('model-password2'));">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal -->