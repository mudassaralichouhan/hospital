<?php

include($path . '/ui/include/footer.php');

echo '</div>';
// <!-- /Main Part -->

include($path . '/ui/include/right naviagtion.php');

echo '</div>';
echo '</div>';

// <!-- JS Common -->
echo '<script src="' . $url . '/ui/public/vendor/popper/1.12.9/popper.min.js"></script>';

// <!-- JS Page -->
echo '<script src="' . $url . '/ui/public/js/chart.min.js"></script>';

// <!-- JS Custom -->
echo '<script src="' . $url . '/ui/public/js/bootstrap.min.js"></script>';
echo '<script src="' . $url . '/ui/public/js/script.js"></script>';
echo '<script src="' . $url . '/ui/public/js/source-code.js"></script>';

// <!-- these are project base -->
echo '<script> $baseURL = "' . $url . '/"; </script>';
echo '<script src="' . $url . '/ui/public/js/custom.js"></script>';
echo '<script src="' . $url . '/ui/public/js/comm_function.js"></script>';

echo '</body>';
echo '</html>';
