<!-- Logo -->
<div class="logo px-4 pt-5 pb-2">
    <a href="<?= $url ?>">
        <div class="text-center text-nowrap">
            <i class="fa fa-spin fa-play-circle rounded-circle" aria-hidden="true"></i>
            <h6 class="logo-title text-uppercase mt-3">Hospital</h6>
        </div>
    </a>
</div>
<!-- /Logo -->

<!-- Logo mobile -->
<div class="logo-mobile pt-4 pb-4 w-100">
    <a href="<?= $url ?>">
        <div class="text-center text-nowrap">
            <i class="fa fa-spin fa-play-circle rounded-circle" aria-hidden="true"></i>
        </div>
    </a>
</div>
<!-- /Logo mobile -->