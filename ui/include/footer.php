<?php

// <!-- Footer -->
echo '<footer class="bg-white w-100 pl-5 pr-5 pt-4 pb-4 mt-auto">';
echo '<div>copyright © ' . Date('Y') . ' - Health care hospital </div>'.
    '<a class="float-right p-1" href="https://www.github.com/mudassaralichouhan/php"><i class="fa fa-github-alt"></i></a>';
echo '</footer>';
// <!-- /Footer -->

?>

