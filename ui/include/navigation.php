<!-- Left Sidebar -->
<div id="sidebar-left" class="sidebar-left bg-dark text-light pl-0 pr-0">
    <div class="collapse-wrapper">

        <?php include 'logo.php' ?>

        <nav class="sidebar-nav">
            <!-- Sidebar Menu -->
            <div class="mb-1 text-uppercase d-none d-lg-block text-muted">
                <small>General</small>
            </div>
            <ul id="sidebarNav" class="nav nav-dark flex-column">

                <li class="nav-item">
                    <a class="nav-link" href="<?= $url ?>">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <span class="d-none d-lg-inline">Dashboard</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?= $url ?>/ui/chart.php">
                        <i class="fa fa-bar-chart" aria-hidden="true"></i>
                        <span class="d-none d-lg-inline">Chart</span>
                    </a>
                </li>

                <?php
                $consultant = '#';
                if($_SESSION[SESSION]['rule'] === 'Admin') {
                    $consultant = $url.'/ui/consultant.php';
                }
                ?>

                <li class="nav-item">
                    <a href="#cNavItem1" class="nav-link collapsed" data-toggle="collapse" aria-expanded="false"
                       aria-controls="cNavItem2">
                        <i class="fa fa-moon-o" aria-hidden="true"></i>
                        <span class="d-none d-lg-inline">Consultant</span>
                    </a>
                    <ul id="cNavItem1" class="nav flex-column collapse bg-dark" data-parent="#sidebarNav">
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $consultant ?>">Consultant</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#cNavItem2" class="nav-link collapsed" data-toggle="collapse" aria-expanded="false"
                       aria-controls="cNavItem2">
                        <i class="fa fa-child" aria-hidden="true"></i>
                        <span class="d-none d-lg-inline">Slips</span>
                    </a>
                    <ul id="cNavItem2" class="nav flex-column collapse bg-dark" data-parent="#sidebarNav">
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $url ?>/ui/slip/patient.php">Patient</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1"
                               href="<?= $url ?>/ui/slip/?s=slip_surgical&p=procedure_surgical">Surgical</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1"
                               href="<?= $url ?>/ui/slip/?s=slip_dental&p=procedure_dental">Dental</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $url ?>/ui/slip/?s=slip_xray&p=procedure_xray">Xray</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1"
                               href="<?= $url ?>/ui/slip/?s=slip_lab_test&p=procedure_lab_test">Lab Test</a>
                        </li>
                    </ul>
                </li>

                <?php
                $procedure_spacialization = '#';
                $procedure_dental = '#';
                $procedure_surgical = '#';
                $procedure_xray = '#';
                $procedure_lab_test = '#';

                if($_SESSION[SESSION]['rule'] === 'Admin') {
                    $procedure_spacialization = $url.'/ui/product/spacialization.php';
                    $procedure_dental = $url.'/ui/product/?p=procedure_dental';
                    $procedure_surgical = $url.'/ui/product/?p=procedure_surgical';
                    $procedure_xray = $url.'/ui/product/?p=procedure_xray';
                    $procedure_lab_test = $url.'/ui/product/?p=procedure_lab_test';
                }
                ?>

                <li class="nav-item">
                    <a href="#cNavItem3" class="nav-link collapsed" data-toggle="collapse" aria-expanded="false"
                       aria-controls="cNavItem2">
                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                        <span class="d-none d-lg-inline">Products</span>
                    </a>
                    <ul id="cNavItem3" class="nav flex-column collapse bg-dark" data-parent="#sidebarNav">
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $procedure_spacialization ?>">
                                Spacialization
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $procedure_surgical ?>">
                                Surgical
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $procedure_dental ?>">
                                Dental
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $procedure_xray ?>">
                                Xray
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $procedure_lab_test ?>">
                                Laboratory
                            </a>
                        </li>
                    </ul>
                </li>

                <?php
                $report_busness = '#';
                $report_consultant = '#';

                if($_SESSION[SESSION]['rule'] === 'Admin') {
                    $report_busness = $url.'/ui/report/busness.php';
                    $report_consultant = $url.'/ui/report/consultant.php';
                }
                ?>

                <li class="nav-item">
                    <a href="#cNavItem4" class="nav-link collapsed" data-toggle="collapse" aria-expanded="false"
                       aria-controls="cNavItem2">
                        <i class="fa fa-group" aria-hidden="true"></i>
                        <span class="d-none d-lg-inline">Report</span>
                    </a>
                    <ul id="cNavItem4" class="nav flex-column collapse bg-dark" data-parent="#sidebarNav">
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?=$report_busness?>">Busness</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?=$report_consultant?>">With
                                Consultant</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-level-1" href="<?= $url ?>/ui/report/user.php">Me...!</a>
                        </li>
                    </ul>
                </li>


            </ul>
            <!-- /Sidebar Menu -->
        </nav>
    </div>
</div>
<!-- /Left Sidebar -->