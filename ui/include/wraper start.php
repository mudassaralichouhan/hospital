<?php

require __DIR__ . '/../../init.php';

echo '<!DOCTYPE html>';
echo '<html lang="en">';

echo '<head><title>'.$title.' - Hospital</title>';

// <!-- Required Meta Tags -->
echo '<meta charset="utf-8">';
echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
echo '<meta name="description" content="hospital managment system on particular demand">';
echo '<meta name="keywords" content="hospital, care, health">';
echo '<meta name="author" content="mudassaralichouhan">';

// <!-- Favicon -->
echo '<link rel="icon" href="" sizes="32x32"/>';
echo '<link rel="icon" href="" sizes="192x192"/>';

// <!-- Main CSS with Bootstrap -->
echo '<link rel="stylesheet" href="' . $url . '/ui/public/css/style.min.css">';

// <!-- CSS Vendor -->
echo '<link rel="stylesheet" href="' . $url . '/ui/public/vendor/font-awesome/4.7.0/css/font-awesome.min.css">';
echo '<script src="' . $url . '/ui/public/js/jquery-3.2.1.min.js"></script>';

echo '</head>';

echo '<body class="bg-gray-100">';

    echo '<div class="position-relative fixed-top text-center" id="notifications"></div>';

    echo '<div class="container-fluid no-gutters">';
        echo '<div class="row">';
        include($path . '/ui/include/navigation.php');

// <!-- Main Part -->
echo '<div class="main-wrapper">';
include($path . '/ui/include/header.php');
?>