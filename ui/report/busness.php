<?php

session_start();
include '../../vendor/autoload.php';

if(!is_admin()) {
    header("location: ".$_SERVER['HTTP_HOST']);
}

$title = 'Busness Report';
include('../include/wraper start.php');

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">

        <div class="col-lg-12 md-3">
            <section class="widget shadow-01 md-4" id="widget-01">
                <div class="widget-block">
                    <header>
                        <h3 class="widget-title">Busness Report</h3>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            <br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </header>

                    <div id="row">
                        <div id="status" class="container">
                        </div>
                        <div class="col-6">
                            <label>Date From:</label>
                            <input type="date"
                                   class="form-control form-control-lg mb-3"
                                   onchange="businessReport()"
                                   id="from"
                            >
                        </div>
                        <div class="col-6">
                            <label>Date to:</label>
                            <input type="date"
                                   class="form-control form-control-lg mb-3"
                                   onchange="businessReport()"
                                   id="to"
                            >
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-5">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Patient's</h3>
                                        <p class="text-muted">
                                            Total Paitent are <span id="total-patient"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-patient"></span>
                                                <span>rs</span>
                                            </p>
                                            <p class="text-muted mt-2">Profite of patient</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-patient"></span>
                                                <span>rs</span>
                                            </p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-patient"></span>
                                                <span>%</span>
                                            </p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-patient"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    patient
                                    <span id="floated-total-patient"> </span>
                                    <span>K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-7">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Dental's</h3>
                                        <p class="text-muted">
                                            Total Dental are<span id="total-dental"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-dental"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of dental</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-dental"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-dental"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-dental"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    dental
                                    <span id="floated-total-dental"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-7">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Surgical's</h3>
                                        <p class="text-muted">
                                            Total Surgical are<span id="total-surgical"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-surgical"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of surgical</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-surgical"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-surgical"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-surgical"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    surgical
                                    <span id="floated-total-surgical"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-5">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">X-Ray's</h3>
                                        <p class="text-muted">
                                            Total X-Ray are<span id="total-xray"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-xray"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of xray</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-xray"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-xray"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-xray"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    xray
                                    <span id="floated-total-xray"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Laboratory's</h3>
                                        <p class="text-muted">
                                            Total Laboratory are<span id="total-lab"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-lab"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of lab</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-lab"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-lab"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-lab"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    lab
                                    <span id="floated-total-lab"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>


                    </div>

                </div>
            </section>
        </div>

    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>
<script>

    /*
     *
     */
    function businessReport() {

        let from = document.getElementById('from');
        let to = document.getElementById('to');

        let response = JSONreq('report', 'get_by_year', {
            'from': from.value,
            'to': to.value
        });

        $('.alert').remove();

        if (response.status === false) {
            let html = '<div class="alert alert-danger" role="alert">' + response.msg + '</div>';
            $('#status').html(html);
            return false;
        }


        document.getElementById('total-patient').innerHTML = response.patient.total;
        document.getElementById('profite-patient').innerHTML = response.patient.profite;
        document.getElementById('profite-outof-patient').innerHTML = response.patient.outof;
        document.getElementById('discount-patient').innerHTML = response.patient.discount_avg;
        document.getElementById('lost-amt-patient').innerHTML = response.patient.lose;
        document.getElementById('floated-total-patient').innerHTML = response.patient.total / 1000;

        document.getElementById('total-dental').innerHTML = response.dental.total;
        document.getElementById('profite-dental').innerHTML = response.dental.profite;
        document.getElementById('profite-outof-dental').innerHTML = response.dental.outof;
        document.getElementById('discount-dental').innerHTML = response.dental.discount_avg;
        document.getElementById('lost-amt-dental').innerHTML = response.dental.lose;
        document.getElementById('floated-total-dental').innerHTML = response.dental.total / 1000;

        document.getElementById('total-surgical').innerHTML = response.surgical.total;
        document.getElementById('profite-surgical').innerHTML = response.surgical.profite;
        document.getElementById('profite-outof-surgical').innerHTML = response.surgical.outof;
        document.getElementById('discount-surgical').innerHTML = response.surgical.discount_avg;
        document.getElementById('lost-amt-surgical').innerHTML = response.surgical.lose;
        document.getElementById('floated-total-surgical').innerHTML = response.surgical.total / 1000;

        document.getElementById('total-xray').innerHTML = response.xray.total;
        document.getElementById('profite-xray').innerHTML = response.xray.profite;
        document.getElementById('profite-outof-xray').innerHTML = response.xray.outof;
        document.getElementById('discount-xray').innerHTML = response.xray.discount_avg;
        document.getElementById('lost-amt-xray').innerHTML = response.xray.lose;
        document.getElementById('floated-total-xray').innerHTML = response.xray.total / 1000;

        document.getElementById('total-lab').innerHTML = response.lab.total;
        document.getElementById('profite-lab').innerHTML = response.lab.profite;
        document.getElementById('profite-outof-lab').innerHTML = response.lab.outof;
        document.getElementById('discount-lab').innerHTML = response.lab.discount_avg;
        document.getElementById('lost-amt-lab').innerHTML = response.lab.lose;
        document.getElementById('floated-total-lab').innerHTML = response.lab.total / 1000;

    }
</script>






