<?php

session_start();
include '../../vendor/autoload.php';


$title = 'Busness Report';
include('../include/wraper start.php');

$name = $_SESSION[SESSION]['name'];

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">

        <div class="col-lg-12 md-3">
            <section class="widget shadow-01 md-4" id="widget-01">
                <div class="widget-block">
                    <header>
                        <h3 class="widget-title"><?= $name ?> Report</h3>
                        <p class="text-muted">
                            <?= $name ?> report totaly upto your activity, we help your reporting to easy,
                            <br>be patient good luck.
                        </p>
                    </header>
                    <div class="widget-content py-3">

                        <div id="row">
                            <div id="status" class="container">
                            </div>
                            <div class="col-6">
                                <label for="from">Date From:</label>
                                <input type="date"
                                       class="form-control form-control-lg mb-3"
                                       onchange="userReport()"
                                       id="from"
                                >
                            </div>
                            <div class="col-6">
                                <label for="to">Date to:</label>
                                <input type="date"
                                       class="form-control form-control-lg mb-3"
                                       onchange="userReport()"
                                       id="to"
                                >
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-5">
                                <section class="widget widget-dark bg-dark small shadow-01">

                                    <div class="widget-block">
                                        <header>
                                            <h3 class="widget-title">Patient's</h3>
                                            <p class="text-muted">
                                                Total Paitent are <span id="total-patient"></span>
                                            </p>
                                        </header>

                                        <p class="text-muted mt-2">
                                            See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                        </p>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-patient"></span>
                                                    <span>rs</span>
                                                </p>
                                                <p class="text-muted mt-2">Profite of patient</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-outof-patient"></span>
                                                    <span>rs</span>
                                                </p>
                                                <p class="text-muted mt-2">Profite out-of amount</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="discount-patient"></span>
                                                    <span>%</span>
                                                </p>
                                                <p class="text-muted mt-2">Discount avarage</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1" id="lost-amt-patient"></p>
                                                <p class="text-muted mt-2">lose amount in discount</p>
                                            </div>
                                        </div>

                                        <footer></footer>
                                    </div>
                                    <span class="badge badge-secondary">
                                    patient
                                    <span id="floated-total-patient"> </span>
                                    <span>K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                                </section>
                            </div>

                            <div class="col-lg-7">
                                <section class="widget widget-dark bg-dark small shadow-01">

                                    <div class="widget-block">
                                        <header>
                                            <h3 class="widget-title">Dental's</h3>
                                            <p class="text-muted">
                                                Total Dental are<span id="total-dental"></span>
                                            </p>
                                        </header>

                                        <p class="text-muted mt-2">
                                            See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                        </p>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-dental"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite of dental</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-outof-dental"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite out-of amount</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="discount-dental"></span>
                                                    <span>%</span></p>
                                                <p class="text-muted mt-2">Discount avarage</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1" id="lost-amt-dental"></p>
                                                <p class="text-muted mt-2">lose amount in discount</p>
                                            </div>
                                        </div>

                                        <footer></footer>
                                    </div>
                                    <span class="badge badge-secondary">
                                    dental
                                    <span id="floated-total-dental"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                                </section>
                            </div>

                            <div class="col-lg-7">
                                <section class="widget widget-dark bg-dark small shadow-01">

                                    <div class="widget-block">
                                        <header>
                                            <h3 class="widget-title">Surgical's</h3>
                                            <p class="text-muted">
                                                Total Surgical are<span id="total-surgical"></span>
                                            </p>
                                        </header>

                                        <p class="text-muted mt-2">
                                            See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                        </p>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-surgical"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite of surgical</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-outof-surgical"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite out-of amount</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="discount-surgical"></span>
                                                    <span>%</span></p>
                                                <p class="text-muted mt-2">Discount avarage</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1" id="lost-amt-surgical"></p>
                                                <p class="text-muted mt-2">lose amount in discount</p>
                                            </div>
                                        </div>

                                        <footer></footer>
                                    </div>
                                    <span class="badge badge-secondary">
                                    surgical
                                    <span id="floated-total-surgical"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                                </section>
                            </div>

                            <div class="col-lg-5">
                                <section class="widget widget-dark bg-dark small shadow-01">

                                    <div class="widget-block">
                                        <header>
                                            <h3 class="widget-title">X-Ray's</h3>
                                            <p class="text-muted">
                                                Total X-Ray are<span id="total-xray"></span>
                                            </p>
                                        </header>

                                        <p class="text-muted mt-2">
                                            See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                        </p>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-xray"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite of xray</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-outof-xray"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite out-of amount</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="discount-xray"></span>
                                                    <span>%</span></p>
                                                <p class="text-muted mt-2">Discount avarage</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1" id="lost-amt-xray"></p>
                                                <p class="text-muted mt-2">lose amount in discount</p>
                                            </div>
                                        </div>

                                        <footer></footer>
                                    </div>
                                    <span class="badge badge-secondary">
                                    xray
                                    <span id="floated-total-xray"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                                </section>
                            </div>

                            <div class="col-lg-12">
                                <section class="widget widget-dark bg-dark small shadow-01">

                                    <div class="widget-block">
                                        <header>
                                            <h3 class="widget-title">Laboratory's</h3>
                                            <p class="text-muted">
                                                Total Laboratory are<span id="total-lab"></span>
                                            </p>
                                        </header>

                                        <p class="text-muted mt-2">
                                            See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                        </p>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-lab"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite of lab</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="profite-outof-lab"></span>
                                                    <span>rs</span></p>
                                                <p class="text-muted mt-2">Profite out-of amount</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1">
                                                    <span id="discount-lab"></span>
                                                    <span>%</span></p>
                                                <p class="text-muted mt-2">Discount avarage</p>
                                            </div>
                                        </div>

                                        <div class="widget-content pt-1">
                                            <div>
                                                <p class="h1" id="lost-amt-lab"></p>
                                                <p class="text-muted mt-2">lose amount in discount</p>
                                            </div>
                                        </div>

                                        <footer></footer>
                                    </div>
                                    <span class="badge badge-secondary">
                                    lab
                                    <span id="floated-total-lab"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                                </section>
                            </div>


                        </div>

                    </div>
                </div>
            </section>
        </div>

    </div>
</div>
<?php
include($path . '/ui/include/wraper end.php');
?>
<script src="<?=$url?>/ui/public/js/report.js"></script>
