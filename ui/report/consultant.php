<?php

session_start();
include '../../vendor/autoload.php';

if(!is_admin()) {
    header("location: ".$_SERVER['HTTP_HOST']);
}


$title = 'With Consultant! Busness Report';
include('../include/wraper start.php');

?>
<div class="content-wrapper container-fluid px-5 mb-4 trans-03-in-out">
    <div class="row">

        <div class="col-lg-12 md-3">
            <section class="widget shadow-01 md-4" id="widget-01">
                <div class="widget-block">
                    <header>
                        <h3 class="widget-title">Consultant's Report</h3>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            <br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </header>

                    <div id="row">

                        <div id="status" class="container">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="from" class="col-form-label">From (start)</label>
                                <input type="date"
                                       id="from"
                                       class="form-control form-control-lg mb-1"
                                       onchange="consultantReport()"
                                >

                            </div>

                            <div class="form-group col-md-6">
                                <label for="to" class="col-form-label">To (end)</label>
                                <input type="date"
                                       id="to"
                                       class="form-control form-control-lg mb-1"
                                       onchange="consultantReport()"
                                >
                            </div>

                            <div class="form-group col-md-12">
                                <label for="consultant-id" class="col-form-label">Select Consultant</label>
                                <select autofocus
                                        id="patient-consultant-id"
                                        class="form-control form-control-lg mb-1"
                                        onchange="consultantReport()"
                                >
                                    <option value="">Consultant</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="report-box">

                        <div class="col-md-7">
                            <section class="widget border-1 border-dark" id="widget-18">
                                <div class="widget-block">

                                    <header class="">
                                        <h3 class="widget-title">Consultant Infomation</h3>
                                        <p class="text-muted">
                                            Some consultant introduction
                                        </p>
                                        <div class="service-icons">
                                            <button data-toggle="modal"
                                                    class="btn btn-icon-sm rounded-circle btn-transparent"
                                                    data-target="#sourceCodeModal"><i class="fa fa-code"
                                                                                      aria-hidden="true"></i></button>
                                        </div>
                                    </header>

                                    <div class="row">
                                        <div class="form-group col-md-6"><span class="badge badge-secondary">Consultant Id</span>
                                            <h3 class="h3" id="id"></h3>
                                        </div>
                                        <div class="form-group col-md-6"><span class="badge badge-secondary">Register Date</span>
                                            <h3 class="h3" id="consultant-register-date"></h3>
                                        </div>
                                        <div class="form-group col-md-6"><span class="badge badge-secondary">Last Modifed</span>
                                            <h3 class="h3" id="consultant-update"></h3>
                                        </div>
                                        <div class="form-group col-md-6"><span class="badge badge-secondary">User</span>
                                            <h3 class="h3" id="consultant-user"></h3>
                                        </div>

                                        <h5 class="col-md-4">Consultant
                                            <lable class="h4" id="consultant-perc"></lable>
                                            %
                                        </h5>
                                        <h5 class="col-md-4">Dental
                                            <lable class="h4" id="consultant-dental-perc"></lable>
                                            %
                                        </h5>
                                        <h5 class="col-md-4">Surgical
                                            <lable class="h4" id="consultant-surgical-perc"></lable>
                                            %
                                        </h5>
                                        <h5 class="col-md-4">Laboratory
                                            <lable class="h4" id="consultant-lab-perc"></lable>
                                            %
                                        </h5>
                                        <h5 class="col-md-4">X-Ray
                                            <lable class="h4" id="consultant-xray-perc"></lable>
                                            %
                                        </h5>

                                    </div>
                                    <footer>
                                        <a href="#">See More <i class="fa fa-long-arrow-right"
                                                                aria-hidden="true"></i></a>
                                    </footer>

                                </div>
                            </section>
                        </div>

                        <div class="col-lg-5">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Patient's</h3>
                                        <p class="text-muted">
                                            Total Paitent are <span id="total-patient"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-patient"></span>
                                                <span>rs</span>
                                            </p>
                                            <p class="text-muted mt-2">Profite of patient</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-patient"></span>
                                                <span>rs</span>
                                            </p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-patient"></span>
                                                <span>%</span>
                                            </p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-patient"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    patient
                                    <span id="floated-total-patient"> </span>
                                    <span>K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-8">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Dental's</h3>
                                        <p class="text-muted">
                                            Total Dental are <span id="total-dental"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-dental"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of dental</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-dental"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-dental"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-dental"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    dental
                                    <span id="floated-total-dental"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-4">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Surgical's</h3>
                                        <p class="text-muted">
                                            Total Surgical are <span id="total-surgical"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-surgical"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of surgical</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-surgical"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-surgical"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-surgical"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    surgical
                                    <span id="floated-total-surgical"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-5">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">X-Ray's</h3>
                                        <p class="text-muted">
                                            Total X-Ray are <span id="total-xray"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-xray"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of xray</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-xray"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-xray"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-xray"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    xray
                                    <span id="floated-total-xray"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                        <div class="col-lg-7">
                            <section class="widget widget-dark bg-dark small shadow-01">

                                <div class="widget-block">
                                    <header>
                                        <h3 class="widget-title">Laboratory's</h3>
                                        <p class="text-muted">
                                            Total Laboratory are <span id="total-lab"></span>
                                        </p>
                                    </header>

                                    <p class="text-muted mt-2">
                                        See More <i class="fa fa-level-down" aria-hidden="true"></i>
                                    </p>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-lab"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite of lab</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="profite-outof-lab"></span>
                                                <span>rs</span></p>
                                            <p class="text-muted mt-2">Profite out-of amount</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1">
                                                <span id="discount-lab"></span>
                                                <span>%</span></p>
                                            <p class="text-muted mt-2">Discount avarage</p>
                                        </div>
                                    </div>

                                    <div class="widget-content pt-1">
                                        <div>
                                            <p class="h1" id="lost-amt-lab"></p>
                                            <p class="text-muted mt-2">lose amount in discount</p>
                                        </div>
                                    </div>

                                    <footer></footer>
                                </div>
                                <span class="badge badge-secondary">
                                    lab
                                    <span id="floated-total-lab"></span>
                                    <span> K</span>
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </section>
                        </div>

                    </div>
                </div>
            </section>
        </div>

    </div>
</div>

<?= include('../include/wraper end.php') ?>
<script>
    let reportBox = $('#report-box');
    reportBox.hide();
</script>
<script src="<?=$url?>/ui/public/js/patient.js"></script>
<script>setNameFeeOfConsultant();</script>
<script src="<?=$url?>/ui/public/js/report.js"></script>