<?php
session_start();
// error_reporting(0);
require('vendor/autoload.php');

if (!isset($_REQUEST[SWITCH_TO])) {
    echo json_encode([STATUS => false]);
    exit;
}

/*
 * -------------------
 * integration from hospital-project API(back-end) to front-end
 * -------------------
 */
switch ($_REQUEST[SWITCH_TO]) {

    case 'consultant':
        require 'src/core/consultant/util_consultant.php';
        exit;

    case 'patient':
        require 'src/core/patient/util_patient.php';
        exit;

    case 'user':
        require 'src/core/user/util_user.php';
        exit;

    /*
     *  About Slip's
     */

    case 'slip':
        require 'src/core/slips/util_slip.php';
        exit;

    case 'slip_dental':
        $slip_obj = new DentalSlip();
        $product_table = DentalProduct::SQL_TABLE;
        $slip_identity = DentalSlip::IDENTITY;
        $slip_sql_table = DentalSlip::SQL_TABLE;
        require 'src/core/slips/util_slip.php';
        exit;

    case 'slip_lab_test':
        $slip_obj = new LaboratorySlip();
        $product_table = LaboratoryProduct::SQL_TABLE;
        $slip_identity = LaboratorySlip::IDENTITY;
        $slip_sql_table = LaboratorySlip::SQL_TABLE;
        require 'src/core/slips/util_slip.php';
        exit;

    case 'slip_surgical':
        $slip_obj = new SurgicalSlip();
        $product_table = SurgicalProduct::SQL_TABLE;
        $slip_identity = SurgicalSlip::IDENTITY;
        $slip_sql_table = SurgicalSlip::SQL_TABLE;
        require 'src/core/slips/util_slip.php';
        exit;

    case 'slip_xray':
        $slip_obj = new XraySlip();
        $product_table = XrayProduct::SQL_TABLE;
        $slip_identity = XraySlip::IDENTITY;
        $slip_sql_table = XraySlip::SQL_TABLE;
        require 'src/core/slips/util_slip.php';
        exit;


    /*
     *  About Products
     */
    case 'procedure_spacializ':
        require 'src/core/products/util_product_spacialization.php';
        exit;

    case 'procedure_surgical':

        $sql_table = SurgicalProduct::SQL_TABLE;
        $product = new SurgicalProduct();
        $slip_identity = SurgicalSlip::IDENTITY;
        require 'src/core/products/util_product.php';
        exit;

    case 'procedure_dental':

        $sql_table = DentalProduct::SQL_TABLE;
        $product = new DentalProduct();
        $slip_identity = DentalSlip::IDENTITY;
        require 'src/core/products/util_product.php';
        exit;

    case 'procedure_lab_test':

        $sql_table = LaboratoryProduct::SQL_TABLE;
        $product = new LaboratoryProduct();
        $slip_identity = LaboratorySlip::IDENTITY;
        require 'src/core/products/util_product.php';
        exit;

    case 'procedure_xray':

        $sql_table = XrayProduct::SQL_TABLE;
        $product = new XrayProduct();
        $slip_identity = XraySlip::IDENTITY;
        require 'src/core/products/util_product.php';
        exit;

    /*
     *  Information about website
     */

    case 'dashborad_info':
        require 'src/helper/dashboard.php';
        exit;

    case 'chart_info':
        require 'src/helper/chart.php';
        exit;

    /*
     * Logout application
     */
    case 'logout':
        session_destroy();
        exit;

    /*
     *
     */
    case 'report':
        require 'src/core/report/util_report.php';
        exit;

    /*
     * Search
     */
    case 'search':
        require 'src/core/search/search.php';
        exit;

}