<?php

interface identity_interface
{
    public function getId();

    public function getRegister(): string;

    public function getUpdate(): string;

    public function getName(): string;

    public function setName(string $name);

    public function getIsDisable(): bool;

    public function setIsDisable(bool $disable);

    public function getUserid(): int;

}

?>