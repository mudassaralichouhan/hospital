<?php


interface sql_io_interface
{
    function getAll(): array;

    function setAll(array $sql_data): bool;

    function delete(int $id): bool;

    function update(): bool;

    function insert(): bool;

    function fetch(): array;
}