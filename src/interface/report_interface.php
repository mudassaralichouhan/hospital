<?php


namespace report;


interface report_interface
{
    public function getLab(): array;

    public function getPatient(): array;

    public function getXray(): array;

    public function getDental(): array;

    public function getSurgical(): array;
}