<?php

trait CRUD_trait
{
    public function delete(int $id): bool
    {
        return (new SQL(self::SQL_TABLE))
            ->delete($id);
    }

    public function update(): bool
    {
        return (new SQL(self::SQL_TABLE))
            ->update($this->getId(), $this);
    }

    public function insert(): bool
    {
        return (new SQL(self::SQL_TABLE))
            ->insert($this);
    }

    public function fetch(): array
    {
        return [];
    }
}