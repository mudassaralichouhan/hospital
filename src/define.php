<?php

/*
 * globle Uniq identity
 */
define('SESSION', 'login');     // Store session cred identity.

/*
 * these are for coumunicate for status
 */
define('MESSAGE', 'msg');
define('STATUS', 'status');

/*
 * use for ajax function from client-side
 */
define('ACTION', 'action');
define('SWITCH_TO', 'switch_to');
define('R_ARRAY', 'data_arr');

/*
 * standar method for ACTION e.g CRUD
 */
define('INSERT', 'insert');
define('FETCH', 'fetch');
define('DELETE', 'delete');
define('UPDATE', 'update');
define('LIMIT', 'limit');
define('PAGINATION', 'pagination');
define('HEADERS', 'headers');
define('DATA', 'data');