<?php

abstract class Sql_IO_Abstructioin
{
    abstract protected function retriveAll(string $column, string $value, array $select): array;

    abstract protected function retrive(string $column, string $value, array $select): array;

    abstract protected function insert(\sql_io_interface $arr): bool;

    abstract protected function delete(int $id): bool;

    abstract protected function update(int $id, \sql_io_interface $data): bool;
}

?>