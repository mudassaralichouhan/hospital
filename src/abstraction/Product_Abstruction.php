<?php


abstract class Product extends Identity_Abstraction
{
    /*
     * SQL database table columns names
     */
    private const PRICE_COL = 'price';

    private int $price=0;

    public function __construct()
    {
        parent::__construct();
    }

//    Lab test price
    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice(int $price): bool
    {
        if ($price >= 0) {
            $this->price = $price;
            return true;
        }
        return false;
    }

//==================================================================
// SQL Management

    public function getAll(): array
    {
        return $data = [
            parent::NAME_COL => $this->getName(),
            parent::DELETE_COL => $this->getIsDisable(),
            parent::USER_COL => $this->getUserid(),

            self::PRICE_COL => $this->getPrice()
        ];
    }

    function setAll(array $dataList): bool
    {
        if (empty($dataList))
            return false;

        $this->setId($dataList[parent::ID_COL]);
        $this->setName($dataList[parent::NAME_COL]);
        $this->setUserid($dataList[parent::USER_COL]);
        $this->setRegister($dataList[parent::REGISTER_COL]);
        $this->setUpdate($dataList[parent::UPDATE_COL]);
        $this->setIsDisable($dataList[parent::DELETE_COL]);

        $this->setPrice($dataList[self::PRICE_COL]);

        return true;
    }
}