<?php

abstract class Identity_Abstraction implements identity_interface
{
    // member instance private variable
    private int $id=0, $user_id=0;
    private string $name='', $register='', $update='';
    private bool $is_disable = false;

    protected const ID_COL = 'id';
    protected const NAME_COL = 'name';
    protected const USER_COL = 'user_id';

    protected const REGISTER_COL = 'reg_on';
    protected const UPDATE_COL = 'update_on';
    protected const DELETE_COL = 'is_disable';

    public function __construct()
    {
        // date_default_timezone_set('Asia/Karachi');
        $this->setUserid($_SESSION[SESSION]['id']);
    }

// id
    public function getId(): int
    {
        return $this->id;
    }

    protected function setId(int $id): bool
    {
        if ($id > 0) {
            $this->id = $id;
            return true;
        }
        return false;
    }

// name
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): bool
    {
        if (strlen($name) >= 3 and strlen($name) <= 100) {
            $this->name = ucString($name);
            return true;
        }
        return false;
    }

// enter date on database
    public function getRegister(): string
    {
        return $this->register;
    }

    protected function setRegister(string $date): bool
    {
        $this->register = $date;
        return true;
    }

// last update on database of this row
    public function getUpdate(): string
    {
        return $this->update;
    }

    protected function setUpdate($date): bool
    {
        if (is_string($date)) {
            $this->update = $date;
            return true;
        }
        return false;
    }

// deleted record is store but not deleted
    public function getIsDisable(): bool
    {
        return $this->is_disable;
    }

    public function setIsDisable(bool $disable): bool
    {
        if (is_bool($disable)) {
            $this->is_disable = $disable;
            return $this->is_disable;
        }
        return false;
    }

// who register this person not compalary for employ(user)
    public function getUserid(): int
    {
        return $this->user_id;
    }

    protected function setUserid(int $id): bool
    {
        if ($id > 0) {
            $this->user_id = $id;
            return true;
        }
        return false;
    }

    public function getIssuedUserByName(): String
    {
        $sql = new SQL(User::SQL_TABLE);
        $consultant_info = $sql->retrive('id', $this->getUserid(), ['name']);
        if (empty($consultant_info)) {
            return '';
        }
        return $consultant_info['name'];
    }

}