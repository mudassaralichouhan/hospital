<?php

abstract class Slip_Abstruction extends Identity_Abstraction
{
    /*
     * SQL database table colunms names
     */
    private const PATIENT_ID_COL = 'patient_id';
    private const PRCEDURE_ID_COL = 'type_of';
    private const DISCOUNT_COL = 'discount';
    private const REMAIN_AMT_COL = 'remain_amt';
    private const PRICE_COL = 'price';
    private const GETTED_AMT_COL = 'get_amt';

//  Member instance variable
    private int $patientId = 0;
    private int $price = 0;
    private int $paid = 0, $discount = 0;
    private int $remaining = 0;

//  consturctor
    public function __construct($name)
    {
        parent::__construct();
        $this->setName($name);
    }

//  patient id
    public function getPatientId(): int
    {
        return $this->patientId;
    }

    public function setPatientId(int $id)
    {
        if ($id > 0) {
            $this->patientId = $id;
            return true;
        }
        return false;
    }

//  Recive amount
    public function getPaid(): int
    {
        return $this->paid;
    }

    private function setPaid(int $get_amt = 0): bool
    {
        if ($get_amt > 0) {
            $this->paid = $get_amt;
        }
        return false;
    }

//  Remaining Amount
    public function getRemain(): int
    {
        return $this->remaining;
    }

    public function setRemain(int $amt): bool
    {

        if ( $amt > 0 and $amt <= $this->getRemain() ) {
            $this->remaining = $this->getRemain() - $amt;
            return true;
        }
        return false;
    }

//  Discount
    public function getDiscount(): int
    {
        return $this->discount;
    }

    public function setDiscount(int $dis = 0)
    {
        if ($dis >= 0 and $dis <= 100) {
            $this->discount = $dis;

            $paid_amount_discounted = ($this->price / 100) * (100 - $this->discount);
            $this->setPaid($paid_amount_discounted);

            return true;
        }
        return false;
    }

//  Price
    public function getFee(): int
    {
        return $this->price;
    }

    protected function setFee(int $fee)
    {
        if ($fee >= 0) {
            $this->price = $fee;
            $paid_amount_discounted = ($this->price / 100) * (100 - $this->getDiscount());
            $this->setPaid($paid_amount_discounted);

            $this->setRemain( $this->getPaid() );

            return true;
        }
        return false;
    }

//  type of
    abstract public function getProcedureId(): int;

    abstract public function setProcedureId(int $id);
//=====================================================================
// SQL Management

    function getAll(): array
    {
        return $dataList = [
            parent::NAME_COL => $this->getName(),
            self::PATIENT_ID_COL => $this->getPatientId(),
            parent::USER_COL => $this->getUserid(),
            self::PRCEDURE_ID_COL => $this->getProcedureId(),
            self::PRICE_COL => $this->getFee(),
            self::DISCOUNT_COL => $this->getDiscount(),
            self::GETTED_AMT_COL => $this->getPaid(),
            self::REMAIN_AMT_COL => $this->getRemain()
        ];
    }

    function setAll(array $sql_data): bool
    {
        if (empty($sql_data))
            return false;

        $this->setId($sql_data[parent::ID_COL]);
        $this->setName($sql_data[parent::NAME_COL]);
        $this->setPatientId($sql_data[self::PATIENT_ID_COL]);
        $this->setUserid($sql_data[parent::USER_COL]);
        $this->setProcedureId($sql_data[self::PRCEDURE_ID_COL]);

        $this->setDiscount($sql_data[self::DISCOUNT_COL]);
        $this->remaining = $sql_data[self::REMAIN_AMT_COL];

        $this->setRegister($sql_data[parent::REGISTER_COL]);
        $this->setUpdate($sql_data[parent::UPDATE_COL]);
        $this->setIsDisable($sql_data[parent::DELETE_COL]);

        return true;
    }
}