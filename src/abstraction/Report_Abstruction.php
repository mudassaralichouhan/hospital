<?php


namespace report;

abstract class Report_Abstruction implements report_interface
{
    private int $id;
    private string $from='', $to='';

    /**
     * Report_Abstruction constructor.
     */
    public function __construct()
    {
        $this->from = Date('Y-m-d') . ' 00:00:00';
        $this->to = Date('Y-m-d') . ' 23:59:59';
    }

// id
    public function getId(): int
    {
        return $this->id;
    }

    protected function setId(int $id)
    {
        $this->id = $id;
    }

//  Date
    public function setFrom($from_date): bool
    {
        $to = explode(' ', $this->to)[0];
        if($to > $from_date)
        {
            $this->from = $from_date . ' 00:00:00';
            return true;
        }

        return false;
    }

    public function setTo($to_date): bool
    {
        $from = explode(' ', $this->from)[0];
        if($from < $to_date)
        {
            $this->to = $to_date . ' 23:59:59';
            return true;
        }

        return false;
    }

    public function getDateSqlFormate()
    {
        return "(`reg_on` >= '$this->from' AND `reg_on` <= '$this->to')";
    }

}