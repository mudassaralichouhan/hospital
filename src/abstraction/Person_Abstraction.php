<?php


abstract class Person_Abstruction extends Identity_Abstraction
{

    private int $age=0;
    private string $gender, $contact, $location = 'Nothing else';

    /*
     * SQL database tables columns names
     */
    protected const AGE_COL = 'age';
    protected const GENDER_COL = 'gender';
    protected const CONTACT_COL = 'contact';
    protected const ADDRESS_COL = 'location';

    /**
     * Person_Abstruction constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

// age
    public function getAge(): int
    {
        return $this->age;
    }

    public function setAge(int $age): bool
    {
        if ($age >= 0 and $age < 200) {
            $this->age = $age;
            return true;
        }
        return false;
    }

// gender
    public function getGender(): string
    {
        return $this->gender;
    }

    public function setGender(string $gender)
    {
        if ($gender === "Male" or $gender === "Female") {
            $this->gender = $gender;
            return true;
        }
        return false;
    }

// contact
    public function getContact(): string
    {
        return $this->contact;
    }

    public function setContact(string $contact)
    {
        $pattren = '/^((\(((\+|00)92)\)|(\+|00)92)(( |\-)?)(3[0-9]{2})\6|0(3[0-9]{2})( |\-)?)[0-9]{3}( |\-)?[0-9]{4}$/';
        // 0300 123 12 12   +92 00 123 12 12
//        $contact=substr_replace($contact,'+923',0,2);
        if (is_numeric($contact) and strlen($contact) === 11 and preg_match($pattren,$contact)) {
            $this->contact = $contact;
            return true;
        }
        return false;
    }

// location
    public function getLocation(): string
    {
        return $this->location;
    }

    public function setLocation(string $location)
    {
        if (strlen($location) > 6 and strlen($location) < 150) {
            $this->location = ucfirst($location);
            return true;
        }
        return false;
    }

}

?>