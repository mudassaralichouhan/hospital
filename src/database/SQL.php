<?php

class SQL extends Sql_IO_Abstructioin
{
    private $conn = null;
    private $table = null;


    public function __construct($table)
    {
        // we have only one database there for :)
        $this->conn = Database::getConnection();
        if($this->conn === null) {

            echo json_encode([
                MESSAGE => 'Database connection error!!!',
                STATUS => false
            ]);
            exit;
        }
		date_default_timezone_set('Asia/Karachi');
        $this->table = $table;
        try {
            $stmt = $this->conn->prepare(' set time_zone = \'+05:00\' ');
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    public function table(string $table)
    {
        $this->table = $table;
    }

    public function fetchByQuery(String $query, array $select = ['*']): array
    {
        $select = $this->selectFormate($select);
        $sql = "SELECT $select FROM $this->table WHERE `is_disable`=0 AND $query";
        try { // echo $sql;exit;

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            if (!empty($result)) {
                return $result;
            }
            return [];

        } catch (PDOException $e) {
            echo "ERR fetching table: " . $e->getMessage();
            return [];
        }
    }

    public function fetchByQueryAll(String $query, array $select = ['*']): array
    {
        $select = $this->selectFormate($select);
        $sql = "SELECT $select FROM $this->table WHERE `is_disable`=0 AND $query";

        try { // echo $sql;exit;
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);

        } catch (PDOException $e) {

            echo "ERR fetching table: " . $e->getMessage();
            return [];
        }
    }

    public function retrive(string $column, string $value, array $select = ['*']): array
    {
        $select = $this->selectFormate($select);
        $sql = "SELECT $select FROM $this->table WHERE `is_disable`=0 AND `$column`='$value'";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            if (!empty($result)) {
                return $result;
            }
            return [];
        } catch (PDOException $e) {
           echo "ERR fetching table: " . $e->getMessage();
            return [];
        }
    }

    public function retriveAll(string $column, string $value, array $select = ['*']): array
    {
        $select = $this->selectFormate($select);
        $sql = "SELECT $select FROM $this->table WHERE `is_disable`=0 AND `$column`='$value' ";
        try {

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            return ($stmt->fetchAll(\PDO::FETCH_ASSOC));

        } catch (PDOException $e) {
            echo "ERR fetching table: " . $e->getMessage();
            return [];
        }
    }

    public function insert(\sql_io_interface $data): bool
    {
        /* inserting the form for sql update formate */
        $column = "";
        $cell = "";
        foreach ($data->getAll() as $key => $value) {
            if ($value == "")
                continue;
            $column = $column . "`$key`^";
            $cell = $cell . '"' . $value . '"^';
        }
        $column = str_replace("^", ",", rtrim($column, '^'));
        $cell = str_replace("^", ",", rtrim($cell, '^'));
        /* /inserting the form for sql update formate */

        $sql = "INSERT INTO $this->table " . " ($column) VALUES($cell)";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return true;

        } catch (PDOException $e) {

            echo "ERR insert data: " . $e->getMessage();
            return false;

        }
    }

    public function update(int $id, \sql_io_interface $data): bool
    {
        /* editing the form for sql update formate */
        $formate = "";
        foreach ($data->getAll() as $key => $value) {
            if ($value === '')
                continue;
            $formate = $formate . "`$key`='$value'^";
        }
        $formate = str_replace("^", ",", rtrim($formate, '^'));
        /* /editing the form for sql update formate */

        $sql = "UPDATE $this->table SET " . " $formate WHERE `is_disable`=0 AND `id`='$id'";
        try {
            $stmt = $this->conn->prepare($sql);
            return $stmt->execute();

        } catch (PDOException $e) {

            echo "ERR update data: " . $e->getMessage();
            return false;
        }
    }

    public function updateByQuery(int $id, string $column, string $value): bool
    {
        $sql = "UPDATE $this->table SET `$column`='$value' WHERE `is_disable`=0 AND `id`= '$id'";
        try {
            $stmt = $this->conn->prepare($sql);
            return $stmt->execute();

        } catch (PDOException $e) {

            echo "ERR update data: " . $e->getMessage();
            return false;
        }
    }

    public function delete(int $id): bool
    {
        $sql = "UPDATE $this->table SET `is_disable`=1 WHERE `id`='$id'";
        try {

            $stmt = $this->conn->prepare($sql);
            return $stmt->execute();

        } catch (PDOException $e) {

            echo "ERR deleting data: " . $e->getMessage();
            return false;
        }
    }

    public function countByQuery($stat): int
    {
        try {

            $sql = "SELECT COUNT(*) FROM $this->table WHERE `is_disable`=0 AND $stat";
            $result = $this->conn->prepare($sql);
            $result->execute();

            return $result->fetchColumn();

        } catch (PDOException $e) {
            echo "ERR row count: " . $e->getMessage();
            return false;
        }
    }

    public function count(): int
    {
        try {

            $sql = 'SELECT COUNT(*) FROM ' . $this->table;
            $result = $this->conn->prepare($sql);
            $result->execute();

            return $result->fetchColumn();

        } catch (PDOException $e) {
            echo "ERR row count: " . $e->getMessage();
            return false;
        }
    }

    private function selectFormate(array $select)
    {
        $s = '';
        foreach ($select as $value) {
            $s = $s . "$value^";
        }
        return str_replace("^", ",", rtrim($s, '^'));
    }

}