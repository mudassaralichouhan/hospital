<?php

class Database
{
    private const HOST = 'localhost';
    private const DB_NAME = 'hospital';
    private const USERNAME = 'root';
    private const PASSWORD = '';

    public function createTable($sqlTable)
    {
        try {
            $conn = self::getConnection();
            $conn->exec($sqlTable);
            return true;
        } catch (PDOException $e) {
            echo "ERR create table: " . $e->getMessage();
        }
    }

    public static function getConnection()
    {
        try {
            $host = self::HOST;
            $db_name = self::DB_NAME;
            $connection = new PDO("mysql:host=$host; dbname=$db_name", self::USERNAME, self::PASSWORD);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;

        } catch (PDOException $e) {
            // echo 'ERR connection db: ' . $e->getMessage();
            return null;
        }
    }
}

?>