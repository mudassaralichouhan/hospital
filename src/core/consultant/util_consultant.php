<?php

switch ($_GET[ACTION]) {
    case INSERT:
        if(is_admin()) {
            consultant_insert($_GET[R_ARRAY]);
        } else {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
        }
        exit;
    case DELETE:
        if(is_admin()) {
            consultant_delete($_GET[R_ARRAY]['id']);
        } else {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
        }
        exit;
    case UPDATE:
        if(is_admin()) {
            $consultant_info = $_GET[R_ARRAY];
            consultant_update($consultant_info['id'], $consultant_info);
        } else {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
        }
        exit;
    case FETCH:
            consultant_fetch($_GET[R_ARRAY]['id']);
        exit;
    case LIMIT:
        if(is_admin()) {
            $data = $_GET[R_ARRAY];
            consultant_with_limit($data['page']);
        } else {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
        }
        exit;
    case 'name_fee':
        consutant_name_fee();
        exit;

}

function consultant_insert(array $data)
{
    $sql = new SQL(Consultant::SQL_TABLE);
    if ($sql->retrive('contact', $data['consult_contact'], ['contact']) !== []) {
        echo json_encode([
            MESSAGE => ' Already exist contact=' . $data['consult_contact'],
            STATUS => false
        ]);
    } else {

        $consultant = new Consultant();

        $consultant->setSpacialization($data['consult_specializ']);
        $consultant->setName($data['consult_name']);
        $consultant->setAge($data['consult_age']);
        $consultant->setGender($data['consult_gender']);
        $consultant->setContact($data['consult_contact']);
        $consultant->setLocation($data['consult_address']);
        $consultant->setFee($data['consult_fee']);

        $consultant->setConsultantPer($data['consult_consultant_per']);
        $consultant->setLabPer($data['consult_lab_per']);
        $consultant->setXrayPer($data['consult_xray_per']);
        $consultant->setSurgicalPer($data['consult_surgical_per']);
        $consultant->setDentalPer($data['consult_dental_per']);

        if ($consultant->insert() === true) {
            echo json_encode([
                MESSAGE => 'Insert Successfull=',
                STATUS => true
            ]);
        }
    }
}

function consultant_update(int $id, array $data)
{
    $consultant = new Consultant($id);

    $consultant->setSpacialization($data['consult_specializ']);
    $consultant->setName($data['consult_name']);
    $consultant->setGender($data['consult_gender']);
    $consultant->setAge($data['consult_age']);
    $consultant->setContact($data['consult_contact']);
    $consultant->setLocation($data['consult_address']);
    $consultant->setFee($data['consult_fee']);

    $consultant->setConsultantPer($data['consult_consultant_per']);
    $consultant->setLabPer($data['consult_lab_per']);
    $consultant->setXrayPer($data['consult_xray_per']);
    $consultant->setSurgicalPer($data['consult_surgical_per']);
    $consultant->setDentalPer($data['consult_dental_per']);

    if ($consultant->update()) {
        echo json_encode([
            MESSAGE => ' Update Successfull ',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => ' Update un-Successfull ',
            STATUS => false
        ]);
    }
}

function consultant_delete(int $id)
{
    if ((new Consultant())->delete($id) === true) {
        echo json_encode([
            MESSAGE => 'Deleted Successfull',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Not Delete something was wrong',
            STATUS => false
        ]);
    }
}

function consutant_name_fee()
{
    $sql = new SQL(Consultant::SQL_TABLE);

    $consultant = $sql->fetchByQueryAll('true ORDER BY name', $select = ['id', 'name', 'fee']);

    echo json_encode($consultant);
}

function consultant_fetch(int $id)
{
    $sql = new SQL(Consultant::SQL_TABLE);
    $user_sql = $sql->retrive('id', $id, [
        'id',
        'name',
        'gender',
        'contact',
        'age',
        'specialization',
        'fee',
        'user_id',
        'location',
        'consultant_perc',
        'lab_perc',
        'dental_perc',
        'surgical_perc',
        'xray_perc',
        'update_on',
        'reg_on'
    ]);

    if (!empty($user_sql)) {

        $sql->table(User::SQL_TABLE);
        $user = $user_sql['user_id'];
        $user = $sql->retrive('id', $user, ['name']);
        if (!empty($user)) {
            $user_sql['user_id'] = $user['name'];
        }

        echo json_encode($user_sql);
    } else {

        echo json_encode([
            MESSAGE => 'Consulltant not found',
            'status' => false
        ]);

    }
}

function consultant_with_limit(int $page)
{
    $sql = new SQL(Consultant::SQL_TABLE);
    $pag = new Pagination($page, $_SESSION[SESSION]['explode'], $_SESSION[SESSION]['button'], $sql->count());

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = "true ORDER BY name LIMIT $offset , " . $pag->getPerPageRec();
    $consultant = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'gender',
        'contact',
        'specialization',
        'fee',
        'consultant_perc'
    ]);

    if (!empty($consultant)) {

        $sql->table(Spacialization::SQL_TABLE);

        for ($i = 0; $i < sizeof($consultant); $i++) {
            $specialization_id = $consultant[$i]['specialization'];
            $specialization_name = $sql->retrive('id', $specialization_id, ['name']);
            if ($specialization_name) {
                $consultant[$i]['specialization'] = $specialization_name['name'];
                continue;
            }
            $consultant[$i]['specialization'] = 'Not Found';
        }

        echo json_encode([
            HEADERS => ['Name', 'Gender', 'Contact', 'Specialization', 'Fee', 'Consult Perc(%)'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $consultant
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            STATUS => false
        ]);
    }
}