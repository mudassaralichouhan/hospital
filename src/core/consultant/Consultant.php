<?php

class Consultant extends Person_Abstruction implements sql_io_interface
{
    const SQL_TABLE = "consultant";
    /*
     * SQL database table columns names
     */
    private const FEE_COL = 'fee';
    private const SPECIALIZ_COL = 'specialization';
    private const X_RAY_PERC_COL = "xray_perc";
    private const CONSULTANT_PERC_COL = "consultant_perc";
    private const LABORATORY_PERC_COL = "lab_perc";
    private const DENTAL_PERC_COL = "dental_perc";
    private const SUGICAL_PERC_COL = "surgical_perc";

    use CRUD_trait;

//  consultant member variable
    private $consultant_perc = 0, $lab_perc = 0, $dental_perc = 0, $surgical_perc = 0, $xRay_perc = 0;
    private $spacialization = null, $fee = 0;

//  constructor
    public function __construct(int $id = 0)
    {
        parent::__construct();

        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $consutlant_info = $sql->retrive('id', $this->getId());
            $this->setAll($consutlant_info);
        }
    }

//  consultant spacialization
    public function getSpacilization(): int
    {
        return $this->spacialization;
    }

    public function setSpacialization(int $spacial_product_id)
    {
        if ($spacial_product_id > 0) {
            $this->spacialization = $spacial_product_id;
            return true;
        }
        return false;
    }

//  consultant percentage
    public function getConsultantPer(): int
    {
        return $this->consultant_perc;
    }

    public function setConsultantPer($per = 0)
    {
        if ($per >= 0 and $per <= 100) {
            $this->consultant_perc = $per;
            return true;
        }
        return false;
    }

//  consultant X-Ray percentage
    public function getXrayPer(): int
    {
        return $this->xRay_perc;
    }

    public function setXrayPer(int $per = 0)
    {
        if ($per >= 0 and $per <= 100) {
            $this->xRay_perc = $per;
            return true;
        }
        return false;
    }

//  consultant lab test percentage
    public function getLabPer(): int
    {
        return (int)$this->lab_perc;
    }

    public function setLabPer(int $per = 0)
    {
        if ($per >= 0 and $per <= 100) {
            $this->lab_perc = $per;
            return true;
        }
        return false;
    }

//  consultant Dental percentage
    public function getDentalPer(): int
    {
        return $this->dental_perc;
    }

    public function setDentalPer(int $per = 0)
    {
        if ($per >= 0 and $per <= 100) {
            $this->dental_perc = $per;
            return true;
        }
        return false;
    }

//  consultant surgical percentage
    public function getSurgical_per(): int
    {
        return $this->surgical_perc;
    }

    public function setSurgicalPer(int $per = 0)
    {
        if ($per >= 0 and $per <= 100) {
            $this->surgical_perc = $per;
            return true;
        }
        return false;
    }

//  consultant fee
    public function getFee(): int
    {
        return $this->fee;
    }

    public function setFee(int $fee = 0)
    {
        if ($fee >= 0) {
            $this->fee = $fee;
            return true;
        }
        return false;
    }

    /*
     * ----------------------------
     * SQL Management
     * ---------------------------
     */

    public function getAll(): array
    {
        return $data = [
            parent::NAME_COL => $this->getName(),
            parent::AGE_COL => $this->getAge(),
            parent::GENDER_COL => $this->getGender(),
            parent::CONTACT_COL => $this->getContact(),
            parent::ADDRESS_COL => $this->getLocation(),

            self::SPECIALIZ_COL => $this->getSpacilization(),
            self::FEE_COL => $this->getfee(),

            self::X_RAY_PERC_COL => $this->getXrayPer(),
            self::CONSULTANT_PERC_COL => $this->getConsultantPer(),
            self::LABORATORY_PERC_COL => $this->getLabPer(),
            self::DENTAL_PERC_COL => $this->getDentalPer(),
            self::SUGICAL_PERC_COL => $this->getSurgical_per()

        ];
    }

    public function setAll(array $sql_data): bool
    {
        if (empty($sql_data))
            return false;

        $this->setId($sql_data[parent::ID_COL]);
        $this->setName($sql_data[parent::NAME_COL]);
        $this->setAge($sql_data[parent::AGE_COL]);
        $this->setGender($sql_data[parent::GENDER_COL]);
        $this->setLocation($sql_data[parent::ADDRESS_COL]);
        $this->setContact($sql_data[parent::CONTACT_COL]);

        $this->setSpacialization($sql_data[self::SPECIALIZ_COL]);
        $this->setFee($sql_data[self::FEE_COL]);

        $this->setXrayPer($sql_data[self::X_RAY_PERC_COL]);
        $this->setConsultantPer($sql_data[self::CONSULTANT_PERC_COL]);
        $this->setLabPer($sql_data[self::LABORATORY_PERC_COL]);
        $this->setDentalPer($sql_data[self::DENTAL_PERC_COL]);
        $this->setSurgicalPer($sql_data[self::SUGICAL_PERC_COL]);


        $this->setRegister($sql_data[parent::REGISTER_COL]);
        $this->setUpdate($sql_data[parent::UPDATE_COL]);
        $this->setIsDisable($sql_data[parent::DELETE_COL]);

        return true;
    }

}

?>