<?php

switch ($_GET[ACTION]) {
    case INSERT:

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }


        spacializ_insert($_GET[R_ARRAY]);
        exit;

    case DELETE:

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }


        spacializ_delete($_GET[R_ARRAY]['id']);
        exit;

    case UPDATE:

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }


        $info = $_GET[R_ARRAY];
        spacializ_update($info);
        exit;

    case FETCH:

        spacializ_fetch($_GET[R_ARRAY]['id']);
        exit;

    case 'name_fee':

        spacializ_name_fee();
        exit;

    case LIMIT:

        $data = $_GET[R_ARRAY];
        spacializ_with_limit($data['offset'], $data['rows']);
        exit;

    case 'list':

        spacializ_list();
        exit;
}

function spacializ_list()
{
    $sql = new SQL(Spacialization::SQL_TABLE);

    $query = "true ORDER BY name";
    $s = $sql->fetchByQueryAll($query, ['id,name']);

    if (!empty($s)) {
        echo json_encode($s);
    } else {
        echo json_encode([
            STATUS => false,
            MESSAGE => 'Not fetching something was wrong'
        ]);
    }
}

function spacializ_insert(array $data)
{

    $sql = new SQL(Spacialization::SQL_TABLE);
    $spacializ = new Spacialization();

    if ($sql->retrive('name', $data['spacializ_name'], ['name']) !== []) {
        echo json_encode([
            MESSAGE => ' Already exist name=' . $data['spacializ_name'],
            STATUS => false
        ]);

    } else {

        $spacializ->setName($data['spacializ_name']);

        if ($spacializ->insert() === true) {
            echo json_encode([
                MESSAGE => 'Insert Successfull=',
                STATUS => true
            ]);
        }
    }
}

function spacializ_name_fee()
{
    $sql = new SQL(Spacialization::SQL_TABLE);
    $query = "true ORDER BY name";
    $s = $sql->fetchByQueryAll($query, ['id', 'name', 'price']);
    echo json_encode($s);
}

function spacializ_delete(int $id)
{
    if ((new Spacialization())->delete($id) === true) {

        echo json_encode([
            MESSAGE => 'Deleted Successfull',
            STATUS => true
        ]);

    } else {

        echo json_encode([
            MESSAGE => 'Not Delete something was wrong',
            STATUS => false
        ]);
    }
}

function spacializ_update(array $data)
{
    $spacializ = new Spacialization($data['id']);

    $spacializ->setName($data['spacializ_name']);

    if ($spacializ->update()) {
        echo json_encode([
            MESSAGE => 'Update Successfull=',
            STATUS => true
        ]);
    }
}

function spacializ_fetch(int $id)
{
    $sql = new SQL(Spacialization::SQL_TABLE);
    $s = $sql->retrive('id', $id, ['id', 'name']);

    if (!empty($s)) {
        echo json_encode($s);
    } else {

        echo json_encode([
            MESSAGE => 'Patient not found',
            'status' => false
        ]);

    }
}

function spacializ_with_limit(int $offset, int $rows)
{
    $sql = new SQL (Spacialization::SQL_TABLE);

    $query = "true ORDER BY name LIMIT " . $offset . " , " . $rows;
    $s = $sql->fetchByQueryAll($query);

    if (!empty($s)) {
        echo json_encode($s);
    } else {
        echo json_encode([
            MESSAGE => 'Not fetching spacialization with limit',
            'status' => false
        ]);
    }
}