<?php


class LaboratoryProduct extends Product implements sql_io_interface
{
    const SQL_TABLE = 'product_lab_test';

    use CRUD_trait;

    public function __construct(int $id = 0)
    {
        parent::__construct();

        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $info = $sql->retrive(parent::ID_COL, $this->getId());
            $this->setAll($info);
        }
    }

}