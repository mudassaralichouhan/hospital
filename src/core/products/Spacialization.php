<?php

class Spacialization extends Identity_Abstraction implements sql_io_interface
{
    const SQL_TABLE = 'doctor_specilization';

    use CRUD_trait;

    public function __construct(int $id = 0)
    {
        parent::__construct();

        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $info = $sql->retrive('id', $this->getId());
            $this->setAll($info);
        }
    }


//==================================================================
// SQL Management

    public function getAll(): array
    {
        return $data = [
            parent::NAME_COL => $this->getName(),
            parent::USER_COL => $this->getUserid()
        ];
    }

    public function setAll(array $sql_data = []): bool
    {
        if (empty($sql_data))
            return false;

        $this->setId($sql_data[parent::ID_COL]);
        $this->setName($sql_data[parent::NAME_COL]);
        $this->setUserid($sql_data[parent::USER_COL]);
        $this->setRegister($sql_data[parent::REGISTER_COL]);
        $this->setUpdate($sql_data[parent::UPDATE_COL]);
        $this->setIsDisable(parent::DELETE_COL);

        return true;
    }
}