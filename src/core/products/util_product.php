<?php

switch ($_GET[ACTION]) {

    case 'name_fee':
        product_name_fee($sql_table);
        exit;
    case 'fetch_with_patient_limit':
        $data = $_GET[R_ARRAY];
        fetch_with_patient_limit($slip_identity, $data['id'], $data['page']);
        exit;
    case INSERT:

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }

        product_insert($product, $sql_table, $_GET[R_ARRAY]);
        exit;
    case DELETE:

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }
        product_delete($product, $_GET[R_ARRAY]['id']);
        exit;
    case UPDATE:

        if(!is_admin()) {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
            exit;
        }

        $info = $_GET[R_ARRAY];
        product_update($product, $info);
        exit;
    case FETCH:
        product_fetch($sql_table, $_GET[R_ARRAY]['id']);
        exit;
    case LIMIT:
        $data = $_GET[R_ARRAY];
        product_with_limit($sql_table, $data['offset'], $data['rows']);
        exit;
}

function product_insert(object $product, string $sql_table, array $data)
{

    $sql = new SQL($sql_table);

    if ($sql->retrive('name', $data['product_name'], ['name']) !== []) {
        echo json_encode([
            MESSAGE => ' Already exist name=' . $data['product_name'],
            STATUS => false
        ]);

    } else {

        $product->setName($data['product_name']);
        $product->setPrice($data['product_price']);

        if ($product->insert() === true) {
            echo json_encode([
                MESSAGE => 'Insert Successfull=',
                STATUS => true
            ]);
        }
    }
}

function product_name_fee(string $sql_table)
{
    $sql = new SQL($sql_table);
    $query = "true ORDER BY name";
    $s = $sql->fetchByQueryAll($query, ['id', 'name', 'price']);
    echo json_encode($s);
}

function product_delete(object $product, int $id)
{
    if ($product->delete($id) === true) {

        echo json_encode([
            MESSAGE => 'Deleted Successfull',
            STATUS => true
        ]);

    } else {

        echo json_encode([
            MESSAGE => 'Not Delete something was wrong',
            STATUS => false
        ]);
    }
}

function product_update(object $product, array $data)
{
    $product->__construct($data['id']);
    $product->setName($data['product_name']);
    $product->setPrice((int)$data['product_price']);

    if ($product->update()) {
        echo json_encode([
            MESSAGE => 'Update Successfull=',
            STATUS => true
        ]);
    }
}

function product_fetch(string $sql_table, int $id)
{
    $sql = new SQL($sql_table);
    $product = $sql->retrive('id', $id, ['*']);

    if (!empty($product)) {

        $sql->table(User::SQL_TABLE);
        $user = $sql->fetchByQuery(' `id`=' . $product['user_id'], ['name']);
        if (!empty($user)) {
            $product['user_id'] = [
                $user['name'],
                $product['user_id']
            ];
        } else {
            $product['user_id'] = [
                'user not found',
                $product['user_id']
            ];
        }

        echo json_encode($product);

    } else {

        echo json_encode([
            MESSAGE => 'Product not found',
            'status' => false
        ]);

    }
}

function product_with_limit(string $sql_table, int $offset, int $rows)
{
    $sql = new SQL ($sql_table);

    $query = "true ORDER BY name LIMIT " . $offset . " , " . $rows;
    $s = $sql->fetchByQueryAll($query);

    if (!empty($s)) {
        echo json_encode($s);
    } else {
        echo json_encode([
            MESSAGE => 'Not fetching spacialization with limit',
            'status' => false
        ]);
    }
}

function fetch_with_patient_limit($slip_identity, $id, $page)
{

    $sql = new SQL (DentalSlip::SQL_TABLE);$pag = new Pagination($page,
        $_SESSION[SESSION]['explode'],
        $_SESSION[SESSION]['button'],
        $sql->countByQuery("`type_of`=$id AND `name`='$slip_identity'")
    );

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = "`type_of`=$id AND `name`='$slip_identity' ORDER BY reg_on DESC LIMIT " . ($offset) . " , " . $pag->getPerPageRec();
    $slip = $sql->fetchByQueryAll($query, [
        'id',
        'patient_id',
        'user_id',
        'discount',
        'get_amt',
        'remain_amt',
        'reg_on'
    ]);

    if (!empty($slip)) {
        for ( $i=0; $i<sizeof($slip); $i++ ) {

            $sql->table(Patient::SQL_TABLE);
            $patient = $sql->fetchByQuery(' `id`=' . $slip[$i]['patient_id'], ['name'] );
            if( !empty($patient) ) {
                $slip[$i]['patient_id'] = [
                    $patient['name'],
                    $slip[$i]['patient_id']
                ];
            }

            $sql->table(User::SQL_TABLE);
            $patient = $sql->fetchByQuery(' `id`=' . $slip[$i]['user_id'], ['name'] );
            if( !empty($patient) ) {
                $slip[$i]['user_id'] = [
                    $patient['name'],
                    $slip[$i]['user_id']
                ];
            }
        }
        echo json_encode([
            HEADERS => ['ID','Patient Name', 'Issu by', 'Discount', 'Recive', 'Remaining', 'date'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $slip
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Not fetching Product with limit',
            STATUS => false
        ]);
    }
}