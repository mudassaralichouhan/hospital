<?php

class Patient extends Person_Abstruction implements sql_io_interface
{
    public const SQL_TABLE = "patient";
    /*
     * SQL database table columns names
     */
    private const CONSULTANT_COL = 'consultant';
    private const DISCOUNT_COL = 'discount';
    private const FEE_COL = 'fee';
    private const PAID_COL = 'paid';

    /*
     * Trait
     */
    use CRUD_trait;

    private int $fee = 0, $paid, $discount = 0;
    private $user_id = null, $consultant_id = null;

    /**
     * Patient constructor.
     * @param int $id
     */
    public function __construct(int $id = 0)
    {
        parent::__construct();
        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $patient_info = $sql->retrive('id', $this->getId());
            $this->setAll($patient_info);
        }
    }


// paitent consultant id
    public function setConsultant(int $id): bool
    {
        if ($id >= 0) {
            $this->consultant_id = $id;

            $sql = new SQL(Consultant::SQL_TABLE);
            $consultant_info = $sql->retrive('id', $this->getConsultant(), ['fee']);
            if (empty($consultant_info)) {
                return '';
            }
            $this->setFee($consultant_info['fee']);

            return true;
        }
        return false;
    }

    public function getConsultant(): int
    {
        return $this->consultant_id;
    }

    public function getConsultantName(): String
    {
        $sql = new SQL(Consultant::SQL_TABLE);
        $consultant_info = $sql->retrive('id', $this->getConsultant(), ['name']);
        if (empty($consultant_info)) {
            return '';
        }
        return $consultant_info['name'];
    }

// amount paid from patient
    public function getPaid(): int
    {
        return $this->paid;
    }

    protected function setPaid(int $paid)
    {
        if ($paid >= 0) {
            $this->paid = $paid;
            return true;
        }
        return false;
    }

// Patient Fee
    public function getFee(): int
    {
        return $this->fee;
    }

    protected function setFee(int $fee): bool
    {
        if ($fee >= 0) {
            $this->fee = $fee;
            return true;
        }
        return false;
    }

// discount on this patient Discount
    public function getDiscount(): int
    {
        return $this->discount;
    }

    public function setDiscount(int $dis): bool
    {
        if ($dis >= 0 and $dis <= 100 and $this->getConsultant()) {

            $this->discount = $dis;
            $this->setPaid(($this->getFee() / 100) * (100 - $this->getDiscount()));

            return true;
        }
        return false;
    }


    /*
     * -------------------------
     * SQL Management
     * -------------------------
     */
    public function getAll(): array
    {
        return [
            parent::NAME_COL => $this->getName(),
            parent::AGE_COL => $this->getAge(),
            parent::GENDER_COL => $this->getGender(),
            parent::CONTACT_COL => $this->getContact(),
            parent::ADDRESS_COL => $this->getLocation(),
            self::CONSULTANT_COL => $this->getConsultant(),

            self::FEE_COL => $this->getFee(),
            self::PAID_COL => $this->getPaid(),
            parent::USER_COL => $this->getUserid(),
            self::DISCOUNT_COL => $this->getDiscount()
        ];
    }

    public function setAll(array $sql_data = []): bool
    {
        if (empty($sql_data))
            return false;

        $this->setId($sql_data[parent::ID_COL]);
        $this->setName($sql_data[parent::NAME_COL]);
        $this->setAge($sql_data[parent::AGE_COL]);
        $this->setGender($sql_data[parent::GENDER_COL]);
        $this->setLocation($sql_data[parent::ADDRESS_COL]);
        $this->setContact($sql_data[parent::CONTACT_COL]);

        $this->setConsultant($sql_data[self::CONSULTANT_COL]);
        $this->setFee($sql_data[self::FEE_COL]);
        $this->setPaid($sql_data[self::PAID_COL]);
        $this->setUserid($sql_data[parent::USER_COL]);
        $this->setDiscount($sql_data[self::DISCOUNT_COL]);

        $this->setRegister($sql_data[parent::REGISTER_COL]);
        $this->setUpdate($sql_data[parent::UPDATE_COL]);
        $this->setIsDisable($sql_data[parent::DELETE_COL]);

        return true;
    }


}

?>