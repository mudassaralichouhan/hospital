<?php

switch ($_GET[ACTION]) {
    case INSERT:
        patient_insert($_GET[R_ARRAY]);
        exit;
    case DELETE:
        patient_delete($_GET[R_ARRAY]['id']);
        exit;
    case UPDATE:
        patient_update($_GET[R_ARRAY]);
        exit;
    case FETCH:
        patient_fetch($_GET[R_ARRAY]['id']);
        exit;
    case LIMIT:
        $data = $_GET[R_ARRAY];
        patient_with_limit($data['page']);
        exit;
    case 'patient_limit_consultant':
        $data = $_GET[R_ARRAY];
        patient_limit_consultant($data['page'],$data['id']);
        exit;
    case 'patient_limit_user':
        $data = $_GET[R_ARRAY];
        patient_limit_user($data['page'],$data['id']);
        exit;
    case 'patient_slip':
        $data = $_GET[R_ARRAY];
        patient_slip($data['id']);
        exit;
    case 'today':
        patient_today();
        exit;
}

function patient_insert(array $data)
{
    $patient = new Patient();

    $patient->setName($data['patient_name']);
    $patient->setGender($data['patient_gender']);
    $patient->setContact($data['patient_contact']);
    $patient->setLocation($data['patient_address']);
    $patient->setAge($data['patient_age']);
    $patient->setConsultant($data['patient_consultant_id']);
    $patient->setDiscount($data['patient_discount']);

    if ($patient->insert() === true) {
        echo json_encode([
            MESSAGE => 'Insert Successfull=',
            STATUS => true
        ]);
    }

}

function patient_update(array $patient_info)
{
    $patient = new Patient($patient_info['id']);
    $patient->setName($patient_info['patient_name']);
    $patient->setGender($patient_info['patient_gender']);
    $patient->setAge($patient_info['patient_age']);
    $patient->setContact($patient_info['patient_contact']);
    $patient->setConsultant($patient_info['patient_consultant_id']);
    $patient->setDiscount((int)$patient_info['patient_discount']);
    $patient->setLocation($patient_info['patient_address']);

    if ($patient->update()) {
        echo json_encode([
            MESSAGE => 'Update Successfull=',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Update un-Successfull=',
            STATUS => false
        ]);
    }
}

function patient_delete(int $id)
{
    if ((new Patient())->delete($id) === true) {
        echo json_encode([
            MESSAGE => 'Deleted Successfull',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Not Delete something was wrong',
            STATUS => false
        ]);
    }
}

function patient_fetch(int $id)
{
    $sql = new SQL(Patient::SQL_TABLE);
    $fetch_patient = $sql->retrive('id', $id, [
        'id',
        'name',
        'gender',
        'age',
        'contact',
        'location',
        'consultant',
        'fee',
        'paid',
        'user_id',
        'discount',
        'reg_on',
        'update_on'
    ]);

    if (!empty($fetch_patient)) {

        $sql->table(User::SQL_TABLE);
        $user_id = $fetch_patient['user_id'];
        $user_id = $sql->retrive('id', $user_id, ['name']);
        if (!empty($user_id)) {
            $fetch_patient['user_id'] = $user_id['name'];
        } else {
            $fetch_patient['user_id'] = 'User not found';
        }

        echo json_encode($fetch_patient);

    } else {

        echo json_encode([
            MESSAGE => 'Patient not found',
            'status' => false
        ]);

    }
}

function patient_with_limit(int $page)
{
    $sql = new SQL(Patient::SQL_TABLE);
    $pag = new Pagination($page, $_SESSION[SESSION]['explode'], $_SESSION[SESSION]['button'], $sql->count());

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = "true ORDER BY reg_on DESC LIMIT " . ($offset) . " , " . $pag->getPerPageRec();
    $patient = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'gender',
        'age',
        'consultant',
        'paid',
        'discount',
        'reg_on'
    ]);

    if (!empty($patient)) {

        $sql->table(Consultant::SQL_TABLE);

        for ($i = 0; $i < sizeof($patient); $i++) {
            $consultant_id = $patient[$i]['consultant'];
            $consultant = $sql->retrive('id', $consultant_id, ['name']);
            if (!empty($consultant)) {
                $patient[$i]['consultant'] = [$consultant_id,$consultant['name']];
            } else {
                $patient[$i]['consultant'] = [$consultant_id,'Consultant not found'];
            }

            $patient[$i]['paid'] .= 'rs';
            $patient[$i]['discount'] .= '%';
        }

        echo json_encode([
            HEADERS => ['Name', 'Gender', 'Age', 'Doctor', 'Paid amt', 'Disc', 'Register'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $patient
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            'status' => false
        ]);
    }
}

function patient_limit_consultant(int $page, $consultant_id)
{
    $sql = new SQL(Patient::SQL_TABLE);
    $pag = new Pagination (
        $page,
        $_SESSION[SESSION]['explode'],
        $_SESSION[SESSION]['button'],
        $sql->countByQuery("consultant=$consultant_id")
    );

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = " consultant=$consultant_id ORDER BY reg_on DESC LIMIT " . ($offset) . " , " . $pag->getPerPageRec();
    $patient = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'gender',
        'age',
        'paid',
        'discount',
        'reg_on'
    ]);

    if (!empty($patient)) {

        echo json_encode([
            HEADERS => ['Name', 'Gender', 'Age', 'Paid amt', 'Disc', 'Register'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $patient
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit of paticular conutlant',
            'status' => false
        ]);
    }
}

function patient_limit_user(int $page, $user_id)
{
    $sql = new SQL(Patient::SQL_TABLE);
    $pag = new Pagination (
        $page,
        $_SESSION[SESSION]['explode'],
        $_SESSION[SESSION]['button'],
        $sql->countByQuery("user_id=$user_id")
    );

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = " user_id=$user_id ORDER BY reg_on DESC LIMIT " . ($offset) . " , " . $pag->getPerPageRec();
    $patient = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'gender',
        'age',
        'consultant',
        'paid',
        'discount',
        'reg_on'
    ]);

    if (!empty($patient)) {

        $sql->table(Consultant::SQL_TABLE);

        for ($i = 0; $i < sizeof($patient); $i++) {
            $consultant_id = $patient[$i]['consultant'];
            $consultant = $sql->retrive('id', $consultant_id, ['name']);
            if (!empty($consultant)) {
                $patient[$i]['consultant'] = [$consultant_id,$consultant['name']];
            } else {
                $patient[$i]['consultant'] = [$consultant_id,'Consultant not found'];
            }

            $patient[$i]['paid'] .= 'rs';
            $patient[$i]['discount'] .= '%';
        }

        echo json_encode([
            HEADERS => ['Name', 'Gender', 'Age','Doctor' , 'Paid amt', 'Disc', 'Register'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $patient
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit of paticular conutlant',
            'status' => false
        ]);
    }
}

function patient_slip(int $id)
{
    $sql = new SQL(SurgicalSlip::SQL_TABLE);

    $slip = $sql->fetchByQueryAll(" `patient_id`='$id' ", [
        'id',
        'name',
        'type_of',
        'price',
        'discount',
        'get_amt',
        'remain_amt',
        'reg_on'
    ]);

    if (!empty($slip)) {

        for ($i = 0; $i < sizeof($slip); $i++) {

            if($slip[$i]['name'] === DentalSlip::IDENTITY) {

                $sql->table(DentalProduct::SQL_TABLE);
                $product_name = $sql->fetchByQuery('`id`=' . $slip[$i]['type_of'], ['name','id']);
                if (!empty($product_name)) {
                    $slip[$i]['type_of'] = [$product_name['name'], $product_name['id']];
                } else {
                    $slip[$i]['type_of'] = ['Product not found',''];
                }

            } elseif ($slip[$i]['name'] === LaboratorySlip::IDENTITY) {

                $sql->table(LaboratoryProduct::SQL_TABLE);
                $product_name = $sql->fetchByQuery('`id`=' . $slip[$i]['type_of'], ['name','id']);
                if (!empty($product_name)) {
                    $slip[$i]['type_of'] = [$product_name['name'], $product_name['id']];
                } else {
                    $slip[$i]['type_of'] = ['Product not found',''];
                }

            } elseif ($slip[$i]['name'] === XraySlip::IDENTITY) {

                $sql->table(XrayProduct::SQL_TABLE);
                $product_name = $sql->fetchByQuery('`id`=' . $slip[$i]['type_of'], ['name','id']);
                if (!empty($product_name)) {
                    $slip[$i]['type_of'] = [$product_name['name'], $product_name['id']];
                } else {
                    $slip[$i]['type_of'] = ['Product not found',''];
                }

            } elseif ($slip[$i]['name'] === SurgicalSlip::IDENTITY) {

                $sql->table(SurgicalProduct::SQL_TABLE);
                $product_name = $sql->fetchByQuery('`id`=' . $slip[$i]['type_of'], ['name','id']);
                if (!empty($product_name)) {
                    $slip[$i]['type_of'] = [$product_name['name'], $product_name['id']];
                } else {
                    $slip[$i]['type_of'] = ['Product not found',''];
                }
            }

        }

        echo json_encode([
            HEADERS => ['Name', 'Procedure', 'Price', 'Discount', 'Recive Rs', 'Remaining Rs', 'date'],
            DATA => $slip
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'On this patient any procedure not found',
            'status' => false
        ]);
    }
}

function patient_today()
{
    $sql = new SQL(Patient::SQL_TABLE);
    $today_date = '2020-11-11 00:00:00';
//    $today_date = date('Y-m-d').' 00:00:00';
    $fetch_patient = $sql->fetchByQueryAll("reg_on >='$today_date' LIMIT 12", [
        'name',
        'gender',
        'location',
    ]);

    if (!empty($fetch_patient)) {

        echo json_encode($fetch_patient);

    } else {

        echo json_encode([
            MESSAGE => 'Patient not found',
            'status' => false
        ]);

    }
}
