<?php

switch ($_GET[ACTION]) {

    case INSERT:
        slip_patient_insert($slip_obj, $_GET[R_ARRAY]);
        exit;

    case DELETE:
        slip_patient_delete($slip_obj, $_GET[R_ARRAY]['id']);
        exit;

    case UPDATE:
        $user_info = $_GET[R_ARRAY];
        slip_patient_update($slip_obj, $user_info);
        exit;

    case FETCH:
        slip_patient_fetch($slip_sql_table, $_GET[R_ARRAY]['id']);
        exit;

    case LIMIT:
        $data = $_GET[R_ARRAY];
        slip_with_limit($product_table, $slip_identity, $slip_sql_table, $data['page']);
        exit;

    case 'due':
        slip_dues();
        exit;

    case 'due_with_limit':
        $data = $_GET[R_ARRAY];
        due_with_limit($data['page']);
        exit;
}

function slip_patient_insert(object $slip_obj, array $data)
{
    $slip = $slip_obj;

    $slip->setPatientId($data['patient_id']);
    $slip->setProcedureId($data['product_id']);
    $slip->setDiscount($data['discount']);
    $slip->setRemain((int)$data['get_amount']);

    if ($slip->insert()) {
        echo json_encode([
            MESSAGE => 'Insert Successfull=',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Insert un-Successfull=',
            STATUS => false
        ]);
    }

}

function slip_patient_update(object $slip_obj, array $data)
{
    $slip = $slip_obj;
    $slip_obj->__construct($data['id']);

    $slip->setProcedureId($data['product_id']);
    $slip->setDiscount($data['discount']);
    $slip->setRemain($data['get_amount']);

    if ($slip->update()) {
        echo json_encode([
            MESSAGE => 'Update Successfull=',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Update un-Successfull=',
            STATUS => false
        ]);
    }
}

function slip_patient_delete(object $slip_obj, int $id)
{
    if ($slip_obj->delete($id)) {
        echo json_encode([
            MESSAGE => 'Deleted Successfull',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Not Delete something was wrong',
            STATUS => false
        ]);
    }
}

function slip_patient_fetch(string $slip_sql_table, int $id)
{
    $sql = new SQL($slip_sql_table);
    $s = $sql->retrive('id', $id, [
        'id',
        'patient_id',
        'user_id',
        'type_of',
        'price',
        'discount',
        'get_amt',
        'remain_amt',
        'reg_on',
        'update_on'
    ]);

    if (!empty($s)) {

        $sql->table(Patient::SQL_TABLE);
        $s['patient_id'] = $sql->fetchByQuery(' `id`=' . $s['patient_id'], ['name'])['name'];

        $sql->table(User::SQL_TABLE);
        $user = $sql->fetchByQuery(' `id`=' . $s['user_id'], ['name']);
        if (!empty($user)) {
            $s['user_id'] = $user['name'];
        }

        echo json_encode($s);

    } else {

        echo json_encode([
            MESSAGE => 'Patient not found',
            'status' => false
        ]);

    }
}

function slip_with_limit(string $product_table, string $slip_identity, string $slip_sql_table, int $page)
{
    $sql = new SQL($slip_sql_table);
    $pag = new Pagination($page,
        $_SESSION[SESSION]['explode'],
        $_SESSION[SESSION]['button'],
        $sql->countByQuery('`name` = \'' . $slip_identity . "'")
    );

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = "`name`='" . $slip_identity . "' ORDER BY reg_on DESC LIMIT " . ($offset) . " , " . $pag->getPerPageRec();
    $slip = $sql->fetchByQueryAll($query, [
        'id',
        'patient_id',
        'type_of',
        'price',
        'discount',
        'get_amt',
        'remain_amt',
        'reg_on'
    ]);

    if (!empty($slip)) {

        for ($i = 0; $i < sizeof($slip); $i++) {
            $sql->table(Patient::SQL_TABLE);
            $slip[$i]['patient_id'] = [
                $sql->fetchByQuery(' `id`=' . $slip[$i]['patient_id'], ['name'])['name'],
                $slip[$i]['patient_id']
            ];

            $sql->table($product_table);
            $product_name = $sql->fetchByQuery('`id`=' . $slip[$i]['type_of'], ['name']);
            if (!empty($product_name)) {
                $slip[$i]['type_of'] = [
                    $product_name['name'],
                    $slip[$i]['type_of']
                ];
            } else {
                $slip[$i]['type_of'] = [
                    'not found',
                    ''
                ];
            }

        }

        echo json_encode([
            HEADERS => ['Patient Name', 'Procedure', 'Price', 'Discount', 'Recive Rs', 'Remaining Rs', 'date'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $slip
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            'status' => false
        ]);
    }
}

function slip_dues()
{
    $sql = new SQL(DentalSlip::SQL_TABLE);
    $query = 'remain_amt!=0 ORDER BY reg_on DESC LIMIT 12';
    $slip = $sql->fetchByQueryAll($query, [
        'patient_id',
        'name',
        'remain_amt',
        'reg_on'
    ]);

    if (!empty($slip)) {

        echo json_encode([
            DATA => $slip,
            'total_slips' => $sql->countByQuery('remain_amt!=0'),
            'notif' => sizeof($slip),
            STATUS => true

        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            STATUS => false
        ]);
    }
}

function due_with_limit($page)
{
    $sql = new SQL(DentalSlip::SQL_TABLE);
    $pag = new Pagination($page,
        $_SESSION[SESSION]['explode'],
        $_SESSION[SESSION]['button'],
        $sql->countByQuery('`remain_amt`!=0')
    );

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = "`remain_amt`!=0 ORDER BY reg_on DESC LIMIT  $offset , " . $pag->getPerPageRec();
    $slip = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'patient_id',
        'remain_amt',
        'reg_on'
    ]);

    if (!empty($slip)) {

        for ($i = 0; $i < sizeof($slip); $i++) {

            $slip[$i]['id'] = $slip[$i]['patient_id'];

            $sql->table(Patient::SQL_TABLE);
            $slip[$i]['patient_id'] = $sql->fetchByQuery(' `id`=' . $slip[$i]['patient_id'], ['name'])['name'];

        }

        echo json_encode([
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $slip,
            'total_slips' => $pag->getTotalRec(),
            'current_slips' => ($pag->getCurrPage() * $pag->getPerPageRec())
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            'status' => false
        ]);
    }
}

