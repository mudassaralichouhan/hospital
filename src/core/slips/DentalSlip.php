<?php

class DentalSlip extends Slip_Abstruction implements sql_io_interface
{
    const SQL_TABLE = "slip";
    const IDENTITY = 'Dental Ship';

//    Member variable
    private $typeId;

    use CRUD_trait;

//    constructor
    public function __construct(int $id = 0)
    {
        parent::__construct(self::IDENTITY);
        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $patient_info = $sql->retrive('id', $this->getId());
            $this->setAll($patient_info);
        }
    }

//  X dental product type id
    public function getProcedureId(): int {
        return $this->typeId;
    }

    public function setProcedureId(int $id) {
        if ($id > 0) {
            $this->typeId = $id;
            $sql = new SQL(DentalProduct::SQL_TABLE);

            $dental = $sql->retrive('id', $this->typeId, ['price']);
            if (!empty($dental)) {
                $this->setFee($dental['price']);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}