<?php


class LaboratorySlip extends Slip_Abstruction implements sql_io_interface
{
    const SQL_TABLE = "slip";
    const IDENTITY = 'Lab Slip';

//    Member variable
    private $typeId;

    use CRUD_trait;

//    constructor
    public function __construct(int $id = 0)
    {
        parent::__construct(self::IDENTITY);
        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $patient_info = $sql->retrive('id', $this->getId());
            $this->setAll($patient_info);
        }
    }

//  X dental product type id
    public function getProcedureId(): int
    {
        return $this->typeId;
    }

    public function setProcedureId($id)
    {
        if (is_numeric($id)) {

            $this->typeId = $id;
            $sql = new SQL(LaboratoryProduct::SQL_TABLE);

            $xray = $sql->retrive('id', $this->typeId, ['price']);
            $this->setFee($xray['price']);
            return true;

        }

        return false;
    }
}