<?php

switch ($_GET[ACTION]) {
    case 'suggest':
        suggest($_GET[R_ARRAY]['key']);
        exit;
    case 'search':
        $search_info = $_GET[R_ARRAY];
        search($search_info['page'], $search_info['key']);
        exit;
}

function suggest(string $key_word)
{

    $sql = new SQL(Patient::SQL_TABLE);

    if (is_numeric($key_word) and strlen($key_word) >= 3 ) {
        $query = " contact LIKE '%$key_word%' ORDER BY reg_on DESC";
    } else if ($key_word === 'male' or $key_word === 'female') {
        $query = " gender='$key_word' ORDER BY reg_on DESC";
    } else {
        $query = " name LIKE '%$key_word%' ORDER BY reg_on DESC";
    }

    $patient_data = $sql->fetchByQueryAll(
        $query." LIMIT 7",
        ['name', 'id']
    );

    echo json_encode($patient_data);
}

function search(int $page, string $key_word)
{
    if (is_numeric($key_word) and strlen($key_word) >= 3 ) {
        $query = " contact LIKE '%$key_word%' ORDER BY reg_on DESC";
    } else if ($key_word === 'male' or $key_word === 'female') {
        $query = " gender='$key_word' ORDER BY reg_on DESC";
    } else {
        $query = " name LIKE '%$key_word%' ORDER BY reg_on DESC";
    }

    $sql = new SQL(Patient::SQL_TABLE);
    $total = $sql->countByQuery($query);
    $pag = new Pagination (
        $page,
        $_SESSION[SESSION]['explode'],
        $_SESSION[SESSION]['button'],
        $total
    );

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = $query." LIMIT $offset, ".$pag->getPerPageRec();
    $patient = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'gender',
        'contact',
        'location',
    ]);

    if (!empty($patient)) {

        echo json_encode([
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $patient,
            'offset' => $pag->getCurrPage() * $pag->getPerPageRec(),
            'total' => $total
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            'status' => false
        ]);
    }
}