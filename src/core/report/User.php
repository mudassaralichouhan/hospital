<?php


namespace report;


class User extends Report_Abstruction implements report_interface
{

//    constructor
    const SQL_TABLE = 'user';

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        parent::__construct();
        $this->setId($id);
    }

    public function getLabProducts()
    {
        $sql = new \SQL(\LaboratoryProduct::SQL_TABLE);
        return $sql->retrive('user_id', $this->getId());
    }

    public function getXrayProducts()
    {
        $sql = new \SQL(\XrayProduct::SQL_TABLE);
        return $sql->retrive('user_id', $this->getId());
    }

    public function getDentalProducts()
    {
        $sql = new \SQL(\DentalProduct::SQL_TABLE);
        return $sql->retrive('user_id', $this->getId());
    }

    public function getSurgicalProducts()
    {
        $sql = new \SQL(\SurgicalProduct::SQL_TABLE);
        return $sql->retrive('user_id', $this->getId());
    }


    public function getPatient(): array
    {
        $sql = new \SQL(\Patient::SQL_TABLE);
        return $sql->fetchByQueryAll("user_id='" . $this->getId() . "' AND " . $this->getDateSqlFormate());
    }

    public function getLab(): array
    {
        $sql = new \SQL(\LaboratorySlip::SQL_TABLE);
        $identity = \LaboratorySlip::IDENTITY;
        return $sql->fetchByQueryAll("name='$identity' and user_id='" . $this->getId() . "' AND " . $this->getDateSqlFormate());
    }

    public function getXray(): array
    {
        $sql = new \SQL(\XraySlip::SQL_TABLE);
        $identity = \XraySlip::IDENTITY;
        return $sql->fetchByQueryAll("name='$identity' and user_id='" . $this->getId() . "' AND " . $this->getDateSqlFormate());
    }

    public function getDental(): array
    {
        $sql = new \SQL(\DentalSlip::SQL_TABLE);
        $identity = \DentalSlip::IDENTITY;
        return $sql->fetchByQueryAll("name='$identity' and user_id='" . $this->getId() . "' AND " . $this->getDateSqlFormate());
    }

    public function getSurgical(): array
    {
        $sql = new \SQL(\SurgicalSlip::SQL_TABLE);
        $identity = \SurgicalSlip::IDENTITY;
        return $sql->fetchByQueryAll("name='$identity' and user_id='" . $this->getId() . "' AND " . $this->getDateSqlFormate());
    }

    public function getInfo()
    {
        $sql = new \SQL(\User::SQL_TABLE);
        return $sql->retrive("id", $this->getId(), [
            'id',
            'reg_on',
            'update_on'
        ]);
    }

}