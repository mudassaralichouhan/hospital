<?php


namespace report;
use SQL;

class Session extends \Identity_Abstraction implements \sql_io_interface
{
    /**
     * Trait
     */
    use \CRUD_trait;

    /**
     * SQL table name
     */
    const SQL_TABLE = ' remote_log';

    /**
     * SQL table colunm names
     */
    private const STATUS_COL = 'status';
    private const AGENT_COL = 'agent';
    private const METHOD_COL = 'method';

    private const USER_ADDR_COL = 'client_addr';
    private const USER_PORT_COL = 'client_port';

    private const CITY_COL = 'city';
    private const REGION_COL = 'region';
    private const CODE_REGION_COL = 'region_code';
    private const CODE_CONTINENT_COL = 'continent_code';
    private const CODE_COUNTRY_COL = 'country_code';
    private const CONTINENT_COL = 'continent_name';
    private const COUNTRY_COL = 'country_name';
    private const NAME_REGION_COL = 'region_name';
    private const LATITUDE_COL = 'latitude';
    private const LONGITUDE_COL = 'longitude';
    private const TIME_ZONE_COL = 'time_zone';

    /*
     * Member variable
     */
    private int $status = 0;
    private $client_agent, $request_method, $client_port, $client_ip;
    private $city, $country, $continent, $country_code, $continent_code, $time_zone;
    private $region, $region_name, $region_code;
    private $latitude, $longitude;

    /*
     *
     */
    public function __construct(int $id, array $r_server = []) {
        if($id > 0){
            $this->setUserid($id);
            $this->status = 0;
            if(!empty($r_server)) {
                $this->client_ip = $r_server['REMOTE_ADDR'];
                $this->client_port = $r_server['REMOTE_PORT'];
                $this->request_method = $r_server['REQUEST_METHOD'];
                $this->client_agent = $r_server['HTTP_USER_AGENT'];
                $this->setGeo();
                $this->insert();
            }
        }
    }

    public static function getLog(int $user_id): array
    {
        $sql = new SQL(self::SQL_TABLE);
        $query = " `user_id`='$user_id' ORDER BY ".self::REGISTER_COL.' LIMIT 7';
        $log = $sql->fetchByQueryAll($query, [
            parent::ID_COL,
            self::CITY_COL,
            self::COUNTRY_COL,
            self::CONTINENT_COL,
            self::AGENT_COL,
            self::REGISTER_COL
        ]);

        for($i=0; $i<sizeof($log); $i++){
            $log[$i]['browser'] = self::getBrowser($log[$i][self::AGENT_COL]);
            $log[$i]['os'] = self::getOS($log[$i][self::AGENT_COL]);
            unset($log[$i][self::AGENT_COL]);
        }

        return $log;
    }

    public static function lastSession(int $user_id)
    {
        $sql = new SQL(self::SQL_TABLE);
        $query = " `user_id`='$user_id' ORDER BY ".self::REGISTER_COL;
        $log = $sql->fetchByQuery($query, [
            self::CODE_COUNTRY_COL,
            self::TIME_ZONE_COL
        ]);

        return $log;
    }

    function getAll(): array
    {
        return [
            parent::ID_COL => $this->getId(),
            parent::USER_COL => $this->getUserid(),
            self::STATUS_COL => $this->status,

            self::AGENT_COL => $this->client_agent,
            self::METHOD_COL =>$this->request_method,
            self::USER_ADDR_COL => $this->client_ip,
            self::USER_PORT_COL => $this->client_port,

            self::CITY_COL => $this->city,
            self::COUNTRY_COL => $this->country,
            self::CONTINENT_COL => $this->continent,
            self::CODE_COUNTRY_COL => $this->country_code,
            self::CODE_CONTINENT_COL => $this->continent_code,
            self::TIME_ZONE_COL => $this->time_zone,

            self::REGION_COL => $this->region,
            self::NAME_REGION_COL => $this->region_name,
            self::CODE_REGION_COL => $this->region_code,

            self::LATITUDE_COL => $this->latitude,
            self::LONGITUDE_COL => $this->longitude
        ];
    }

    function setAll(array $sql_data): bool
    {

        if (empty($sql_data))
            return false;

        $this->setUserid($sql_data[parent::USER_COL]);

        $this->status = $sql_data[self::STATUS_COL];

        $this->client_agent = $sql_data[self::AGENT_COL];
        $this->request_method = $sql_data[self::METHOD_COL];
        $this->client_ip = $sql_data[self::USER_ADDR_COL];
        $this->client_port = $sql_data[self::USER_PORT_COL];

        $this->city = $sql_data[self::CITY_COL];
        $this->country = $sql_data[self::COUNTRY_COL];
        $this->continent = $sql_data[self::CONTINENT_COL];
        $this->country_code = $sql_data[self::CODE_COUNTRY_COL];
        $this->continent_code = $sql_data[self::CODE_CONTINENT_COL];
        $this->time_zone = $sql_data[self::TIME_ZONE_COL];

        $this->region = $sql_data[self::REGION_COL];
        $this->region_name = $sql_data[self::NAME_REGION_COL];
        $this->region_code = $sql_data[self::CODE_REGION_COL];

        $this->latitude = $sql_data[self::LATITUDE_COL];
        $this->longitude = $sql_data[self::LONGITUDE_COL];


        $this->setRegister($sql_data[parent::REGISTER_COL]);
        $this->setUpdate($sql_data[parent::UPDATE_COL]);
        $this->setIsDisable($sql_data[parent::DELETE_COL]);

        return true;
    }

    private function setGeo()
    {
        $ip = explode('.',$this->client_ip);
        if ( $ip[0]!=127 and $ip[0]!=192 and $ip[0]!=168 ) {

            $geo_info = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$this->client_ip));
            if($geo_info['geoplugin_status'] == 200) {

                $this->status = $geo_info['geoplugin_status'];

                $this->city = $geo_info['geoplugin_city'];
                $this->country = $geo_info['geoplugin_countryName'];
                $this->continent = $geo_info['geoplugin_continentName'];
                $this->country_code = $geo_info['geoplugin_countryCode'];
                $this->continent_code = $geo_info['geoplugin_continentCode'];
                $this->time_zone = $geo_info['geoplugin_timezone'];

                $this->region = $geo_info['geoplugin_region'];
                $this->region_name = $geo_info['geoplugin_regionName'];
                $this->region_code = $geo_info['geoplugin_regionCode'];

                $this->latitude = $geo_info['geoplugin_latitude'];
                $this->longitude = $geo_info['geoplugin_longitude'];

            }
        }
    }

    public function status(): int {
        return $this->status;
    }

    public static function getBrowser($agent) {

        $browser        =   "Unknown Browser";

        $browser_array  =   array(
            '/msie/i'       =>  'Internet Explorer',
            '/firefox/i'    =>  'Firefox',
            '/Mozilla/i'	=>	'Mozila',
            //'/Mozilla/5.0/i'=>	'Mozila',
            '/safari/i'     =>  'Safari',
            '/chrome/i'     =>  'Chrome',
            '/edge/i'       =>  'Edge',
            '/opera/i'      =>  'Opera',
            '/OPR/i'        =>  'Opera',
            '/netscape/i'   =>  'Netscape',
            '/maxthon/i'    =>  'Maxthon',
            '/konqueror/i'  =>  'Konqueror',
            '/Bot/i'		=>	'BOT Browser',
            '/Valve Steam GameOverlay/i'  =>  'Steam',
            '/mobile/i'     =>  'Handheld Browser'
        );

        foreach ($browser_array as $regex => $value) {

            if (preg_match($regex, $agent)) {
                $browser    =   $value;
            }

        }

        return $browser;

    }

    public static function getOS($agent) {

        $os_platform    =   "Unknown OS Platform";

        $os_array       =   array(
            '/windows nt 10/i'     =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/kalilinux/i'          =>  'KaliLinux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile',
            '/Windows Phone/i'      =>  'Windows Phone'
        );

        foreach ($os_array as $regex => $value) {

            if (preg_match($regex, $agent)) {
                $os_platform    =   $value;
            }

        }
        return $os_platform;

    }
}