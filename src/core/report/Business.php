<?php


namespace report;


class Business extends Report_Abstruction
{

    public function getLab(): array
    {
        $sql = new \SQL(\LaboratorySlip::SQL_TABLE);
        $identity = \LaboratorySlip::IDENTITY;
        return $sql->fetchByQueryAll(" `name`='$identity' AND `remain_amt`='0' AND " . $this->getDateSqlFormate(),
            ['get_amt', 'discount', 'remain_amt', 'price']
        );
    }

    public function getXray(): array
    {
        $sql = new \SQL(\XraySlip::SQL_TABLE);
        $identity = \XraySlip::IDENTITY;
        return $sql->fetchByQueryAll(" `name`='$identity' AND `remain_amt`='0' AND " . $this->getDateSqlFormate(),
            ['get_amt', 'discount', 'remain_amt', 'price']
        );
    }

    public function getDental(): array
    {
        $sql = new \SQL(\DentalSlip::SQL_TABLE);
        $identity = \DentalSlip::IDENTITY;
        return $sql->fetchByQueryAll(" `name`='$identity' AND `remain_amt`='0' AND " . $this->getDateSqlFormate(),
            ['get_amt', 'discount', 'remain_amt', 'price']
        );
    }

    public function getSurgical(): array
    {
        $sql = new \SQL(\SurgicalSlip::SQL_TABLE);
        $identity = \SurgicalSlip::IDENTITY;
        return $sql->fetchByQueryAll(" `name`='$identity' AND `remain_amt`='0' AND " . $this->getDateSqlFormate(),
            ['get_amt', 'discount', 'remain_amt', 'price']
        );
    }

    public function getPatient(): array
    {
        $sql = new \SQL(\Patient::SQL_TABLE);
        return $sql->fetchByQueryAll($this->getDateSqlFormate(),
            ['fee', 'paid', 'discount',]
        );
    }
}