<?php

namespace report;
class Consultant extends Report_Abstruction
{

    private $perc, $xray_perc, $dental_perc, $lab_perc, $surgical_perc;
//    constructor

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        parent::__construct();
        $this->setId($id);
        $sql = new \SQL(\Consultant::SQL_TABLE);
        $consultant_info = $sql->retrive("id", $this->getId(), [
            'consultant_perc',
            'dental_perc',
            'surgical_perc',
            'lab_perc',
            'xray_perc'
        ]);

        if (!empty($consultant_info)) {
            $this->perc = $consultant_info['consultant_perc'];
            $this->dental_perc = $consultant_info['dental_perc'];
            $this->surgical_perc = $consultant_info['surgical_perc'];
            $this->lab_perc = $consultant_info['lab_perc'];
            $this->xray_perc = $consultant_info['xray_perc'];
        }
    }

    public function getPerc()
    {
        return $this->perc;
    }

    public function getDentalPerc()
    {
        return $this->dental_perc;
    }

    public function getSurgicalPerc()
    {
        return $this->surgical_perc;
    }

    public function getLabPerc()
    {
        return $this->lab_perc;
    }

    public function getXrayPerc()
    {
        return $this->xray_perc;
    }


    public function getLab(): array
    {
        $sql = new \SQL(\LaboratorySlip::SQL_TABLE);
        $identity = \LaboratorySlip::IDENTITY;
        return $sql->fetchByQueryAll(" name='$identity' and consultant_id='" . $this->getId() . "' AND " .
            $this->getDateSqlFormate(), ['get_amt', 'discount', 'remain_amt', 'price']);

    }

    public function getPatient(): array
    {
        $sql = new \SQL(\Patient::SQL_TABLE);
        return $sql->fetchByQueryAll("  consultant='" . $this->getId() . "' AND " .
            $this->getDateSqlFormate(), ['fee', 'paid', 'discount',]);

    }

    public function getXray(): array
    {
        $sql = new \SQL(\XraySlip::SQL_TABLE);
        $identity = \XraySlip::IDENTITY;
        return $sql->fetchByQueryAll(" name='$identity' and consultant_id='" . $this->getId() . "' AND " .
            $this->getDateSqlFormate(), ['get_amt', 'discount', 'remain_amt', 'price']);
    }

    public function getDental(): array
    {
        $sql = new \SQL(\DentalSlip::SQL_TABLE);
        $identity = \DentalSlip::IDENTITY;
        return $sql->fetchByQueryAll("  name='$identity' and consultant_id='" . $this->getId() . "' AND " .
            $this->getDateSqlFormate(), ['get_amt', 'discount', 'remain_amt', 'price']);
    }

    public function getSurgical(): array
    {
        $sql = new \SQL(\SurgicalSlip::SQL_TABLE);
        $identity = \SurgicalSlip::IDENTITY;
        $surgical = $sql->fetchByQueryAll("  name='$identity' and consultant_id='" . $this->getId() . "' AND " .
            $this->getDateSqlFormate(), ['get_amt', 'discount', 'remain_amt', 'price']);

        if (!empty($surgical)) {


            return $surgical;
        }
        return [];
    }

    public function getInfo()
    {
        $sql = new \SQL(\Consultant::SQL_TABLE);
        return $sql->retrive("id", $this->getId(), [
            'id',
            'consultant_perc',
            'dental_perc',
            'surgical_perc',
            'lab_perc',
            'xray_perc',
            'user_id',
            'reg_on',
            'update_on'
        ]);
    }
}