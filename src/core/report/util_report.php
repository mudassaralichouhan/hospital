<?php

$user_id = $_SESSION[SESSION]['id'];

switch ($_GET[ACTION]) {

    case 'get_by_year':
        $dates = $_GET[R_ARRAY];
        get_by_year($dates['from'], $dates['to']);
        exit;

    case 'get_consultant_report':
        $arg = $_GET[R_ARRAY];
        get_consultant_report($arg['from'], $arg['to'], (int)$arg['id']);
        exit;

    case 'get_user_report':
        $arg = $_GET[R_ARRAY];
        get_user_report($arg['from'], $arg['to'], $user_id);
        exit;
}

function get_consultant_report(string $from_date, string $to_date, int $id)
{
    if ($from_date === '' or $to_date === '' or $id === 0) {

        echo json_encode([
            MESSAGE => 'Missing Feilds',
            STATUS => false
        ]);
        return false;

    } else {

        $business = new \report\Consultant($id);

        if (!$business->setFrom($from_date)) {
            echo json_encode([
                MESSAGE => 'Start (FROM) not less then ended (TO)',
                STATUS => false
            ]);
            return false;
        }
        if (!$business->setTo($to_date)) {
            echo json_encode([
                MESSAGE => 'End (TO) not greater then started (FROM)',
                STATUS => false
            ]);
            return false;
        }

        echo json_encode([
            'consultant_info' => $business->getInfo(),
            'patient' => patient_info($business),
            'dental' => slip_info($business->getDental()),
            'surgical' => slip_info($business->getSurgical()),
            'xray' => slip_info($business->getXray()),
            'lab' => slip_info($business->getLab())
        ]);
    }

    return true;
}

function get_by_year(string $from, string $to)
{
    $business = new \report\Business();

    if ($from === '' or $to === '') {
        echo json_encode([
            MESSAGE => 'Missing Feilds',
            STATUS => false
        ]);
        return false;
    } else {

        if (!$business->setFrom($from)) {
            echo json_encode([
                MESSAGE => 'Start (FROM) not less then ended (TO)',
                STATUS => false
            ]);
            return false;
        }
        if (!$business->setTo($to)) {
            echo json_encode([
                MESSAGE => 'End (TO) not greater then started (FROM)',
                STATUS => false
            ]);
            return false;
        }
    }

    echo json_encode([
        'patient' => patient_info($business),
        'dental' => slip_info($business->getDental()),
        'surgical' => slip_info($business->getSurgical()),
        'xray' => slip_info($business->getXray()),
        'lab' => slip_info($business->getLab())
    ]);

    return true;
}

function patient_info(\report\Report_Abstruction $business): array
{
    /*
     * Patient infomation
     */
    $get_patient = $business->getPatient();

    if (!empty($get_patient)) {
        $total = sizeof($get_patient);
        $discount = 0;
        $paid = 0;
        $fee = 0;

        foreach ($get_patient as $patient) {
            $discount += $patient['discount'];
            $paid += $patient['paid'];
            $fee += $patient['fee'];
        }

        $patient_info['discount_avg'] = round($discount / $total, 2);
        $patient_info['profite'] = $paid;
        $patient_info['outof'] = $fee;
        $patient_info['lose'] = $fee - $paid;
        $patient_info['total'] = $total;


        return $patient_info;
    }

    return [];

}

function slip_info(array $get_slip): array
{
    /*
     * Slip infomation
     */
    if (!empty($get_slip)) {
        $total = sizeof($get_slip);
        $discount = 0;
        $paid = 0;
        $fee = 0;

        foreach ($get_slip as $slip) {
            $discount += $slip['discount'];
            $paid += $slip['get_amt'];
            $fee += $slip['price'];
        }

        $slip_info['discount_avg'] = round($discount / $total, 2);
        $slip_info['profite'] = $paid;
        $slip_info['outof'] = $fee;
        $slip_info['lose'] = $fee - $paid;
        $slip_info['total'] = $total;

    } else {
        $slip_info['discount_avg'] = '';
        $slip_info['profite'] = '';
        $slip_info['outof'] = '';
        $slip_info['lose'] = '';
        $slip_info['total'] = '';
    }

    return $slip_info;
}

function get_user_report(string $from_date, string $to_date, int $id)
{
    if ($from_date === '' or $to_date === '' or $id === 0) {

        echo json_encode([
            MESSAGE => 'Missing Feilds',
            STATUS => false
        ]);
        return false;

    } else {

        $business = new \report\User($id);

        if (!$business->setFrom($from_date)) {
            echo json_encode([
                MESSAGE => 'Start (FROM) not less then ended (TO)',
                STATUS => false
            ]);
            return false;
        }
        if (!$business->setTo($to_date)) {
            echo json_encode([
                MESSAGE => 'End (TO) not greater then started (FROM)',
                STATUS => false
            ]);
            return false;
        }

        echo json_encode([
            'consultant_info' => $business->getInfo(),
            'patient' => patient_info($business),
            'dental' => slip_info($business->getDental()),
            'surgical' => slip_info($business->getSurgical()),
            'xray' => slip_info($business->getXray()),
            'lab' => slip_info($business->getLab())
        ]);
    }

    return true;
}