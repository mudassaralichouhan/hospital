<?php

class User extends Person_Abstruction implements sql_io_interface
{
    const SQL_TABLE = "user";

    /*
     * SQL database table columns names
     */
    private const EMAIL_COL = 'email';
    private const RULE_COL = 'rule';
    private const PASSWORD_COL = 'password';
    private const SETTING_COL = 'setting';
    private const IMAGE_COL = 'image';

    use CRUD_trait;

    private string $email = '@email', $password, $rule = 'User';
    private string $setting = ''; // store for json
    private $image = null;

    /**
     * User constructor.
     */
    public function __construct(int $id = 0)
    {
        $this->setUserid(1);

        if ($id > 0) {
            $sql = new SQL(self::SQL_TABLE);
            $this->setId($id);
            $user_info = $sql->retrive('id', $this->getId());
            $this->setAll($user_info);
        }
    }

// user email
    public function getEmail(): string
    {
        if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return $this->email;
        }
        return null;
    }

    public function setEmail(string $email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
            return true;
        }
        return false;
    }

// user rule
    public function getRule(): string
    {
        return $this->rule;
    }

    public function setRule($rule = 'User'): bool
    {
        $this->$rule = 'User';

        if ($rule === 'Admin' or $rule === 'User') {
            $this->rule = $rule;
            return true;
        }

        $this->rule = 'User';
        return false;
    }

// user password
    private function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $pass): bool
    {
        if (strlen($pass) > 5) {
            $this->password = md5($pass);
            return true;
        }
        return false;
    }

// match user password
    public function matchUser(string $email, string $password): bool
    {
        if($email=='@email' or $email=='03000000000') {
            return false;
        }
        $sql = new SQL(self::SQL_TABLE);

        $this->setPassword($password);
        $password = $this->getPassword();

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = $sql->fetchByQuery("`email`='$email' and `password`='$password' ", ['id']);
        } elseif (is_numeric($email)) {
            $user = $sql->fetchByQuery("`contact`='$email' and `password`='$password' ", ['id']);
        }


        if (!empty($user)) {
            $this->setAll($sql->retrive('id', $user['id']));
            date_default_timezone_set('Asia/Karachi');
            if ($sql->updateByQuery($user['id'], 'last_login', date('Y-m-d H:i:s')))
                return true;
        }
        return false;
    }

    public function matchUserPassword(string $password): bool
    {
        if ($this->getPassword() === md5($password))
            return true;
        return false;
    }

//  user setting :) e.g pagination

    public function setSetting(array $setting)
    {
        $button = array_key_exists('button', $setting) ? $setting['button'] : 11;
        $rec_per_page = array_key_exists('rec_per_page', $setting) ? $setting['rec_per_page'] : 50;

        $this->setting = json_encode([
            'button' => $button,
            'rec_per_page' => $rec_per_page
        ]);
    }

    public function getSetting(): array
    {
        return (array)json_decode($this->setting);
    }

    public function setImage($image): bool
    {
        $image_type = $image['type'];
        $image_tmp = $image['tmp_name'];

        $image_base64 = base64_encode(file_get_contents($image_tmp) );
        $this->image = 'data:'.$image_type.';base64,'.$image_base64;

        if($this->image !== null) {
            return true;
        }

        return false;

    }

    public function getImage()
    {
        return $this->image;
    }
//==================================================================
// SQL Management

    public function getAll(): array
    {
        return $data = [
            parent::NAME_COL => $this->getName(),
            parent::AGE_COL => $this->getAge(),
            parent::GENDER_COL => $this->getGender(),
            parent::CONTACT_COL => $this->getContact(),
            parent::ADDRESS_COL => $this->getLocation(),
            self::IMAGE_COL => $this->getImage(),

            self::EMAIL_COL => $this->getEmail(),
            self::PASSWORD_COL => $this->getPassword(),
            self::SETTING_COL => json_encode($this->getSetting())
        ];
    }

    public function setAll(array $sql_data): bool
    {
        if (empty($sql_data))
            return false;

        $this->setId($sql_data[parent::ID_COL]);
        $this->setName($sql_data[parent::NAME_COL]);
        $this->setAge((int)$sql_data[parent::AGE_COL]);
        $this->setGender($sql_data[parent::GENDER_COL]);
        $this->setLocation((string)$sql_data[parent::ADDRESS_COL]);
        $this->setContact($sql_data[parent::CONTACT_COL]);

        $this->password = $sql_data[self::PASSWORD_COL];
        $this->setEmail($sql_data[self::EMAIL_COL]);
        $this->image = $sql_data[self::IMAGE_COL];
        $this->setRule($sql_data[self::RULE_COL]);

        $this->setRegister($sql_data[parent::REGISTER_COL]);
        $this->setUpdate($sql_data[parent::UPDATE_COL]);
        $this->setIsDisable($sql_data[parent::DELETE_COL]);

        $this->setSetting((array)json_decode($sql_data[self::SETTING_COL]));

        return true;
    }
}