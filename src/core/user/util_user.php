<?php

use \report\Session as Session;

if(isset($_SESSION[SESSION])) {
    $user_id = $_SESSION[SESSION]['id'];
}

switch ($_POST[ACTION]) {
    case 'login':
        $user_data = $_POST[R_ARRAY];
        login($user_data['email'], $user_data['password']);
        exit;

    case 'change_password':
        $user_data = $_POST[R_ARRAY];
        change_password($user_id, $user_data['curr_password'], $user_data['password']);
        exit;

    case 'change_email':
        $user_data = $_POST[R_ARRAY];
        change_email($user_id, $user_data['email']);
        exit;

    case 'user_log':
        user_log($user_id);
        exit;

    case 'user_log_delete':
        user_log_delete($user_id, $_POST[R_ARRAY]['id']);
        exit;

    case 'user_setting':
        $user_data = $_POST[R_ARRAY];
        user_setting($user_id, $user_data);
        exit;

    case 'profile_photo_insert':
        profile_photo_upload($user_id, $_FILES['image']);
        exit;

    case 'profile_photo_fetch':
        profile_photo_fetch($user_id);
        exit;

    case 'fetch_setting':
        user_fetch_setting($user_id);
        exit;

    case INSERT:
        user_insert($_POST[R_ARRAY]);
        exit;

    case DELETE:
        $password = $_POST[R_ARRAY]['password'];
        $user = new User($user_id);
        if($user->matchUserPassword($password))
        {
            user_delete($user_id);
            session_destroy();

        } else {
            echo json_encode([
                MESSAGE => 'Password not match',
                STATUS => false
            ]);
        }

        exit;

    case UPDATE:
        user_update($id , $_POST[R_ARRAY]);
        exit;

    case FETCH:
        user_fetch($user_id);
        exit;

    case LIMIT:
        if(is_admin()) {
            $data = $_POST[R_ARRAY];
            user_with_limit($data['page']);
        } else {
            echo json_encode([
                MESSAGE => 'privilage denied',
                STATUS => false
            ]);
        }
        exit;
}

function login(string $email, string $password)
{
    if ($email == '' and $password == '') {
        echo json_encode([
            MESSAGE => ' invalid username or password',
            STATUS => false
        ]);
    } else {
        $user = new User();
        if ($user->matchUser($email, $password)) {

            $user_setting = $user->getSetting();

            new Session($user->getId(), $_SERVER);
			
            $log = Session::lastSession($user->getId());
			if(empty($log)) {
				$log['time_zone'] = '';
				$log['country_code'] = '';
			}

            $_SESSION[SESSION] = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'rule' => $user->getRule(),
                'explode' => $user_setting['rec_per_page'],
                'button' => $user_setting['button'],
                'time_zone' => $log['time_zone'],
                'country_code' => $log['country_code']
            ];

            echo json_encode([
                MESSAGE => 'login successfull',
                STATUS => true
            ]);

        } else {
            echo json_encode([
                MESSAGE => 'in-valide username or password',
                STATUS => false
            ]);
        }
    }
}

function user_insert(array $data)
{
    $sql = new SQL(User::SQL_TABLE);

    $exist_email = empty($sql->retrive('email', $data['email'], ['email']));
    $exist_contact = empty($sql->retrive('contact', $data['email'], ['contact']));

    if ($exist_contact or $exist_email) {
        echo json_encode([
            MESSAGE => ' user already exist ' . $data['email'],
            STATUS => false
        ]);
    } else {

        $user = new User();

        $user->setName($data['name']);
        if ($user->setEmail($data['email'])) {
            $user->setContact('03000000000');
        } else {
            $user->setContact($data['email']);
            $user->setEmail('@email');
        }

        $user->setAge(0);
        $user->setPassword($data['password']);
        $user->setGender($data['gender']);

        if ((new SQL(User::SQL_TABLE))
            ->insert($user)
        ) {
            echo json_encode([
                MESSAGE => 'Register Sucessfully',
                STATUS => true
            ]);
        }

    }
}

function user_update(int $id, array $data)
{
    $user = new User( $id );

    $data['user_contact'] = isset($data['user_contact']) ? $data['user_contact'] : '';

    $user->setName($data['user_name']);
    $user->setGender($data['user_gender']);
    $user->setAge($data['user_age']);
    $user->setLocation($data['user_address']);
    $rule = isset($data['user_rule']) ? $data['user_rule'] : '';
    $user->setRule($rule);
    $user->setContact($data['user_contact']);

    if ($user->update()) {
        echo json_encode([
            MESSAGE => 'Update Successfull=',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Update un-Successfull=',
            STATUS => false
        ]);
    }
}

function user_delete(int $id)
{
    if ((new User())->delete($id)) {
        echo json_encode([
            MESSAGE => 'Deleted Successfull',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Not Delete something was wrong',
            STATUS => false
        ]);
    }
}

function user_fetch(int $id)
{
    $sql = new SQL(User::SQL_TABLE);
    $s = $sql->retrive('id', $id, [
        'id',
        'name',
        'gender',
        'age',
        'location',
        'rule',
        'reg_on',
        'update_on',
        'email',
        'contact',
        'last_login'
    ]);

    if (!empty($s)) {
        echo json_encode($s);
    } else {

        echo json_encode([
            MESSAGE => 'Patient not found',
            'status' => false
        ]);

    }
}

function user_with_limit(int $page)
{
    $sql = new SQL(User::SQL_TABLE);
    $pag = new Pagination($page, $_SESSION[SESSION]['explode'], $_SESSION[SESSION]['button'], $sql->count());

    $offset = ($pag->getCurrPage() * $pag->getPerPageRec()) - $pag->getPerPageRec();

    $query = "true ORDER BY rule,name LIMIT " . ($offset) . " , " . $pag->getPerPageRec();
    $users = $sql->fetchByQueryAll($query, [
        'id',
        'name',
        'gender',
        'contact',
        'email',
        'rule'
    ]);

    if (!empty($users)) {
        echo json_encode([
            HEADERS => ['Name', 'Gender', 'Contact', 'E-mail', 'Rule'],
            PAGINATION => [
                'previous_page' => $pag->getPrevious(),
                'next_page' => $pag->getNext(),
                'buttons' => $pag->get()
            ],
            DATA => $users
        ]);

    } else {
        echo json_encode([
            MESSAGE => 'Not fetching patient with limit',
            'status' => false
        ]);
    }
}

function change_password(int $id, string $curr_password, string $new_password)
{
    $user = new User($id);

    if ($user->matchUserPassword($curr_password)) {

        if ($user->setPassword($new_password) and $user->update()) {
            echo json_encode([
                MESSAGE => 'Password Successfully Changes',
                STATUS => true
            ]);
        } else {
            echo json_encode([
                MESSAGE => 'new password not SET',
                STATUS => false
            ]);
        }
    } else {
        echo json_encode([
            MESSAGE => 'chack your old password',
            STATUS => false
        ]);
    }
}

function change_email(int $id, string $email)
{
    $user = new User($id);

    if ($email !== '' and $user->getEmail() !== $email) {

        $user->setEmail($email);
        if ($user->update()) {
            echo json_encode([
                MESSAGE => 'E-Mail Successfully Changes',
                STATUS => true
            ]);

        } else {
            echo json_encode([
                MESSAGE => 'Some thing was wrong',
                STATUS => false
            ]);
        }

    } else {
        echo json_encode([
            MESSAGE => 'E-Mail exist',
            STATUS => false
        ]);
    }
}

function user_log(int $id)
{
    echo json_encode([
        'headers' => [
            "City",
            "Country",
            "Continent",
            "Register",
            'Browser',
            'OS'
        ],
        'log' => \report\Session::getLog($id)
    ]);
}

function user_log_delete(int $id, int $log_id) {
    if( ( new \report\Session($id) )->delete($log_id))
    {
        echo json_encode([
            MESSAGE => 'Deleted',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Some thing was wrong',
            STATUS => false
        ]);
    }
}

function user_setting(int $id, array $setting)
{
    $user = new User($id);

    $set['button'] = $setting['button'];
    $set['rec_per_page'] = $setting['rec_per_page'];

    $user->setSetting($set);

    if ($user->update()) {
        $_SESSION[SESSION]['explode'] = $set['rec_per_page'];
        $_SESSION[SESSION]['button'] = $set['button'];

        echo json_encode([
            MESSAGE => 'Update Sucessfully',
            STATUS => true
        ]);
    } else {
        echo json_encode([
            MESSAGE => 'Some thing was wrong',
            STATUS => false
        ]);
    }
}

function user_fetch_setting(int $id)
{
    $user = new User($id);

    echo json_encode([
        'setting' => $user->getSetting()
    ]);

}

function profile_photo_upload($id, $image) {
    $image_type = $image['type'];
    $image_size = $image['size'];

    if( in_array($image_type, ["image/jpg","image/jpeg","image/png","image/gif"]) ){

        if ($image_size <= 3*1024*1024) {

            $user = new User($id);
            if( $user->setImage($image) and  $user->update() ) {
                echo json_encode([
                    MESSAGE => 'Upload carefully',
                    STATUS => true
                ]);
            }

        } else {
            echo json_encode([
                MESSAGE => 'Size is over-limit',
                STATUS => false
            ]);
        }
    } else {
        echo json_encode([
            MESSAGE => 'extention in-valid',
            STATUS => false
        ]);
    }
}

function profile_photo_fetch($id) {
    $user = new User($id);
    $image = $user->getImage();

    if($image == null)
    {
        echo json_encode([
            MESSAGE => 'Upload your profile Photo',
            STATUS => false
        ]);

    } else {

        echo json_encode([
            STATUS => true,
            MESSAGE => $image
        ]);

    }
}
