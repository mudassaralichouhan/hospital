<?php

// error_reporting(0);

// interfaces
require_once(__DIR__ . '/interface/identity_interface.php');
require_once(__DIR__ . '/interface/sql_io_interface.php');
require_once(__DIR__ . '/interface/report_interface.php');

// trait
require_once(__DIR__ . '/trait/CRUD_trait.php');

// abstaction
require_once(__DIR__ . '/abstraction/Identity_Abstraction.php');
require_once(__DIR__ . '/abstraction/Person_Abstraction.php');
require_once(__DIR__ . '/abstraction/Slip_Abstruction.php');
require_once(__DIR__ . '/abstraction/Sql_IO_Abstruction.php');
require_once(__DIR__ . '/abstraction/Product_Abstruction.php');
require_once(__DIR__ . '/abstraction/Report_Abstruction.php');

// sql
require_once(__DIR__ . '/database/SQL.php');
require_once(__DIR__ . '/database/Database.php');

// core
require_once(__DIR__ . '/helper/modules.php');

require_once(__DIR__ . '/core/user/User.php');
require_once(__DIR__ . '/core/patient/Patient.php');
require_once(__DIR__ . '/core/consultant/Consultant.php');

require_once(__DIR__ . '/core/report/Consultant.php');
require_once(__DIR__ . '/core/report/User.php');
require_once(__DIR__ . '/core/report/Business.php');
require_once(__DIR__ . '/core/report/Session.php');

require_once(__DIR__ . '/core/products/LaboratoryProduct.php');
require_once(__DIR__ . '/core/products/DentalProduct.php');
require_once(__DIR__ . '/core/products/SurgicalProduct.php');
require_once(__DIR__ . '/core/products/XrayProduct.php');
require_once(__DIR__ . '/core/products/Spacialization.php');

require_once(__DIR__ . '/core/slips/LaboratorySlip.php');
require_once(__DIR__ . '/core/slips/XraySlip.php');
require_once(__DIR__ . '/core/slips/DentalSlip.php');
require_once(__DIR__ . '/core/slips/SurgicalSlip.php');

// etc
require_once(__DIR__ . '/etc/pagination.php');


/*
 * -------------------------------
 * debug porpuse
 * -------------------------------
 */
function dd($data)
{
    echo '<pre>';

    print_r($data);

    echo '</pre>';
}