<?php

/**
 *
 */
class Pagination
{

    private int $total_rec = 0;
    private int $curr_page = 1;    // present page
    private int $last_page;        // last page
    private int $explode_records = 10;
    private int $button;
    private array $buttons = [];

    /*
     * Constructor
     */

    public function __construct(int $current_page, int $explode, int $btn, int $total)
    {
        $this->setTotalRec($total);

        $this->setPerPageRec($explode);
        $this->button = $btn;

        $this->setLastPage($this->getTotalRec(), $this->getPerPageRec());
        $this->setCurrPage($current_page);

    }

    /*
     * Member function
     */


    // split on the base function of pagination

    public function setPerPageRec($rec)
    {
        if ($rec % 5 === 0 and $rec >= 10) {
            $this->explode_records = $rec;
        }
    }

    public function getPerPageRec(): int
    {
        return $this->explode_records;
    }


    //  set Total recorde of pagination

    public function setTotalRec(int $rec)
    {
        if ($rec > 0) {
            $this->total_rec = $rec;
        }
    }

    public function getTotalRec(): int
    {
        return $this->total_rec;
    }


    //  set Current page of pagination

    private function setCurrPage(int $page)
    {
        if ($page > 1 and $page <= $this->getLastPage()) {
            $this->curr_page = $page;
        }
    }

    public function getCurrPage(): int
    {
        return $this->curr_page;
    }


    //  set last page of pagination

    private function setLastPage(int $end, int $explode)
    {
        $this->last_page = ceil($end / $explode);
    }

    private function getLastPage(): int
    {
        return $this->last_page;
    }

    public function get(): array // pagination button return
    {
        // setup buttons
        if ($this->curr_page <= ceil($this->button / 2)) {
            $this->buttons['start'] = 1;
            $this->buttons['end'] = $this->button < $this->last_page ? $this->button : $this->last_page;
        } else if ($this->curr_page >= $this->last_page - floor($this->button / 2)) {
            $this->buttons['start'] = $this->last_page - $this->button + 1;
            $this->buttons['end'] = $this->last_page;
        } else {
            $this->buttons['start'] = $this->curr_page - floor($this->button / 2);
            $this->buttons['end'] = $this->curr_page + floor($this->button / 2);
        }
        // /setup buttons

        $pagi = [];

        $this->curr_page > $this->button ? $pagi['first_index'] = 1 : false;

        for ($i = $this->buttons['start'], $y = 0; $i <= $this->buttons['end']; $i++) {

            if ($i != $this->curr_page) {
                $pagi[$y++ . '-index'] = $i;
                continue;
            }

            $pagi['active'] = $i;
        }

        ($this->last_page - $this->button) > $this->curr_page ? $pagi['last_index'] = $this->last_page : false;

        return $pagi;
    }

    public function getNext()
    {
        if ($this->last_page === $this->curr_page) {
            return '#';
        }
        return $this->curr_page + 1;
    }

    public function getPrevious()
    {
        if ($this->curr_page == 1) {
            return '#';
        }
        return $this->curr_page - 1;
    }
}