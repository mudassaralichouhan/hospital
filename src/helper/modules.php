<?php
function ucString(string $str)
{
    return (ucwords(strtolower(trim($str))));
}

function url_exe(string $get_url): string
{
    $curl = curl_init($get_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);

    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        return '';
    }
    curl_close($curl);

    return json_decode($curl_response);
}

function num_to_readable($number): array
{
    if ($number < 1000) {
        return [
            'number' => $number,
            'symbol' => ''
        ];

    }
    return [
        'number' => round($number / 1000, 2),
        'symbol' => 'k'
    ];
}

function last_months_name($month)
{
    $date = [];

    for ($i = 1; $i < 13; $i++) {
        if ($month == 0) {
            $month = 12;
        }

        array_push($date, date("F", mktime(0, 0, 0, $month, 10)));
        $month--;
    }

    return $date;
}

function last_12_months($month)
{
    if ($month > 12 or $month < 0) {
        $month = 12;
    }

    $date = [];
    $year = date('o');

    for ($i = 1; $i <= 13; $i++) {
        if ($month == 0) {
            $month = 12;
            $year--;
        }

        array_push($date, $year . '-' . $month . '-00');
        $month--;
    }

    return $date;
}

function date_and_time_to_readable($date_time, $mssage = 'nothing to Change'): string
{
    if ($date_time == '') {
        return $mssage;
        exit;
    }

    $date_time = explode(' ', $date_time);
    $date = explode('-', $date_time[0]);
    $time = explode(':', $date_time[1]);

    $curr_year = date('Y');;
    $curr_month = date('m');;
    $curr_day = date('d');

    $curr_hour = date('h');
    $curr_min = date('i');

    /*
     * this sequance is important
     */

    // 2015 < 2020 year
    if ($date[0] < $curr_year) {
        $count = $curr_year - $date[0];
        return $count . ' year ago';
    }

    // 07 < 09 month
    if ($date[1] < $curr_month) {
        $count = $curr_month - $date[1];
        return $count . ' month ago';
    }

    // 19 < 23 day
    if ($date[2] < $curr_day) {
        $count = $curr_day - $date[2];
        return $count . ' day ago';
    }

    // 19 < 10 hour
    if ($time[0] < $curr_hour) {
        $count = $curr_hour - $time[0];
        return $count . ' hour ago';
    }
    //  < 09 mintue
    if ($time[1] < $curr_min) {
        $count = $curr_min - $time[1];
        return $count . ' min ago';
    }

    return 'now';
}

function dd($data)
{
    echo '<pre>';

    print_r($data);

    echo '</pre>';
}

?>