<?php

switch ($_GET[ACTION]) {
    case 'last_12_months':

        echo json_encode([
            'last_month_names' => last_months_name(date('m'))
        ]);
        exit;

    case 'last_year_patient_data':

        $date = last_12_months(date('m') + 1);
        $sql = new SQL(Patient::SQL_TABLE);

        $data = [];

        for ($i = 0; $i <= 11; $i++) {
            array_push(
                $data,
                $sql->countByQuery("`reg_on` >= '" . $date[$i + 1] . "' AND `reg_on` <= '" . $date[$i] . "'")
            );
        }

        echo json_encode([
            'data' => $data
        ]);

        exit;

    case 'last_year_slips_count':

        $sql_slip = new SQL(LaboratorySlip::SQL_TABLE);
        $date = last_12_months(date('m'));

        $dental = [];
        for ($i = 0; $i < 12; $i++) {

            $count = $sql_slip->countByQuery("`name`='" . DentalSlip::IDENTITY . "' AND `reg_on` >= '" .
                $date[$i + 1] . "' AND `reg_on` <= '" . $date[$i] . "'");

            if ($count === null) {
                $count = 0;
            }

            array_push($dental, $count);
        }

        $sugical = [];
        for ($i = 0; $i < 12; $i++) {

            $count = $sql_slip->countByQuery("`name`='" . SurgicalSlip::IDENTITY . "' AND `reg_on` >= '" .
                $date[$i + 1] . "' AND `reg_on` <= '" . $date[$i] . "'");

            if ($count === null) {
                $count = 0;
            }

            array_push($sugical, $count);
        }

        $lab = [];
        for ($i = 0; $i < 12; $i++) {

            $count = $sql_slip->countByQuery("`name`='" . LaboratorySlip::IDENTITY . "' AND `reg_on` >= '" .
                $date[$i + 1] . "' AND `reg_on` <= '" . $date[$i] . "'");

            if ($count === null) {
                $count = 0;
            }

            array_push($lab, $count);
        }

        $xray = [];
        for ($i = 0; $i < 12; $i++) {

            $count = $sql_slip->countByQuery("`name`='" . XraySlip::IDENTITY . "' AND `reg_on` >= '" .
                $date[$i + 1] . "' AND `reg_on` <= '" . $date[$i] . "'");

            if ($count === null) {
                $count = 0;
            }

            array_push($xray, $count);
        }

        echo json_encode([
            'dental' => $dental,
            'sugical' => $sugical,
            'lab' => $lab,
            'xray' => $xray
        ]);

        exit;
}