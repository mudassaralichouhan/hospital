<?php

function is_login(string $url)
{
    if (!isset($_SESSION[SESSION])) {

        $login_page = $url . '/login.php';
        session_destroy();
        header("location: $login_page");
    }
}

function is_admin(): bool {
    if($_SESSION[SESSION]['rule'] === 'Admin')
        return true;
    return false;
}