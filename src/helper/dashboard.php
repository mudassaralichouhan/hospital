<?php

$sql = new SQL(LaboratorySlip::SQL_TABLE);
$total = $sql->count();

// Laboratory test
$sql_query = "`name`='" . LaboratorySlip::IDENTITY . "'";
$count = $sql->countByQuery($sql_query);
$labTest = num_to_readable($count);
$labTest['perc'] = round(($count * 100) / $total, 2);

// Sugical
$sql_query = "`name`='" . SurgicalSlip::IDENTITY . "'";
$count = $sql->countByQuery($sql_query);
$surgical = num_to_readable($count);
$surgical['perc'] = round(($count * 100) / $total, 2);

// Xray
$sql_query = "`name`='" . XraySlip::IDENTITY . "'";
$count = $sql->countByQuery($sql_query);
$xray = num_to_readable($count);
$xray['perc'] = round(($count * 100) / $total, 2);

// Dental
$sql_query = "`name`='" . DentalSlip::IDENTITY . "'";
$count = $sql->countByQuery($sql_query);
$dental = num_to_readable($count);
$dental['perc'] = round(($count * 100) / $total, 2);


$sql->table(Patient::SQL_TABLE);
$patient_info = num_to_readable($sql->count());

$sql->table(Consultant::SQL_TABLE);
$consultant_info = num_to_readable($sql->count());

$sql->table(User::SQL_TABLE);
$users_info = num_to_readable($sql->count());

echo json_encode([
    'consultant_info' => [
        'total' => $consultant_info['number'],
        'symbol' => $consultant_info['symbol']
    ],
    'patient_info' => [
        'total' => $patient_info['number'],
        'symbol' => $patient_info['symbol']
    ],
    'user_info' => [
        'total' => $users_info['number'],
        'symbol' => $users_info['symbol']
    ],
    'dental' => [
        'total' => $dental['number'],
        'symbol' => $dental['symbol'],
        'perc' => $dental['perc']
    ],
    'surgical' => [
        'total' => $surgical['number'],
        'symbol' => $surgical['symbol'],
        'perc' => $surgical['perc']
    ],
    'labtest' => [
        'total' => $labTest['number'],
        'symbol' => $labTest['symbol'],
        'perc' => $labTest['perc']
    ],
    'xray' => [
        'total' => $xray['number'],
        'symbol' => $xray['symbol'],
        'perc' => $xray['perc']
    ]
]);