---
title: Hospital
created: '2021-10-09T14:06:16.621Z'
modified: '2021-10-09T23:44:44.996Z'
---

[Hospital](https://github.com/mudassaralichouhan/hospital)
=

Hospital is a management system utility for dealing with word real genration.

<div>
  <h3>Register Page<h3>
  <img src="screenshots/01-register.png">
  
  <h3>Login Page<h3>
  <img src="screenshots/02-login.png">

  <h3>Setting<h3>
  <img src="screenshots/04-setting.png">
  <img src="screenshots/05-setting.png">

  <h3>Profile and Notification<h3>
  <img src="screenshots/06-profile.png">
  <img src="screenshots/07-notification.png">
  <img src="screenshots/08-notification.png">

  <h3>Search and Search Result<h3>
  <img src="screenshots/09-search.png">
  <img src="screenshots/10-search.png">

  <h3>Chart<h3>
  <img src="screenshots/11-chart.png">

  <h3>Consultant<h3>
  <img src="screenshots/12-consultant.png">

  <h3>Patient<h3>
  <img src="screenshots/13-patient.png">

  <h3>Spacialization and Slips<h3>
  <img src="screenshots/14-slip.png">
  <img src="screenshots/15-slip.png">
  <img src="screenshots/16-spacialization.png">

  <h3>Products<h3>
  <img src="screenshots/17-product.png">
  <img src="screenshots/18-product.png">
  <img src="screenshots/19-product.png">

  <h3>Reporting<h3>
  <img src="screenshots/20-report.png">
  <img src="screenshots/21-report.png">
  <img src="screenshots/22-report.png">
</div>
