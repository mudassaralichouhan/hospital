[Hospital](https://github.com/mudassaralichouhan/hospital)
=

Hospital is a management system utility for dealing with word real genration.

## What is it?
The project is cross-browser and will give you the help to use hospital management such as
`Employers`, `Slip`, `Product`, `Busness Report`, `Search` and `chart`.

## Why should I make it?
Definitely before you show a project to other people or make it public.

It's an easy way to answer questions that your audience will likely have regarding

> bootstrap 4, vanilla javascript
> XHR base project

## Feature
  * <b>Dashboard</b>.
  * <b>Profile:</b> Update e.g. Password, Photo, Address... etc.
  * <b>Admin</b> and <b>User</b> privallage respact.
  * <b>Search:</b> Patients
  * <b>Slips</b> and <b>Products</b> fully customized.
  * <b>Patients, Consultant, User, Dental, Surgical, Labaratory and X-Ray</b> these are also update and delete by <b>Admin</b>
  * <b>Pagination</b> customized
  * Load on <b>Scroll</b>
  * Notification for alert
  * Chart view reporting
  * User <b>Trace</b> by ip location, country and agent.

### Todo
  * Forget Username and Password by email
  * Theming
  * Realtime notification
## Installation
Open file `src/database/Database.php` file then edit.

```php
private const HOST = '';
private const DB_NAME = '';
private const USERNAME = '';
private const PASSWORD = '';
```
Goto `meta/` dir. Import the [sql](https://www.mysql.com/) files to your `database` for create tables. [HELP?](https://stackoverflow.com/questions/17666249/how-do-i-import-an-sql-file-using-the-command-line-in-mysql)
```
root/
|--- meta/
    |--- consultant.sql
    |--- patient.sql
    |--- slip.sql
    |--- user.sql
    |--- remote_log.sql
    |--- doctor_specilization.sql
    |--- product_lab_test.sql
    |--- product_xray_type.sql
    |--- product_dental_procedure.sql
    |--- product_surgical_procedure.sql
```

## [Demo](screenshots.md)
<div>
  <h3>Dashboard<h3>
  <img src="screenshots/03-dashboard.png">
</div>

#### [Please See more overview](screenshots.md)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](LICENSE)
