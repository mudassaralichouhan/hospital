<?php
    session_start();

    if (isset($_SESSION['login'])) {
        header("location: index.php");
    }
    include 'route.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Title -->
  <title>Authantication - Hospital</title>

  <!-- Required Meta Tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Favicon -->
  <link rel="icon" href="" sizes="32x32"/>
  <link rel="icon" href="" sizes="192x192"/>

  <!-- Main CSS with Bootstrap -->
    <link rel="stylesheet" href="./ui/public/css/style.min.css">

  <!-- CSS Vendor -->
    <link rel="stylesheet" href="./ui/public/vendor/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="ui/public/js/jquery-3.2.1.min.js"></script>
    <script> localStorage.clear(); </script>

</head>
<body class="login-page dark">
    <div class="container-fluid no-gutters">
      <div class="row">
        <!-- Login Form -->
        <div class="login-wrapper">
          <!-- Logo -->
          <div class="logo logo-dark px-4 pt-5 pb-2">
            <a href="index.php">
              <div class="text-center text-nowrap">
                <i class="fa fa-spin fa-play-circle mr-0 rounded-circle" aria-hidden="true"></i>
                <h6 class="logo-title text-uppercase mt-3">Hospital</h6>
                <p class="text-muted">
                  <small>ver. 1.0.0</small>
                </p>
              </div>
            </a>
          </div>
          <!-- /Logo Form -->
          <div class="pt-4">

            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
              <li class="nav-item text-center border-0 mb-0 w-50">
                <a class="nav-link border-0 active" id="login-tab" data-toggle="tab" href="#sign-in" role="tab" aria-controls="sign-in" aria-selected="true">
                  <i class="fa fa-pencil-square" aria-hidden="true"></i> Login
                </a>
              </li>
              <li class="nav-item text-center border-0 mb-0 w-50">
                <a class="nav-link border-0" id="register-tab" data-toggle="tab" href="#sign-up" role="tab" aria-controls="sign-up" aria-selected="false">
                  <i class="fa fa-bar-chart" aria-hidden="true"></i> Register
                </a>
              </li>
            </ul>

            <div class="tab-content mt-4" id="myTabContent">

              <div class="tab-pane fade show active" id="sign-in" role="tabpanel" aria-labelledby="login-tab">
                  <div id="login-notifications" class="align-content-center">
                  </div>
                <form onsubmit="return login();">
                  <div class="form-group">
                    <input type="text" class="form-control" id="login-email" aria-describedby="emailHelp" placeholder="Enter email or Mobile no">
                  </div>
                  <div class="form-group">
                    <!--<label for="loginPassword">Password</label>-->
                    <input type="password" class="form-control" id="login-password" placeholder="Password">
                  </div>
                  <button type="submit" class="btn btn-primary btn-lg btn-block">Log in</button>
                </form>

              </div>

              <div class="tab-pane fade" id="sign-up" role="tabpanel" aria-labelledby="register-tab">
                  <div id="register-notifications" class="align-content-center">
                  </div>
                  <form onsubmit="return register();" novalidate>
                      <div class="form-row">

                          <div class="form-group col-md-12">
                              <input type="text" class="form-control" id="register-name" placeholder="Full Name" required>
                              <small id="name-err" class="form-text"></small>
                          </div>

                          <div class="form-group col-md-12">
                              <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                      <input class="form-check-input" type="radio" name="gender" id="register-male" value="Male" required> Male
                                  </label>
                              </div>
                              <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                      <input class="form-check-input" type="radio" name="gender" id="register-female" value="Female" required> Female
                                  </label>
                              </div>
                              <small id="gender-err" class="form-text"></small>
                          </div>

                          <div class="form-group col-md-12">
                              <input type="text" class="form-control" id="register-email" aria-describedby="emailHelp" placeholder="Enter email" required>
                              <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                              <small id="email-err" class="form-text"></small>
                          </div>

                          <div class="form-group col-md-12">
                              <input type="password" class="form-control" id="register-password" placeholder="Password" required>
                          </div>

                          <div class="form-group col-md-12">
                              <input type="password" class="form-control" id="register-password2" placeholder="Repeat Password" required>
                              <small id="password-err" class="form-text"></small>
                          </div>

                      </div>
                      <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                  </form>

              </div>

            </div>

          </div>
        </div>
        <!-- /Login Form -->
      </div>
    </div>

    <script> $baseURL = "<?=$url?>"; </script>;
    <script src="./ui/public/vendor/popper/1.12.9/popper.min.js"></script>;
    <script src="./ui/public/js/bootstrap.min.js"></script>
    <script src="./ui/public/js/script.js"></script>

    <script src="./ui/public/js/custom.js"></script>
    <script src="./ui/public/js/auth.js"></script>
</body>
</html>