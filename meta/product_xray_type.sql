-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 04:32 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_xray_type`
--

CREATE TABLE `product_xray_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `reg_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_disable` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_xray_type`
--

INSERT INTO `product_xray_type` (`id`, `name`, `user_id`, `price`, `reg_on`, `update_on`, `is_disable`) VALUES
(1, 'Digital', 0, 900, '2020-06-02 01:41:41', NULL, 0),
(2, 'Blood Group', 0, 700, '2020-06-07 07:35:49', '2020-07-15 04:59:48', 0),
(110, 'Clavicle Comp', 78, 17000, '2020-07-15 06:34:27', NULL, 0),
(111, 'Agra', 31, 500, '2020-07-15 06:48:39', NULL, 0),
(112, 'Ahmedabad', 5, 2500, '2020-07-15 06:48:39', NULL, 0),
(113, 'Ahmednagar', 40, 1000, '2020-07-15 06:48:39', NULL, 0),
(114, 'Aligarh', 6, 400, '2020-07-15 06:48:39', NULL, 0),
(115, 'Allahabad', 34, 600, '2020-07-15 06:48:39', NULL, 0),
(116, 'Amritsar', 52, 500, '2020-07-15 06:48:39', NULL, 0),
(117, 'Bangalore', 23, 6950, '2020-07-15 06:48:39', NULL, 0),
(118, 'Bareilly', 57, 500, '2020-07-15 06:48:40', NULL, 0),
(119, 'Bhagalpur', 32, 300, '2020-07-15 06:48:40', NULL, 0),
(120, 'Bharuch', 24, 300, '2020-07-15 06:48:40', NULL, 0),
(121, 'Bhilai', 62, 600, '2020-07-15 06:48:40', NULL, 0),
(122, 'Bhopal', 83, 500, '2020-07-15 06:48:40', NULL, 0),
(123, 'Bikaner', 74, 300, '2020-07-15 06:48:40', NULL, 0),
(124, 'Bokaro', 16, 400, '2020-07-15 06:48:40', NULL, 0),
(125, 'Chandigarh', 3, 400, '2020-07-15 06:48:40', NULL, 0),
(126, 'Chennai', 80, 5000, '2020-07-15 06:48:40', NULL, 0),
(127, 'Coimbatore', 33, 3000, '2020-07-15 06:48:40', NULL, 0),
(128, 'Cuttack', 47, 300, '2020-07-15 06:48:40', NULL, 0),
(129, 'Darbhanga', 99, 400, '2020-07-15 06:48:40', NULL, 0),
(130, 'Dhanbad', 64, 600, '2020-07-15 06:48:40', NULL, 0),
(131, 'Ernakulam', 57, 280, '2020-07-15 06:48:40', NULL, 0),
(132, 'Faridabad', 60, 1500, '2020-07-15 06:48:40', NULL, 0),
(133, 'Ghaziabad', 85, 700, '2020-07-15 06:48:40', NULL, 0),
(134, 'Gorakhpur', 49, 750, '2020-07-15 06:48:40', NULL, 0),
(135, 'Guntur', 50, 1200, '2020-07-15 06:48:40', NULL, 0),
(136, 'Gurgaon', 1, 4000, '2020-07-15 06:48:40', NULL, 0),
(137, 'Guwahati', 19, 600, '2020-07-15 06:48:40', NULL, 0),
(138, 'Gwalior', 79, 600, '2020-07-15 06:48:40', NULL, 0),
(139, 'Hooghly', 77, 330, '2020-07-15 06:48:40', NULL, 0),
(140, 'Howrah', 81, 370, '2020-07-15 06:48:40', NULL, 0),
(141, 'Hyderabad', 81, 1850, '2020-07-15 06:48:40', NULL, 0),
(142, 'Indore', 100, 600, '2020-07-15 06:48:40', NULL, 0),
(143, 'Jabalpur', 13, 600, '2020-07-15 06:48:40', NULL, 0),
(144, 'Jaipur', 32, 2000, '2020-07-15 06:48:41', NULL, 0),
(145, 'Jalandhar', 5, 500, '2020-07-15 06:48:41', NULL, 0),
(146, 'Jamnagar', 30, 300, '2020-07-15 06:48:41', NULL, 0),
(147, 'Jamshedpur', 62, 600, '2020-07-15 06:48:41', NULL, 0),
(148, 'Jodhpur', 66, 350, '2020-07-15 06:48:41', NULL, 0),
(149, 'Kanpur', 89, 3000, '2020-07-15 06:48:41', NULL, 0),
(150, 'Kochi', 25, 3300, '2020-07-15 06:48:41', NULL, 0),
(151, 'Kolhapur', 87, 700, '2020-07-15 06:48:41', NULL, 0),
(152, 'Kolkata', 42, 1500, '2020-07-15 06:48:41', NULL, 0),
(153, 'Kota', 99, 300, '2020-07-15 06:48:41', NULL, 0),
(154, 'Lucknow', 8, 550, '2020-07-15 06:48:41', NULL, 0),
(155, 'Ludhiana', 20, 1000, '2020-07-15 06:48:41', NULL, 0),
(156, 'Madurai', 37, 400, '2020-07-15 06:48:41', NULL, 0),
(157, 'Mangalore', 26, 500, '2020-07-15 06:48:41', NULL, 0),
(158, 'Meerut', 87, 600, '2020-07-15 06:48:41', NULL, 0),
(159, 'Moradabad', 60, 500, '2020-07-15 06:48:41', NULL, 0),
(160, 'Mumbai', 90, 5000, '2020-07-15 06:48:41', NULL, 0),
(161, 'Muzaffarpur', 21, 250, '2020-07-15 06:48:41', NULL, 0),
(162, 'Mysore', 60, 600, '2020-07-15 06:48:41', NULL, 0),
(163, 'Nadia', 2, 500, '2020-07-15 06:48:41', NULL, 0),
(164, 'Nagpur', 62, 600, '2020-07-15 06:48:41', NULL, 0),
(165, 'Nalgonda', 24, 350, '2020-07-15 06:48:41', NULL, 0),
(166, 'Nashik', 4, 850, '2020-07-15 06:48:41', NULL, 0),
(167, 'Navimumbai', 58, 6000, '2020-07-15 06:48:41', NULL, 0),
(168, 'Nellore', 92, 600, '2020-07-15 06:48:41', NULL, 0),
(169, 'Delhi', 27, 7000, '2020-07-15 06:48:41', NULL, 0),
(170, 'Noida', 51, 600, '2020-07-15 06:48:41', NULL, 0),
(171, 'Panchkula', 11, 500, '2020-07-15 06:48:41', NULL, 0),
(172, 'Panjim', 96, 700, '2020-07-15 06:48:41', NULL, 0),
(173, 'Patiala', 66, 600, '2020-07-15 06:48:41', NULL, 0),
(174, 'Patna', 35, 2200, '2020-07-15 06:48:41', NULL, 0),
(175, 'Pondicherry', 69, 800, '2020-07-15 06:48:41', NULL, 0),
(176, 'Pune', 32, 6000, '2020-07-15 06:48:41', NULL, 0),
(177, 'Raipur', 57, 600, '2020-07-15 06:48:41', NULL, 0),
(178, 'Rajkot', 85, 1200, '2020-07-15 06:48:42', NULL, 0),
(179, 'Salem', 84, 600, '2020-07-15 06:48:42', NULL, 0),
(180, 'Secunderabad', 25, 600, '2020-07-15 06:48:42', NULL, 0),
(181, 'Siliguri', 28, 350, '2020-07-15 06:48:42', NULL, 0),
(182, 'Sirsa', 10, 300, '2020-07-15 06:48:42', NULL, 0),
(183, 'Solapur', 8, 4000, '2020-07-15 06:48:42', NULL, 0),
(184, 'Srikakulam', 95, 450, '2020-07-15 06:48:42', NULL, 0),
(185, 'Srinagar', 100, 550, '2020-07-15 06:48:42', NULL, 0),
(186, 'Surat', 19, 700, '2020-07-15 06:48:42', NULL, 0),
(187, 'Thane', 75, 800, '2020-07-15 06:48:42', NULL, 0),
(188, 'Thiruvananthapuram', 75, 400, '2020-07-15 06:48:42', NULL, 0),
(189, 'Tiruchirappalli', 98, 350, '2020-07-15 06:48:42', NULL, 0),
(190, 'Tirupati', 46, 300, '2020-07-15 06:48:42', NULL, 0),
(191, 'Udaipur', 11, 500, '2020-07-15 06:48:42', NULL, 0),
(192, 'Vadodara', 74, 3600, '2020-07-15 06:48:42', NULL, 0),
(193, 'Varanasi', 46, 800, '2020-07-15 06:48:42', NULL, 0),
(194, 'Vellore', 36, 480, '2020-07-15 06:48:42', NULL, 0),
(195, 'Vijayawada', 53, 900, '2020-07-15 06:48:42', NULL, 0),
(196, 'Visakhapatnam', 34, 350, '2020-07-15 06:48:42', NULL, 0),
(197, 'Warangal', 66, 2500, '2020-07-15 06:48:43', NULL, 0),
(198, 'New One', 100, 500, '2020-08-01 05:41:55', '2020-08-01 05:43:45', 1),
(199, 'New One One', 100, 500, '2020-08-01 05:42:24', '2020-08-01 05:43:39', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_xray_type`
--
ALTER TABLE `product_xray_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_xray_type`
--
ALTER TABLE `product_xray_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
