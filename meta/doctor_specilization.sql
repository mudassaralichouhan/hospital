-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 04:33 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctor_specilization`
--

CREATE TABLE `doctor_specilization` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reg_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_disable` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_specilization`
--

INSERT INTO `doctor_specilization` (`id`, `name`, `user_id`, `reg_on`, `update_on`, `is_disable`) VALUES
(2, 'Gynecologist/Obstetrician', 0, '2020-04-23 15:52:37', '2020-04-23 16:13:21', 0),
(10, 'Homeopath', 0, '2020-04-23 17:58:28', '2020-04-23 17:58:28', 0),
(11, 'General Physician', 0, '2020-04-23 17:58:56', '2020-04-23 17:58:56', 0),
(13, 'Ear-Nose-Throat (ENT) Specialist', 0, '2020-04-23 17:59:44', '2020-04-23 18:00:06', 0),
(15, 'Dermatologist', 0, '2020-04-26 13:12:11', '2020-04-26 13:12:11', 0),
(18, 'Bushra Akhtar', 3, '2020-07-10 11:20:45', '2020-09-01 09:15:17', 1),
(19, 'Geriatric Medicine Specialists', 3, '2020-07-10 12:13:48', NULL, 0),
(20, 'Gastroenterologists', 3, '2020-07-10 12:13:56', NULL, 0),
(21, 'Family Physicians', 3, '2020-07-10 12:14:08', NULL, 0),
(22, 'Emergency Medicine Specialists', 3, '2020-07-10 12:14:10', NULL, 0),
(23, 'Endocrinologists', 3, '2020-07-10 12:14:17', NULL, 0),
(24, 'Dermatologists', 3, '2020-07-10 12:14:29', NULL, 0),
(25, 'Critical Care Medicine Specialists', 3, '2020-07-10 12:14:56', NULL, 0),
(26, 'Colon And Rectal Surgeons', 3, '2020-07-10 12:15:02', NULL, 0),
(27, 'Cardiologists', 3, '2020-07-10 12:15:07', '2020-09-30 18:57:24', 1),
(28, 'Anesthesiologists', 3, '2020-07-10 12:15:14', NULL, 0),
(29, 'Allergists/immunologists', 3, '2020-07-10 12:15:21', '2020-09-30 18:57:20', 1),
(30, 'Ophthalmologists', 3, '2020-07-10 12:56:02', NULL, 0),
(31, 'Oncologists', 3, '2020-07-10 12:56:05', NULL, 0),
(32, 'Obstetricians And Gynecologists', 3, '2020-07-10 12:56:14', NULL, 0),
(33, 'Neurologists', 3, '2020-07-10 12:56:17', NULL, 0),
(34, 'Nephrologists', 3, '2020-07-10 12:56:25', NULL, 0),
(35, 'Medical Geneticists', 3, '2020-07-10 12:56:34', NULL, 0),
(36, 'Internists', 3, '2020-07-10 12:56:37', NULL, 0),
(37, 'Infectious Disease Specialists', 3, '2020-07-10 12:56:45', NULL, 0),
(38, 'Hospice And Palliative Medicine Specialists', 3, '2020-07-10 12:56:51', NULL, 0),
(39, 'Hematologists', 3, '2020-07-10 12:56:57', NULL, 0),
(40, 'Osteopaths', 3, '2020-07-10 12:57:18', '2020-07-10 13:26:56', 0),
(41, 'Otolaryngologists', 3, '2020-07-10 12:57:21', NULL, 0),
(42, 'Pathologists', 3, '2020-07-10 12:57:26', NULL, 0),
(43, 'Pediatricians', 3, '2020-07-10 12:57:32', '2020-09-01 08:09:52', 1),
(44, 'Physiatrists', 3, '2020-07-10 12:57:39', NULL, 0),
(45, 'Plastic Surgeons', 3, '2020-07-10 12:57:45', NULL, 0),
(46, 'Podiatrists', 3, '2020-07-10 12:57:50', NULL, 0),
(47, 'Preventive Medicine Specialists', 3, '2020-07-10 12:57:56', NULL, 0),
(48, 'Psychiatrists', 3, '2020-07-10 12:58:00', NULL, 0),
(49, 'Pulmonologists', 3, '2020-07-10 12:58:04', NULL, 0),
(50, 'Radiologists', 3, '2020-07-10 12:58:07', NULL, 0),
(51, 'Rheumatologists', 3, '2020-07-10 12:58:10', NULL, 0),
(52, 'Urologists', 3, '2020-07-10 12:58:26', '2020-07-10 13:26:52', 0),
(53, 'General Surgeons', 3, '2020-07-10 12:58:31', NULL, 0),
(54, 'Sports Medicine Specialists', 3, '2020-07-10 12:58:36', '2020-07-22 17:53:41', 0),
(55, 'New One', 100, '2020-08-01 05:38:59', '2020-09-01 08:06:50', 1),
(56, 'New Spacializ', 224, '2020-10-03 07:11:12', '2020-10-03 07:18:53', 0),
(57, 'Ali Ali', 255, '2020-10-28 14:02:49', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctor_specilization`
--
ALTER TABLE `doctor_specilization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctor_specilization`
--
ALTER TABLE `doctor_specilization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
