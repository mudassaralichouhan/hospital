-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 04:33 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `consultant`
--

CREATE TABLE `consultant` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `specialization` int(11) NOT NULL,
  `contact` varchar(13) NOT NULL,
  `fee` int(11) NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `consultant_perc` int(11) NOT NULL DEFAULT 0,
  `lab_perc` int(11) NOT NULL DEFAULT 0,
  `dental_perc` int(11) NOT NULL DEFAULT 0,
  `surgical_perc` int(11) NOT NULL DEFAULT 0,
  `xray_perc` int(11) NOT NULL DEFAULT 0,
  `location` varchar(150) NOT NULL DEFAULT 'Not mention Home Town',
  `reg_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_disable` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultant`
--

INSERT INTO `consultant` (`id`, `name`, `gender`, `age`, `specialization`, `contact`, `fee`, `user_id`, `consultant_perc`, `lab_perc`, `dental_perc`, `surgical_perc`, `xray_perc`, `location`, `reg_on`, `update_on`, `is_disable`) VALUES
(1, 'Zesshan Salamat', 'Male', 35, 15, '03002202022', 0, 212, 0, 0, 0, 0, 0, 'Muridke', '2020-04-11 14:00:00', '2020-10-16 13:56:16', 0),
(2, 'Nadeem Haider', 'Male', 45, 22, '03002536400', 0, 233, 30, 0, 0, 0, 0, 'lahore', '2020-04-11 14:00:00', '2020-10-16 13:56:21', 0),
(3, 'Bushra Bukhari', 'Female', 50, 31, '03002223363', 300, 245, 40, 5, 0, 0, 0, 'Karachi', '2020-04-11 14:00:00', '2020-10-16 13:56:26', 0),
(6, 'Naseebo Lal', 'Female', 65, 10, '03046787678', 0, 270, 10, 0, 20, 0, 0, 'Gujrawala', '2020-04-23 12:05:03', '2020-10-16 13:57:11', 0),
(9, 'Sarver Husain', 'Male', 25, 42, '03002536253', 0, 260, 19, 21, 61, 82, 0, 'Muridke', '2020-04-23 18:20:02', '2020-10-16 13:57:20', 0),
(10, 'Mustafa Gujjar', 'Male', 30, 35, '03001456969', 500, 250, 20, 0, 0, 0, 0, 'You Want to Mansion You Home Town', '2020-04-23 18:26:28', '2020-10-16 13:57:27', 0),
(27, 'Naveed Batti', 'Male', 37, 13, '03004343444', 300, 240, 43, 50, 66, 10, 13, 'Gujrawala Pakistan', '2020-05-25 21:36:41', '2020-10-16 13:57:33', 0),
(90, 'Nawaz Sharif', 'Male', 66, 11, '03005505544', 1100, 230, 70, 20, 5, 10, 10, 'Lahore Modal Town, Panjab, Pakistan', '2020-06-30 02:26:40', '2020-10-16 13:57:37', 0),
(91, 'Rafiq Gori', 'Male', 55, 2, '03215566998', 2500, 220, 70, 10, 0, 20, 5, 'Muridke, Panjab, Pakistan.', '2020-07-15 14:18:48', '2020-10-16 13:57:41', 0),
(92, 'Anum Butt', 'Female', 30, 57, '03002514698', 1000, 215, 75, 10, 2, 50, 15, 'Muridke, Panjab, Pakistan', '2020-08-01 04:36:12', '2020-11-06 03:04:10', 0),
(93, 'Imelda Bartoletti', 'Male', 50, 28, '03411188739', 100, 225, 35, 5, 25, 30, 30, '921 Vivien Falls\r\nKihnmouth, MI 94677-4703', '2020-09-17 18:19:47', '2020-10-16 18:20:03', 0),
(94, 'Julio Brakus', 'Male', 50, 33, '03069302883', 550, 235, 25, 35, 10, 50, 40, '3116 Rico Avenue\nGleasonfurt, NH 28271', '2015-04-04 03:26:35', '2020-10-16 13:58:07', 0),
(95, 'Amely Champlin', 'Male', 37, 30, '03196092475', 1450, 245, 20, 5, 10, 50, 25, '57554 Kiara Heights\nMoisesland, NM 34686', '2020-09-16 13:58:12', '2020-11-04 23:59:34', 1),
(96, 'Elisabeth Hettinger', 'Male', 36, 49, '03298462627', 350, 255, 5, 5, 15, 50, 15, '97985 Kristian Via Suite 135\nSouth Leilaview, NY 53996', '2015-08-04 13:23:42', '2020-10-16 13:58:25', 0),
(97, 'Dr. Oswaldo Wintheiser I', 'Female', 39, 21, '03091134271', 1100, 265, 70, 25, 15, 25, 50, '58900 Tate Knolls\nRogahnmouth, ND 86463', '2015-08-04 04:15:16', '2020-10-16 13:58:29', 0),
(98, 'Garret Bernhard', 'Female', 51, 52, '03066577553', 500, 244, 70, 10, 25, 45, 25, '1709 Bogisich Estates\nLake Rosanna, KS 68678-7726', '2015-05-07 11:15:40', '2020-10-16 13:58:41', 0),
(99, 'Retta Fisher', 'Male', 29, 25, '03066354183', 750, 233, 45, 45, 30, 35, 5, '96834 Shanna Estate\nQuigleytown, RI 38342-4812', '2015-03-17 13:19:11', '2020-10-16 13:58:44', 0),
(100, 'Ilene Bahringer', 'Male', 37, 28, '03009876102', 1250, 251, 50, 45, 25, 40, 50, '3061 Cortney Valley Suite 200\nNew Taniaport, WA 85141', '2020-09-16 13:58:12', '2020-10-16 18:22:19', 0),
(101, 'Reece Lubowitz', 'Female', 44, 15, '03038871136', 1200, 266, 45, 40, 15, 50, 30, '3974 Watsica Roads\nMedhurstfurt, WV 23597-6610', '2015-05-20 06:20:16', '2020-10-16 14:01:29', 0),
(102, 'Dr. Elroy Considine', 'Female', 42, 40, '03098123186', 1400, 215, 10, 5, 20, 15, 5, '68758 Dickens Mountain\nHoppestad, NE 61562', '2020-09-16 13:58:12', '2020-10-16 18:22:19', 0),
(103, 'Queen Hegmann DVM', 'Male', 39, 49, '03084712623', 1400, 212, 45, 50, 10, 5, 45, '8010 Veum Keys Suite 257\nNorth Estrellaport, NM 46005-7793', '2015-11-03 00:09:38', '2020-10-16 14:00:58', 0),
(104, 'Breanne Sawayn', 'Male', 30, 49, '03041691217', 1150, 244, 65, 35, 5, 30, 45, '5125 Anderson Stravenue\nKozeyhaven, SD 64841-8767', '2015-12-04 12:08:38', '2020-10-16 14:00:42', 0),
(105, 'Jannie Christiansen', 'Female', 36, 11, '03269867217', 200, 233, 70, 30, 30, 20, 25, '115 McClure Springs\nOsinskimouth, WV 77254-8697', '2015-09-27 12:06:35', '2020-10-16 14:00:38', 0),
(106, 'Dr. Brandt Mosciski MD', 'Male', 32, 46, '03462210490', 800, 220, 30, 50, 10, 10, 15, '81855 Mathew Circles Suite 942\nNew Vivianeton, DE 76409', '2015-03-09 17:20:36', '2020-10-16 14:00:32', 0),
(107, 'Karl Keebler I', 'Female', 58, 22, '03211127749', 800, 220, 10, 25, 20, 15, 40, '266 Jalyn Meadows\nHenrietteport, MT 53974', '2015-02-02 11:09:18', '2020-10-10 10:42:42', 0),
(108, 'Miss Brandy Metz DDS', 'Female', 55, 49, '03071962475', 1000, 0, 15, 15, 25, 50, 20, '40600 Lura Unions Apt. 360\nCornellside, KY 97498', '2015-10-25 16:09:28', NULL, 0),
(109, 'Albertha Hauck Jr.', 'Male', 48, 42, '03386360492', 1200, 0, 20, 40, 15, 40, 45, '4827 Lang Haven\nPort Mark, WA 30348', '2016-07-02 10:04:31', '2020-09-27 07:12:21', 1),
(110, 'Doug DuBuque', 'Male', 34, 32, '03041767192', 550, 0, 60, 5, 10, 40, 15, '12373 Rico Islands\nLake Janshire, NC 54396', '2016-08-04 07:13:12', NULL, 0),
(111, 'Janessa Abernathy', 'Male', 52, 35, '03252401916', 300, 0, 65, 35, 25, 5, 50, '268 Javon Rue Apt. 858\nAlexandreaport, FL 54357-4045', '2016-08-14 14:14:46', NULL, 0),
(112, 'Beau Gusikowski', 'Male', 57, 36, '03242891139', 600, 0, 10, 25, 25, 5, 5, '438 Larue Extension\nLake Norbertobury, SC 89035-7391', '2016-10-03 06:11:46', NULL, 0),
(113, 'Luigi Frami', 'Male', 45, 11, '03082468976', 1100, 0, 20, 30, 10, 50, 25, '806 Otha Throughway Apt. 451\nPort Adrianmouth, UT 73744', '2017-05-27 23:02:46', NULL, 0),
(114, 'Luigi Jacobi', 'Male', 54, 24, '03156998161', 50, 0, 10, 10, 5, 15, 35, '8067 Anais Hill Apt. 442\nParkerborough, FL 74128', '2017-06-24 04:10:43', NULL, 0),
(115, 'Noelia Mayer', 'Male', 56, 28, '03098497073', 750, 0, 15, 35, 10, 5, 10, '330 Rath Rue\nVernieland, AK 60016-1587', '2017-07-05 01:15:13', NULL, 0),
(116, 'Belle Larson II', 'Male', 33, 55, '03115595251', 1150, 0, 35, 5, 15, 50, 10, '5486 Connie Road\nPort Leliaburgh, NV 72836-4426', '2017-03-16 18:08:39', '2020-08-15 23:36:04', 0),
(117, 'Claude Hand', 'Male', 41, 30, '03062821727', 1000, 0, 40, 25, 30, 10, 20, '22733 Emilia Ridge\nJulienview, KS 40423', '2017-11-15 12:21:42', NULL, 0),
(118, 'Oral Kreiger Sr.', 'Male', 45, 42, '03031379458', 650, 0, 65, 20, 30, 10, 40, '429 Earnestine Inlet Suite 918\nLaurettamouth, CO 88748-3251', '2018-07-07 12:04:18', NULL, 0),
(119, 'Dr. Johann Jakubowski IV', 'Female', 47, 45, '03357075653', 1300, 0, 45, 25, 5, 45, 40, '3560 Boehm Mews\nStiedemannview, MN 00472', '2018-08-25 13:20:45', NULL, 0),
(120, 'Dr. Gilbert Baumbach I', 'Female', 47, 30, '03354981896', 1150, 0, 20, 40, 10, 20, 30, '4007 Wyman Pike Apt. 885\nPort Wayneville, NH 04268-6569', '2018-02-27 15:28:16', NULL, 0),
(121, 'Myrtie Nader', 'Female', 41, 42, '03081627127', 650, 0, 45, 50, 15, 10, 20, '7190 Melissa Landing Suite 518\nHauckburgh, SD 29910-0807', '2019-09-14 01:23:19', NULL, 0),
(122, 'Joanne Hyatt', 'Female', 41, 39, '03171600840', 200, 229, 15, 5, 30, 25, 50, '64261 Corene Brook\nChristiansenview, OR 92348', '2020-09-16 13:58:12', '2020-10-16 18:22:19', 0),
(123, 'Marta Daniel', 'Female', 53, 51, '03417464189', 1200, 239, 30, 5, 30, 30, 45, '52246 McKenzie Prairie\nPagacville, NJ 03304-9977', '2019-03-23 11:25:21', '2020-10-16 14:03:20', 0),
(124, 'Prof. Justyn Turcotte', 'Female', 45, 25, '03044759613', 500, 238, 20, 10, 20, 30, 40, '984 Sawayn Shore Suite 835\nBergstromton, ND 56140', '2020-09-16 13:58:12', '2020-10-16 18:22:19', 0),
(125, 'Mrs. Wava Zieme', 'Female', 49, 18, '03091411039', 50, 0, 65, 15, 5, 45, 5, '520 Ken Groves\nPort Robynmouth, AK 39054', '2019-03-02 03:00:47', NULL, 0),
(126, 'Miss Madelyn Conn', 'Male', 59, 25, '03078936129', 50, 0, 5, 25, 25, 25, 25, '210 Pagac View\nAnkundingfurt, WA 74494-9877', '2019-09-15 13:19:29', NULL, 0),
(127, 'Oscar Walter', 'Female', 51, 52, '03081528406', 1250, 255, 10, 25, 20, 10, 45, '1653 Von Harbor\nMayerberg, NY 57252-3682', '2019-09-03 23:04:48', '2020-10-16 14:03:10', 0),
(128, 'Kathlyn Ortiz', 'Male', 51, 32, '03412160076', 50, 249, 50, 15, 25, 10, 20, '655 Florida Stravenue Apt. 445\nGrahamshire, LA 34302', '2019-07-27 16:09:50', '2020-10-16 14:02:55', 0),
(129, 'Dr. Jayda Ortiz I', 'Female', 37, 20, '03078265119', 1450, 256, 10, 45, 20, 45, 50, '5368 Jordon Extension\nNorth Adellmouth, CO 91265-8111', '2020-09-16 13:58:12', '2020-10-16 18:22:19', 0),
(130, 'Dawson Cremin', 'Female', 43, 46, '03008664285', 1150, 249, 40, 10, 5, 35, 40, '60197 Ratke Port\nCassandramouth, UT 28252-1527', '2019-10-19 21:09:38', '2020-10-16 14:02:43', 0),
(131, 'Rosendo Walter III', 'Male', 51, 30, '03189361000', 300, 259, 5, 5, 10, 40, 50, '1750 Witting Roads\nEleazartown, VT 92378-4739', '2019-11-13 21:20:11', '2020-10-16 14:02:38', 0),
(132, 'Titus Mann Sr.', 'Male', 45, 53, '03143227718', 1200, 253, 35, 40, 25, 20, 50, '2770 Aliyah Expressway Suite 775\nPort Adelbert, KS 68996', '2019-12-14 10:02:35', '2020-10-16 14:02:32', 0),
(133, 'Usman Idress', 'Male', NULL, 49, '03005467895', 500, 254, 80, 30, 0, 35, 20, 'THQ Muridke Pakistan', '2020-08-07 09:31:50', '2020-10-16 14:02:28', 0),
(134, 'Hello', 'Male', NULL, 18, '03002202020', 200, 256, 100, 12, 76, 74, 66, 'Nothing else', '2020-08-26 06:27:56', '2020-11-04 23:57:58', 1),
(135, 'Waqas Ali', 'Male', 50, 52, '03004005001', 500, 261, 50, 10, 10, 5, 2, 'Araf wala', '2020-09-24 08:29:16', '2020-10-16 14:02:19', 0),
(136, 'Ahsan Gujar', 'Male', 30, 47, '03004587500', 200, 264, 80, 5, 0, 0, 0, 'Muridek, pnajab', '2020-09-27 07:14:11', '2020-11-04 13:34:14', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultant`
--
ALTER TABLE `consultant`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultant`
--
ALTER TABLE `consultant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
