-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 04:32 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_dental_procedure`
--

CREATE TABLE `product_dental_procedure` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `reg_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_disable` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_dental_procedure`
--

INSERT INTO `product_dental_procedure` (`id`, `name`, `user_id`, `price`, `reg_on`, `update_on`, `is_disable`) VALUES
(1, 'Dressing', 0, 400, '2020-06-02 01:18:36', NULL, 0),
(3, 'Filling', 0, 700, '2020-06-07 07:30:32', NULL, 0),
(4, 'Hospital Admission Fee', 47, 400, '2020-07-11 03:24:57', NULL, 0),
(5, 'Surgical Impaction', 57, 1500, '2020-07-11 03:24:57', NULL, 0),
(6, 'Anesthesia', 91, 600, '2020-07-11 03:24:57', NULL, 0),
(7, 'Operation Theater', 55, 600, '2020-07-11 03:24:57', NULL, 0),
(8, 'Room Rent', 86, 800, '2020-07-11 03:24:57', NULL, 0),
(9, 'Consultant Visit', 90, 50, '2020-07-11 03:24:57', '2020-09-30 05:41:53', 1),
(10, 'Service', 25, 110, '2020-07-11 03:24:57', '2020-10-03 05:21:33', 0),
(11, 'Extraction', 8, 20, '2020-07-11 03:24:57', NULL, 0),
(12, 'Aluio Plastery', 24, 160, '2020-07-11 03:24:57', '2020-10-03 05:08:25', 0),
(13, 'Ampection', 6, 260, '2020-07-11 03:24:57', '2020-10-03 05:08:54', 0),
(14, 'Scaling', 9, 100, '2020-07-11 03:24:57', NULL, 0),
(15, 'Flap Surgery', 66, 200, '2020-07-11 03:24:57', NULL, 0),
(16, 'Root Canal Treatment', 69, 300, '2020-07-11 03:24:57', NULL, 0),
(17, 'Pultainy', 63, 100, '2020-07-11 03:24:57', NULL, 0),
(18, 'Extraction', 8, 20, '2020-07-11 03:24:57', '2020-10-03 04:45:18', 1),
(19, 'Apexification', 36, 300, '2020-07-11 03:24:57', NULL, 0),
(20, 'Filling (temporary)', 68, 50, '2020-07-11 03:24:57', NULL, 0),
(21, 'Filling (glass Ionomer)', 90, 80, '2020-07-11 03:24:57', NULL, 0),
(22, 'Filling (amalgum)', 50, 100, '2020-07-11 03:24:57', '2020-07-11 04:38:05', 1),
(23, 'Filling (composite)', 20, 200, '2020-07-11 03:24:57', NULL, 0),
(24, 'Root Canal Treatment', 12, 300, '2020-07-11 03:24:57', NULL, 0),
(25, 'Filling (temporary)', 28, 50, '2020-07-11 03:24:57', '2020-10-03 05:18:44', 1),
(26, 'Filling (gic)', 68, 80, '2020-07-11 03:24:57', NULL, 0),
(27, 'Filling (amalgum)', 24, 100, '2020-07-11 03:24:57', '2020-07-11 04:36:32', 1),
(28, 'Filling (composite)', 22, 200, '2020-07-11 03:24:57', NULL, 0),
(29, 'Crown, Bridge', 69, 850, '2020-07-11 03:24:57', NULL, 0),
(30, 'Acrylic Partial Denture A.P.D', 30, 50, '2020-07-11 03:24:57', '2020-10-03 04:45:01', 1),
(31, 'Cast Partial Denture (cpd)', 65, 2500, '2020-07-11 03:24:57', '2020-10-03 04:57:45', 1),
(32, 'Full Denture (single Jaw)', 12, 300, '2020-07-11 03:24:57', NULL, 0),
(33, 'Full Denture (both Jaws)', 32, 600, '2020-07-11 03:24:57', NULL, 0),
(34, 'Surgical Splint', 32, 100, '2020-07-11 03:24:57', '2020-07-11 04:33:25', 1),
(35, 'Obturator', 12, 300, '2020-07-11 03:24:57', NULL, 0),
(36, 'Cranical Plates', 57, 300, '2020-07-11 03:24:58', NULL, 0),
(37, 'Occlusual Splint', 73, 300, '2020-07-11 03:24:58', NULL, 0),
(38, 'Denture Repair', 52, 75, '2020-07-11 03:24:58', '2020-07-11 03:33:40', 0),
(39, 'Permenant Metal Splint', 8, 700, '2020-07-11 03:24:58', NULL, 0),
(40, 'Acryine Gum Splint/veneers', 20, 300, '2020-07-11 03:24:58', '2020-07-11 04:33:28', 1),
(41, 'Fixed Appliance (single Arch)', 1, 1500, '2020-07-11 03:24:58', NULL, 0),
(42, 'Fixed Appliance (both Arch)', 73, 3000, '2020-07-11 03:24:58', NULL, 0),
(43, 'Removable Appliance', 28, 1500, '2020-07-11 03:24:58', NULL, 0),
(44, 'Functional Appliance', 90, 1500, '2020-07-11 03:24:58', '2020-07-11 04:38:35', 1),
(45, 'Rapid Maxillary Expander (rme)', 63, 1000, '2020-07-11 03:24:58', NULL, 0),
(46, 'Extra Oral Traction', 10, 500, '2020-07-11 03:24:58', NULL, 0),
(47, 'Lingual Holding Wire/nance Button (per Arch)', 2, 500, '2020-07-11 03:24:58', NULL, 0),
(48, 'Quardhelix', 81, 1000, '2020-07-11 03:24:58', NULL, 0),
(49, 'Face Mask', 92, 1000, '2020-07-11 03:24:58', NULL, 0),
(50, 'Space Mamtander', 21, 500, '2020-07-11 03:24:58', NULL, 0),
(51, 'Chip-cup', 87, 1000, '2020-07-11 03:24:58', NULL, 0),
(52, 'Oral Screen', 42, 500, '2020-07-11 03:24:58', NULL, 0),
(53, 'Finsh Ok', 100, 350, '2020-08-01 05:41:13', '2020-10-03 05:18:39', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_dental_procedure`
--
ALTER TABLE `product_dental_procedure`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_dental_procedure`
--
ALTER TABLE `product_dental_procedure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
