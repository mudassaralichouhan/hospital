-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 04:32 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_lab_test`
--

CREATE TABLE `product_lab_test` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `reg_on` datetime NOT NULL DEFAULT current_timestamp(),
  `update_on` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_disable` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_lab_test`
--

INSERT INTO `product_lab_test` (`id`, `name`, `user_id`, `price`, `reg_on`, `update_on`, `is_disable`) VALUES
(1, 'HIV', 0, 500, '2020-04-11 22:47:35', NULL, 0),
(2, 'Cholesterol', 0, 360, '2020-04-11 22:52:33', NULL, 0),
(3, 'Liver Function Test (L.F.T)', 0, 800, '2020-04-11 22:54:04', NULL, 0),
(4, 'MCHC', 0, 250, '2020-04-11 22:54:29', NULL, 0),
(5, 'MEASELES LGM ABS', 4, 1600, '2020-04-12 00:49:42', '2020-06-08 12:28:23', 0),
(6, 'MEASELES LGM ABS.', 0, 1600, '2020-04-12 00:50:05', '2020-07-15 10:33:50', 0),
(7, 'GAMMA GT', 0, 250, '2020-04-12 01:01:50', NULL, 0),
(8, 'GASTRIN LEVEL', 0, 2400, '2020-04-12 01:02:11', NULL, 0),
(9, 'GCT (GLUCOSE CHALLANCE TEST)', 0, 500, '2020-04-12 01:02:32', NULL, 0),
(10, 'HBEAG', 0, 1100, '2020-04-12 01:02:52', NULL, 0),
(11, 'HCV GENOTYPING', 0, 1700, '2020-04-12 01:03:11', NULL, 0),
(12, 'HBV BY PCR (QUALITATIVE) REAL ', 0, 4000, '2020-04-12 01:03:31', NULL, 0),
(13, 'HBV BY PCR (QUANTITATION) REAL', 4, 8500, '2020-04-12 01:04:03', '2020-06-08 12:28:28', 0),
(14, 'HCV BY PCR (QUALITATIVE) REAL ', 0, 4000, '2020-04-12 01:04:39', NULL, 0),
(16, 'Digital', 0, 900, '2020-06-01 18:41:00', NULL, 0),
(18, 'Blood Group', 0, 700, '2020-06-07 00:32:45', '2020-07-15 10:24:21', 0),
(21, '2d Echo', 100, 1105, '2020-07-15 11:16:38', NULL, 0),
(22, '4d Scan', 40, 22, '2020-07-15 11:16:38', NULL, 0),
(23, 'Acth (adreno Corticotropic Hor', 23, 1024, '2020-07-15 11:16:38', NULL, 0),
(24, 'Adenosine Deaminase Test', 91, 271, '2020-07-15 11:16:38', NULL, 0),
(25, 'Aec (absolute Eosinophil Count', 12, 2593, '2020-07-15 11:16:39', NULL, 0),
(26, 'Afb (acid Fast Bacilli) Cultur', 71, 869, '2020-07-15 11:16:39', NULL, 0),
(27, 'Afp (alpha Feto Protein) Test', 26, 1992, '2020-07-15 11:16:39', NULL, 0),
(28, 'Alberts Stain', 35, 185, '2020-07-15 11:16:39', NULL, 0),
(29, 'Albumin Test', 55, 3381, '2020-07-15 11:16:39', NULL, 0),
(30, 'Aldolase Test', 70, 249, '2020-07-15 11:16:39', NULL, 0),
(31, 'Alkaline Phosphatase (alp) Tes', 29, 3290, '2020-07-15 11:16:39', NULL, 0),
(32, 'Allergy Test', 81, 289, '2020-07-15 11:16:39', NULL, 0),
(33, 'Ammonia Test', 59, 1245, '2020-07-15 11:16:39', NULL, 0),
(34, 'Amylase Test', 16, 3260, '2020-07-15 11:16:39', NULL, 0),
(35, 'Ana (antinuclear Antibody) Tes', 53, 1765, '2020-07-15 11:16:39', NULL, 0),
(36, 'Anc Profile', 7, 1471, '2020-07-15 11:16:39', NULL, 0),
(37, 'Anca Profile', 65, 465, '2020-07-15 11:16:39', NULL, 0),
(38, 'Anti Ccp (accp) Test', 10, 837, '2020-07-15 11:16:39', NULL, 0),
(39, 'Anti Phospholipid (apl) Test', 12, 294, '2020-07-15 11:16:39', NULL, 0),
(40, 'Anti Tpo Test', 38, 1490, '2020-07-15 11:16:39', NULL, 0),
(41, 'Anti-mullerian Hormone (amh) T', 31, 440, '2020-07-15 11:16:39', NULL, 0),
(42, 'Antithyroglobulin Antibody Tes', 30, 1231, '2020-07-15 11:16:39', NULL, 0),
(43, 'Antithyroid Microsomal Antibod', 72, 343, '2020-07-15 11:16:39', NULL, 0),
(44, 'Aptt (activated Partial Thromb', 49, 2763, '2020-07-15 11:16:39', NULL, 0),
(45, 'Arterial Blood Gas (abg)', 55, 123, '2020-07-15 11:16:39', NULL, 0),
(46, 'Ascitic Fluid Test', 55, 125, '2020-07-15 11:16:39', NULL, 0),
(47, 'Aso Test', 91, 3171, '2020-07-15 11:16:39', '2020-07-22 22:48:28', 1),
(48, 'Audiometry Test', 13, 94, '2020-07-15 11:16:39', NULL, 0),
(49, 'Beta Hcg Test', 9, 3455, '2020-07-15 11:16:39', NULL, 0),
(50, 'Beta Thalassemia Test', 18, 187, '2020-07-15 11:16:39', NULL, 0),
(51, 'Bicarbonate Test', 90, 1677, '2020-07-15 11:16:39', NULL, 0),
(52, 'Bilirubin Test', 19, 4601, '2020-07-15 11:16:39', NULL, 0),
(53, 'Biopsy', 79, 358, '2020-07-15 11:16:39', NULL, 0),
(54, 'Bleeding / Clotting Time Test', 14, 4711, '2020-07-15 11:16:39', NULL, 0),
(55, 'Blood Culture Test', 9, 3049, '2020-07-15 11:16:39', NULL, 0),
(56, 'Blood Group Test', 100, 5694, '2020-07-15 11:16:39', NULL, 0),
(57, 'Blood Sugar Test', 69, 6063, '2020-07-15 11:16:39', NULL, 0),
(58, 'Blood Urea Nitrogen Test', 55, 5253, '2020-07-15 11:16:39', NULL, 0),
(59, 'Bone Density Test / Dexa Scan', 51, 273, '2020-07-15 11:16:39', NULL, 0),
(60, 'Bone Scan', 24, 39, '2020-07-15 11:16:39', NULL, 0),
(61, 'C-peptide Test', 43, 689, '2020-07-15 11:16:39', NULL, 0),
(62, 'Ca 15.3 Test', 34, 1547, '2020-07-15 11:16:40', NULL, 0),
(63, 'Ca 19.9 Test', 10, 1577, '2020-07-15 11:16:40', NULL, 0),
(64, 'Ca 27.29 Test', 45, 17, '2020-07-15 11:16:40', NULL, 0),
(65, 'Ca-125 (tumor Marker) Test', 84, 2599, '2020-07-15 11:16:40', NULL, 0),
(66, 'Calcium Test', 62, 5082, '2020-07-15 11:16:40', NULL, 0),
(67, 'Carbamazepine (tegretol) Test', 29, 313, '2020-07-15 11:16:40', NULL, 0),
(68, 'Cardiolipin Antibodies (acl)', 69, 501, '2020-07-15 11:16:40', NULL, 0),
(69, 'Cbc / Hemogram Test', 91, 5477, '2020-07-15 11:16:40', NULL, 0),
(70, 'Cd4 Test', 48, 995, '2020-07-15 11:16:40', NULL, 0),
(71, 'Cea (carcinoembryonic Antigen)', 40, 1513, '2020-07-15 11:16:40', NULL, 0),
(72, 'Cerebral Spinal Fluid (csf) Te', 90, 287, '2020-07-15 11:16:40', NULL, 0),
(73, 'Chikungunya Test', 24, 376, '2020-07-15 11:16:40', NULL, 0),
(74, 'Chlamydia Test', 6, 338, '2020-07-15 11:16:40', NULL, 0),
(75, 'Chloride Test', 39, 3972, '2020-07-15 11:16:40', NULL, 0),
(76, 'Cholesterol Test', 77, 4298, '2020-07-15 11:16:40', NULL, 0),
(77, 'Ck-mb Test', 40, 1837, '2020-07-15 11:16:40', NULL, 0),
(78, 'Color Doppler', 78, 984, '2020-07-15 11:16:40', NULL, 0),
(79, 'Complement C3', 96, 506, '2020-07-15 11:16:40', NULL, 0),
(80, 'Complement C4', 88, 508, '2020-07-15 11:16:40', NULL, 0),
(81, 'Coombs Test', 79, 2344, '2020-07-15 11:16:40', NULL, 0),
(82, 'Cortisol Test', 49, 1608, '2020-07-15 11:16:40', NULL, 0),
(83, 'Cpk (creatine Phosphokinase) T', 36, 2455, '2020-07-15 11:16:40', NULL, 0),
(84, 'Creatinine Clearance Test', 50, 1182, '2020-07-15 11:16:40', NULL, 0),
(85, 'Creatinine Test', 48, 4749, '2020-07-15 11:16:40', NULL, 0),
(86, 'Crp (c-reactive Protein) Test', 69, 3192, '2020-07-15 11:16:40', NULL, 0),
(87, 'Cryptococcal Antigen Test', 92, 152, '2020-07-15 11:16:40', NULL, 0),
(88, 'Ct Scan', 22, 2614, '2020-07-15 11:16:41', NULL, 0),
(89, 'Cytomegalovirus (cmv) Test', 57, 473, '2020-07-15 11:16:41', NULL, 0),
(90, 'D Dimer Test', 79, 975, '2020-07-15 11:16:41', NULL, 0),
(91, 'Dengue Igg Test', 72, 3799, '2020-07-15 11:16:41', NULL, 0),
(92, 'Dengue Igm Test', 53, 2508, '2020-07-15 11:16:41', NULL, 0),
(93, 'Dengue Ns1 Test', 85, 734, '2020-07-15 11:16:41', NULL, 0),
(94, 'Dhea Test', 93, 1119, '2020-07-15 11:16:41', NULL, 0),
(95, 'Dmsa Scan', 35, 6, '2020-07-15 11:16:41', NULL, 0),
(96, 'Dna Test', 98, 139, '2020-07-15 11:16:41', NULL, 0),
(97, 'Double Marker Test', 62, 536, '2020-07-15 11:16:41', NULL, 0),
(98, 'Ecg', 83, 2062, '2020-07-15 11:16:41', NULL, 0),
(99, 'Eeg', 98, 377, '2020-07-15 11:16:41', NULL, 0),
(100, 'Electrolytes Test', 49, 5001, '2020-07-15 11:16:41', NULL, 0),
(101, 'Electromyography (emg)', 98, 137, '2020-07-15 11:16:41', NULL, 0),
(102, 'Esr (erythrocyte Sedimentation', 86, 3537, '2020-07-15 11:16:41', NULL, 0),
(103, 'Estradiol (e2) Test', 64, 1651, '2020-07-15 11:16:41', NULL, 0),
(104, 'Factor V Leiden Test', 88, 239, '2020-07-15 11:16:41', NULL, 0),
(105, 'Ferritin Test', 7, 2163, '2020-07-15 11:16:41', NULL, 0),
(106, 'Fnac Test', 41, 388, '2020-07-15 11:16:41', NULL, 0),
(107, 'Folic Acid Test', 1, 2649, '2020-07-15 11:16:41', NULL, 0),
(108, 'Fsh (follicle Stimulating Horm', 96, 3400, '2020-07-15 11:16:41', NULL, 0),
(109, 'Fungal Culture Test', 62, 390, '2020-07-15 11:16:41', NULL, 0),
(110, 'G6pd Test', 44, 1815, '2020-07-15 11:16:41', NULL, 0),
(111, 'Gallium Scan', 84, 23, '2020-07-15 11:16:41', NULL, 0),
(112, 'Gamma Gt (ggtp) Test', 64, 1884, '2020-07-15 11:16:41', NULL, 0),
(113, 'Globulin / Ag Ratio', 56, 537, '2020-07-15 11:16:41', NULL, 0),
(114, 'Globulin Test', 91, 2258, '2020-07-15 11:16:42', NULL, 0),
(115, 'Glucose Tolerance Test (gtt)', 60, 2982, '2020-07-15 11:16:42', NULL, 0),
(116, 'Gram Stain Test', 13, 567, '2020-07-15 11:16:42', NULL, 0),
(117, 'Hba1c Test', 29, 3733, '2020-07-15 11:16:42', NULL, 0),
(118, 'Hbeab (hepatitis B Antibody)', 99, 288, '2020-07-15 11:16:42', NULL, 0),
(119, 'Hbsag Test', 90, 4628, '2020-07-15 11:16:42', NULL, 0),
(120, 'Hcv Antibody Test', 85, 2749, '2020-07-15 11:16:42', NULL, 0),
(121, 'Hdl Cholesterol', 78, 2563, '2020-07-15 11:16:42', NULL, 0),
(122, 'Helicobacter Pylori Test', 4, 331, '2020-07-15 11:16:42', NULL, 0),
(123, 'Hemoglobin (hb) Test', 57, 4242, '2020-07-15 11:16:42', NULL, 0),
(124, 'Hemoglobin Electrophoresis', 44, 748, '2020-07-15 11:16:42', NULL, 0),
(125, 'Hepatitis A Test', 1, 534, '2020-07-15 11:16:42', NULL, 0),
(126, 'Hepatitis E Test', 82, 317, '2020-07-15 11:16:42', NULL, 0),
(127, 'Herpes Simplex Virus (hsv) Tes', 81, 618, '2020-07-15 11:16:42', NULL, 0),
(128, 'Hgh Test', 5, 747, '2020-07-15 11:16:42', NULL, 0),
(129, 'Hida Scan', 54, 5, '2020-07-15 11:16:42', NULL, 0),
(130, 'Hiv Test', 47, 5345, '2020-07-15 11:16:42', NULL, 0),
(131, 'Hla B27 Test', 38, 673, '2020-07-15 11:16:42', NULL, 0),
(132, 'Homocysteine Test', 1, 984, '2020-07-15 11:16:42', NULL, 0),
(133, 'Hsg Test', 13, 167, '2020-07-15 11:16:42', NULL, 0),
(134, 'Insulin Test', 39, 2677, '2020-07-15 11:16:42', NULL, 0),
(135, 'Iron Test', 20, 3073, '2020-07-15 11:16:42', NULL, 0),
(136, 'Karyotype Test', 10, 25, '2020-07-15 11:16:43', NULL, 0),
(137, 'Kidney / Renal Function Test', 45, 3641, '2020-07-15 11:16:43', NULL, 0),
(138, 'L. E. Cells Test', 82, 677, '2020-07-15 11:16:43', NULL, 0),
(139, 'Ldh (lactate Dehydrogenase) Te', 19, 1513, '2020-07-15 11:16:43', NULL, 0),
(140, 'Ldl Cholesterol', 16, 2360, '2020-07-15 11:16:43', NULL, 0),
(141, 'Lh (luteinizing Hormone) Test', 76, 3170, '2020-07-15 11:16:43', NULL, 0),
(142, 'Lipase Test', 98, 2925, '2020-07-15 11:16:43', NULL, 0),
(143, 'Lipid Profile', 68, 5477, '2020-07-15 11:16:43', NULL, 0),
(144, 'Lipoprotein A / Lp(a) Test', 87, 944, '2020-07-15 11:16:43', NULL, 0),
(145, 'Lithium Test', 82, 1549, '2020-07-15 11:16:43', NULL, 0),
(146, 'Liver Function Test (lft)', 68, 4763, '2020-07-15 11:16:43', NULL, 0),
(147, 'Lupus Anticoagulant (lac) Test', 81, 915, '2020-07-15 11:16:43', NULL, 0),
(148, 'Magnesium Test', 54, 1953, '2020-07-15 11:16:43', NULL, 0),
(149, 'Malaria (malarial Parasite) Te', 80, 3833, '2020-07-15 11:16:43', NULL, 0),
(150, 'Mammography', 43, 494, '2020-07-15 11:16:43', NULL, 0),
(151, 'Mantoux Test', 13, 396, '2020-07-15 11:16:43', NULL, 0),
(152, 'Mibg Scan', 58, 4, '2020-07-15 11:16:43', NULL, 0),
(153, 'Microalbumin Test', 84, 1904, '2020-07-15 11:16:43', NULL, 0),
(154, 'Microfilaria Parasite Test', 94, 1057, '2020-07-15 11:16:43', NULL, 0),
(155, 'Mri Scan', 28, 1981, '2020-07-15 11:16:44', NULL, 0),
(156, 'Muga Scan', 63, 4, '2020-07-15 11:16:44', NULL, 0),
(157, 'Nerve Conduction Velocity (ncv', 55, 176, '2020-07-15 11:16:44', NULL, 0),
(158, 'No Test', 53, 105, '2020-07-15 11:16:44', NULL, 0),
(159, 'Nt Scan', 65, 160, '2020-07-15 11:16:44', NULL, 0),
(160, 'Pap Smear', 70, 1032, '2020-07-15 11:16:44', NULL, 0),
(161, 'Paratyphi Test', 7, 142, '2020-07-15 11:16:44', NULL, 0),
(162, 'Pcv (packed Cell Volume) Test', 42, 2289, '2020-07-15 11:16:44', NULL, 0),
(163, 'Peripheral Blood Smear Test', 98, 482, '2020-07-15 11:16:44', NULL, 0),
(164, 'Pet-ct Scan', 58, 124, '2020-07-15 11:16:44', NULL, 0),
(165, 'Phosphorus Test', 41, 3008, '2020-07-15 11:16:44', NULL, 0),
(166, 'Plasma Lactate (lactic Acid) T', 83, 146, '2020-07-15 11:16:44', NULL, 0),
(167, 'Platelet Count', 26, 3864, '2020-07-15 11:16:44', NULL, 0),
(168, 'Pleural Fluid Analysis', 70, 521, '2020-07-15 11:16:44', NULL, 0),
(169, 'Potassium Test', 77, 4260, '2020-07-15 11:16:44', NULL, 0),
(170, 'Pregnancy Test', 97, 3295, '2020-07-15 11:16:44', NULL, 0),
(171, 'Progesterone Test', 4, 1475, '2020-07-15 11:16:44', NULL, 0),
(172, 'Prolactin Test', 41, 3510, '2020-07-15 11:16:44', NULL, 0),
(173, 'Protein Test', 52, 2445, '2020-07-15 11:16:44', NULL, 0),
(174, 'Protein/creatinine Ratio', 27, 1222, '2020-07-15 11:16:44', NULL, 0),
(175, 'Psa (prostate Specific Antigen', 92, 1975, '2020-07-15 11:16:44', NULL, 0),
(176, 'Pt (prothrombin Time) Test', 56, 3276, '2020-07-15 11:16:44', NULL, 0),
(177, 'Pth (parathyroid Hormone) Test', 29, 1426, '2020-07-15 11:16:44', NULL, 0),
(178, 'Pulmonary Function Test (pft)', 2, 225, '2020-07-15 11:16:45', NULL, 0),
(179, 'Renal Profile', 45, 3216, '2020-07-15 11:16:45', NULL, 0),
(180, 'Renal Scan / Kidney Scan', 11, 50, '2020-07-15 11:16:45', NULL, 0),
(181, 'Reticulocyte Count Test', 19, 2122, '2020-07-15 11:16:45', NULL, 0),
(182, 'Rheumatoid Arthritis (ra) Fact', 86, 3145, '2020-07-15 11:16:45', NULL, 0),
(183, 'Rubella Test', 8, 900, '2020-07-15 11:16:45', NULL, 0),
(184, 'Semen Analysis Test', 39, 1380, '2020-07-15 11:16:45', NULL, 0),
(185, 'Sex Hormone Test', 73, 220, '2020-07-15 11:16:45', NULL, 0),
(186, 'Sgot Test', 23, 4530, '2020-07-15 11:16:45', NULL, 0),
(187, 'Sgpt Test', 92, 4516, '2020-07-15 11:16:45', NULL, 0),
(188, 'Sickling Test', 16, 1117, '2020-07-15 11:16:45', NULL, 0),
(189, 'Sodium Test', 95, 4264, '2020-07-15 11:16:45', NULL, 0),
(190, 'Sonography (ultrasound / Usg)', 1, 2814, '2020-07-15 11:16:45', NULL, 0),
(191, 'Sonomamography / Sonomamogram', 5, 102, '2020-07-15 11:16:45', NULL, 0),
(192, 'Sperm Dna Fragmentation', 13, 7, '2020-07-15 11:16:45', NULL, 0),
(193, 'Sputum Culture', 54, 499, '2020-07-15 11:16:45', NULL, 0),
(194, 'Sputum Routine Test', 77, 2574, '2020-07-15 11:16:45', NULL, 0),
(195, 'Stool Culture', 46, 917, '2020-07-15 11:16:45', NULL, 0),
(196, 'Stool Routine', 75, 4016, '2020-07-15 11:16:45', NULL, 0),
(197, 'Stress Echo Test', 55, 16, '2020-07-15 11:16:46', NULL, 0),
(198, 'Stress Test (tmt)', 37, 317, '2020-07-15 11:16:46', NULL, 0),
(199, 'Swine Flu Test (h1n1)', 62, 138, '2020-07-15 11:16:46', NULL, 0),
(200, 'Synovial Fluid Analysis', 73, 494, '2020-07-15 11:16:46', NULL, 0),
(201, 'T3 (triiodothyronine) Test', 57, 3824, '2020-07-15 11:16:46', NULL, 0),
(202, 'T4 (thyroxine) Test', 36, 3806, '2020-07-15 11:16:46', NULL, 0),
(203, 'Tb Test', 53, 543, '2020-07-15 11:16:46', NULL, 0),
(204, 'Testosterone Test', 18, 1634, '2020-07-15 11:16:46', NULL, 0),
(205, 'Thallium Scan', 59, 36, '2020-07-15 11:16:46', NULL, 0),
(206, 'Thyroglobulin Test', 70, 1045, '2020-07-15 11:16:46', NULL, 0),
(207, 'Thyroid Scan', 97, 46, '2020-07-15 11:16:46', NULL, 0),
(208, 'Thyroid Test', 41, 5470, '2020-07-15 11:16:46', NULL, 0),
(209, 'Torch Test', 77, 786, '2020-07-15 11:16:46', NULL, 0),
(210, 'Total Protein Test', 4, 3596, '2020-07-15 11:16:46', NULL, 0),
(211, 'Toxoplasma Test', 98, 707, '2020-07-15 11:16:46', NULL, 0),
(212, 'Transferrin Test', 39, 951, '2020-07-15 11:16:46', NULL, 0),
(213, 'Triglycerides Test', 94, 2621, '2020-07-15 11:16:46', NULL, 0),
(214, 'Triple Marker Test', 88, 524, '2020-07-15 11:16:46', NULL, 0),
(215, 'Troponin-i Test', 20, 1316, '2020-07-15 11:16:46', NULL, 0),
(216, 'Tsh (thyroid Stimulating Hormo', 70, 4652, '2020-07-15 11:16:46', NULL, 0),
(217, 'Typhidot Test', 56, 634, '2020-07-15 11:16:46', NULL, 0),
(218, 'Urea Test', 14, 3708, '2020-07-15 11:16:46', NULL, 0),
(219, 'Uric Acid Test', 77, 4913, '2020-07-15 11:16:46', NULL, 0),
(220, 'Urine Culture', 58, 601, '2020-07-15 11:16:46', NULL, 0),
(221, 'Urine Routine', 8, 4885, '2020-07-15 11:16:47', NULL, 0),
(222, 'Valproic Acid', 71, 460, '2020-07-15 11:16:47', NULL, 0),
(223, 'Vdrl Test', 84, 4339, '2020-07-15 11:16:47', NULL, 0),
(224, 'Vitamin A Test', 25, 546, '2020-07-15 11:16:47', NULL, 0),
(225, 'Vitamin B12 Test', 94, 4682, '2020-07-15 11:16:47', NULL, 0),
(226, 'Vitamin C Test', 88, 404, '2020-07-15 11:16:47', NULL, 0),
(227, 'Vitamin D Test', 85, 4577, '2020-07-15 11:16:47', NULL, 0),
(228, 'Vitamin E Test', 41, 433, '2020-07-15 11:16:47', NULL, 0),
(229, 'Vldl Test', 43, 1451, '2020-07-15 11:16:47', NULL, 0),
(230, 'Vq Scan (lung Ventilation And ', 7, 4, '2020-07-15 11:16:47', NULL, 0),
(231, 'Widal Test', 4, 4298, '2020-07-15 11:16:47', NULL, 0),
(232, 'X-ray', 9, 2361, '2020-07-15 11:16:47', NULL, 0),
(233, 'X-ray Skeletal Survey', 76, 4, '2020-07-15 11:16:47', NULL, 0),
(234, 'Surgical Procedure Is New Add ', 78, 2500, '2020-07-15 17:34:43', NULL, 0),
(238, 'Bushra Akhtar Hello', 33, 30000, '2020-07-22 19:01:20', NULL, 0),
(239, 'Bushra Akhtar Hello123', 33, 3000, '2020-07-22 19:05:19', NULL, 0),
(240, 'Bushra Akhtar Hello 123', 33, 30000, '2020-07-22 23:23:33', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_lab_test`
--
ALTER TABLE `product_lab_test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_lab_test`
--
ALTER TABLE `product_lab_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
