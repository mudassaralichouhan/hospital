-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 04:32 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `remote_log`
--

CREATE TABLE `remote_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `agent` text NOT NULL,
  `method` varchar(6) NOT NULL,
  `client_addr` varchar(16) NOT NULL,
  `client_port` int(7) NOT NULL,
  `is_disable` tinyint(11) NOT NULL DEFAULT 0,
  `reg_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` int(11) NOT NULL,
  `city` varchar(200) DEFAULT NULL,
  `region` varchar(200) DEFAULT NULL,
  `region_code` varchar(5) DEFAULT NULL,
  `continent_code` varchar(5) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `continent_name` varchar(200) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `region_name` varchar(200) DEFAULT NULL,
  `latitude` varchar(10) DEFAULT NULL,
  `longitude` varchar(10) DEFAULT NULL,
  `time_zone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `remote_log`
--

INSERT INTO `remote_log` (`id`, `user_id`, `agent`, `method`, `client_addr`, `client_port`, `is_disable`, `reg_on`, `update_on`, `status`, `city`, `region`, `region_code`, `continent_code`, `country_code`, `continent_name`, `country_name`, `region_name`, `latitude`, `longitude`, `time_zone`) VALUES
(240, 234, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '119.160.65.31', 56548, 0, '2020-11-13 22:11:09', NULL, 200, 'Islamabad', 'Islamabad', 'IS', 'AS', 'PK', 'Asia', 'Pakistan', 'Islamabad', '34', '73', 'Asia/Karachi'),
(241, 255, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '107.10.65.31', 56547, 0, '2020-11-13 22:12:11', NULL, 200, 'Appleton', 'Wisconsin', 'WI', 'NA', 'US', 'North America', 'United States', 'Wisconsin', '44', '-88', 'America/Chicago'),
(242, 255, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '107.10.65.31', 56553, 0, '2020-11-13 22:15:01', NULL, 200, 'Appleton', 'Wisconsin', 'WI', 'NA', 'US', 'North America', 'United States', 'Wisconsin', '44.2433', '-88.3564', 'America/Chicago'),
(245, 266, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '127.0.0.1', 62944, 0, '2020-11-14 12:59:18', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 255, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '127.0.0.1', 59821, 0, '2020-11-15 21:43:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 224, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '::1', 63464, 0, '2020-11-16 05:09:34', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 258, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'POST', '127.0.0.1', 63923, 0, '2020-11-16 06:43:19', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `remote_log`
--
ALTER TABLE `remote_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `remote_log`
--
ALTER TABLE `remote_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
